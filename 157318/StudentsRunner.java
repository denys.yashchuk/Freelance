
import java.util.ArrayList;
import java.util.Scanner;

public class StudentsRunner {

    public static void main(String[] args) {
        ArrayList<Student> students = new ArrayList<Student>();
        Scanner sc = new Scanner(System.in);
        boolean stop = false;
        System.out.print("Enter Student ID");
        String id = sc.nextLine();
        System.out.print("Enter Student name");
        String name = sc.nextLine();
        students.add(new Student(id, name));
        System.out.print("The student infohas been stored"
                + "\ninsert Q to quit");
        do {
            switch (sc.nextLine().toLowerCase()) {
                case "q":
                    for (int i = 0; i < students.size(); i++) {
                        System.out.println(" student #" + (i + 1)
                                + "\nthe student ID: " + students.get(i).getId()
                                + "\nthe student name: " + students.get(i).getName());
                    }
                    stop = true;
                    break;
                default:
                    System.out.print("Enter Student ID");
                    id = sc.nextLine();
                    System.out.print("Enter Student name");
                    name = sc.nextLine();
                    students.add(new Student(id, name));
                    System.out.println("The student infohas been stored"
                            + "\ninsert Q to quit");
                    break;

            }
        } while (!stop);
    }

}

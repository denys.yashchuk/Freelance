
import java.util.Scanner;

public class Main {

    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int in = getNumber();
        System.out.println("The number of keys that do not map any symbols to themselves: "
                + numberOfCipherKeys(in));
        for (int i = 2; i < 20001; i++) {
            System.out.println(numberOfCipherKeys(i));
        }
    }

    public static int getNumber() {
        int num = -1;
        System.out.print(" Enter the number of symbols in the alphabet(from 1 to 20000): ");
        String s = sc.nextLine(); // reads line from terminal
        try {
            num = Integer.parseInt(s); // try to parse it to the int type
        } catch (Exception e) {
        }
        // if the number is in range from 1 to 20000 returns this number, otherwise reads new one
        if (num < 1 || num > 20000) {
            return getNumber();
        } else {
            return num;
        }

    }

    /**
     * Calculates the number of keys that do not map any symbols to themselves.
     *
     * @param numberOfSymbols the number of symbols in the alphabet
     * @return the number of keys that do not map any symbols to themselves.
     */
    public static long numberOfCipherKeys(int numberOfSymbols) {
        if (numberOfSymbols == 1) {
            return 0; // if alphabet contains only 1 symbol there is no such keys;
        } else if (numberOfSymbols == 2) {
            return 1; // if there is 2 symbols in the alphabet, there is only 1 key
        } else {
            // if number of symbols is even the number of keys is number of symbols multiply by number of keys
            // for alphabet with decreased number of symbol by 1 plus 1;
            if (numberOfSymbols % 2 == 0) {
                return (numberOfSymbols * numberOfCipherKeys(numberOfSymbols - 1) + 1) % 1000000007;
            } else {
                // if number of symbols is odd the number of keys is number of symbols multiply by number of keys
                // for alphabet with decreased number of symbol by 1 minus 1;
                return (numberOfSymbols * numberOfCipherKeys(numberOfSymbols - 1) - 1) % 1000000007;
            }
        }
    }

}


import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    public static void main(String[] args) {
        Scanner sc;
        try {
            sc = new Scanner(new File("graph.txt"));
            Graph g = new Graph(Integer.parseInt(sc.nextLine()), true);
            char[] names = sc.nextLine().replace(" ", "").toCharArray();
            ArrayList<Vertex> vertices = g.vertices();
            for (int i = 0; i < names.length; i++) {
                vertices.get(i).setName(names[i]);
            }
            while (sc.hasNextLine()) {
                String[] edge = sc.nextLine().trim().split(" ");
                g.addEdge(g.getVertexFromList(new Vertex(edge[0].charAt(0))),
                        g.getVertexFromList(new Vertex(edge[1].charAt(0))),
                        Integer.parseInt(edge[2]));
            }
            g.print();
            g.bsf(new Vertex('a'));
            g.removeVertex(new Vertex('h'));
            System.out.println(g.isComplete() ? "Graph is complete" : "Graph is not complete");
            System.out.println("Number of edges is " + g.nEdges());
            System.out.println("There is " + (g.adjacent(new Vertex('a'), new Vertex('f')) ? "a" : "no")
                    + " link from a to f");
            System.out.println("The weight of the edge from a to c is "
                    + g.getWeight(new Vertex('a'), new Vertex('c')));
            System.out.println("The degree of c is " + g.degree(new Vertex('c')));
            System.out.println("The graph contains " + g.size() + " vertices");
            g.addVertex(new Vertex('k'));
            g.addEdge(new Vertex('k'), new Vertex('a'), 5);
            g.addEdge(new Vertex('g'), new Vertex('k'), 2);
            g.setWeight(new Vertex('a'), new Vertex('c'), 7);
            g.print();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}

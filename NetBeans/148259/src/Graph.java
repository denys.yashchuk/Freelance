
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.ScrollPane;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class Graph {

    private boolean isDirect;
    private boolean isWeighted;
    private ArrayList<Vertex> vert;

    public Graph() {
        this.isDirect = false;
        this.isWeighted = true;
        vert = new ArrayList<Vertex>();
    }

    public Graph(int n) {
        this.isDirect = false;
        this.isWeighted = true;
        vert = new ArrayList<Vertex>();
        for (int i = 0; i < n; i++) {
            vert.add(new Vertex());
        }
    }

    public Graph(int n, boolean isDirect) {
        this.isDirect = isDirect;
        this.isWeighted = true;
        vert = new ArrayList<Vertex>();
        for (int i = 0; i < n; i++) {
            vert.add(new Vertex());
        }
    }

    public boolean isDirect() {
        return isDirect;
    }

    public boolean adjacent(Vertex v, Vertex u) {
        return getVertexFromList(v).outEdges.contains(new Edge(v, u, 1));
    }

    public ArrayList<Vertex> neighbors(Vertex v) {
        ArrayList<Vertex> out = new ArrayList<Vertex>();
        for (int i = 0; i < v.inEdges.size(); i++) {
            out.add(v.inEdges.get(i).from);
        }
        for (int i = 0; i < v.outEdges.size(); i++) {
            Vertex u = v.inEdges.get(i).to;
            if (!out.contains(u)) {
                out.add(u);
            }
        }
        return out;
    }

    public void addVertex(Vertex v) {
        if (vert.contains(v)) {
            System.out.println("The vertex is already in the graph.");
        } else {
            vert.add(v);
        }
    }

    public void removeVertex(Vertex v) {
        v = getVertexFromList(v);
        Vertex u;
        for (Edge e : v.inEdges) {
            u = e.from;
            u.outEdges.remove(e);
        }
        for (Edge e : v.outEdges) {
            u = e.to;
            u.outEdges.remove(e);
        }
        vert.remove(v);
    }

    public void addEdge(Vertex v, Vertex u) {
        getVertexFromList(v).addEdge(u, isDirect, 1);
    }

    public void addEdge(Vertex v, Vertex u, int w) {
        getVertexFromList(v).addEdge(u, isDirect, w);
    }

    public void removeEdge(Vertex v, Vertex u) {
        Edge e = findEdge(v, u);
        v.outEdges.remove(e);
        u.inEdges.remove(e);
        if (!isDirect) {
            e = findEdge(u, v);
            u.outEdges.remove(e);
            v.inEdges.remove(e);
        }
    }

    private Edge findEdge(Vertex v, Vertex u) {
        v = getVertexFromList(v);
        u = getVertexFromList(u);
        for (int i = 0; i < v.outEdges.size(); i++) {
            if (v.outEdges.get(i).to.equals(u)) {
                System.out.println(v.outEdges.get(i));
                return v.outEdges.get(i);
            }
        }
        return null;
    }

    public int getWeight(Vertex v, Vertex u) {
        int indexOfEdge = getVertexFromList(v).outEdges.indexOf(new Edge(v, u, 1));
        return getVertexFromList(v).outEdges.get(indexOfEdge).weight;
    }

    public void setWeight(Vertex v, Vertex u, int w) {
        int indexOfEdge = getVertexFromList(v).outEdges.indexOf(new Edge(v, u, 1));
        getVertexFromList(v).outEdges.get(indexOfEdge).weight = w;
    }

    public boolean isEmpty() {
        return vert.isEmpty();
    }

    public boolean isComplete() {
        Vertex v, u;
        for (int i = 0; i < vert.size(); i++) {
            v = vert.get(i);
            if (v.outEdges.size() != vert.size() - 1) {
                return false;
            } else {
                for (int j = 0; j < vert.size(); j++) {
                    u = vert.get(j);
                    if (!u.equals(v) && !v.outEdges.contains(new Edge(v, u, 1))) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public ArrayList<Vertex> vertices() {
        return vert;
    }

    public ArrayList<Edge> edges() {
        ArrayList<Edge> out = new ArrayList<Edge>();
        for (int i = 0; i < vert.size(); i++) {
            out.addAll(vert.get(i).outEdges);
        }
        return out;
    }

    public int degree(Vertex v) {
        return getVertexFromList(v).degree;
    }

    public int size() {
        return vert.size();
    }

    public int nEdges() {
        return edges().size();
    }

    public void clear() {
        vert.clear();
    }

    public boolean vertexExist(Vertex v) {
        return vert.contains(v);
    }

    public void print() {
        System.out.print("Vertices: ");
        for (Vertex v : vert) {
            System.out.print(v + "; ");
        }
        System.out.print("\nEdges: ");
        if (isWeighted) {
            for (Edge e : edges()) {
                System.out.print(e.showWithWeight() + "; ");
            }
        } else {
            for (Edge e : edges()) {
                System.out.print(e + "; ");
            }
        }
        System.out.println("");
    }

    public void bsf(Vertex start) {
        vert.stream().forEach((v) -> {
            v.wasViseted = false;
        });
        start = getVertexFromList(start);
        start.wasViseted = true;
        start.degree = 0;
        start.bfsParent = null;
        Queue<Vertex> q = new PriorityQueue<Vertex>(size(),
                new Comparator<Vertex>() {
            @Override
            public int compare(Vertex t, Vertex t1) {
                if (t.getName() > t1.getName()) {
                    return -1;
                }
                if (t.getName() < t1.getName()) {
                    return 1;
                }
                return 0;
            }
        });
        q.add(start);
        Vertex v, u;
        while (!q.isEmpty()) {
            v = q.poll();
            for (int i = 0; i < v.outEdges.size(); i++) {
                u = v.outEdges.get(i).to;
                if (!u.wasViseted) {
                    u.wasViseted = true;
                    u.degree = v.degree + 1;
                    u.bfsParent = v;
                    q.add(u);
                }
            }
        }
        JFrame tables = new JFrame();
        tables.setSize(920, 70);
        tables.setResizable(false);
        tables.setTitle("BSF");
        String[] headers = new String[vert.size()];
        String[][] piValues = new String[1][vert.size()];
        String[][] dValues = new String[1][vert.size()];
        for(int i = 0; i<vert.size();i++){
            headers[i] = vert.get(i).getName()+"";
            piValues[0][i] = Integer.toString(vert.get(i).degree);
            dValues[0][i] = (vert.get(i).bfsParent== null)?"NIL":vert.get(i).bfsParent.toString();
        }
        JTable d = new JTable(dValues,headers);
        JTable pi = new JTable(piValues,headers);
        tables.getContentPane().setLayout(new FlowLayout());
        tables.getContentPane().add(new JScrollPane(pi));
        tables.getContentPane().add(new JScrollPane(d));
        tables.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        tables.setVisible(true);
    }

    public Vertex getVertexFromList(Vertex v) {
        return vert.get(vert.indexOf(v));
    }

}


public class TaskDate implements Comparable {

    private int month;
    private int day;
    private int year;

    public TaskDate(int month, int day, int year) {
        this.month = month;
        this.day = day;
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }

    public int getYear() {
        return year;
    }

    //shows date in format "DD\MM\YYYY";
    @Override
    public String toString() {
        return (Integer.toString(getMonth()).length() == 2 ? getMonth() : "0" + getMonth())
                + "\\" + (Integer.toString(getDay()).length() == 2 ? getDay() : "0" + getDay()) + "\\"
                + getYear();
    }

    //return value = 0; if dates are equal; < 0 if other date is later; > 0 if this date is later;
    @Override
    public int compareTo(Object t) {
        TaskDate otherDate = (TaskDate) t;
        if (getYear() != (otherDate.getYear())) {
            return getYear() - (otherDate.getYear());
        } else if (getMonth() != (otherDate.getMonth())) {
            return getMonth() - (otherDate.getMonth());
        } else {
            return getDay() - (otherDate.getDay());
        }
    }
}

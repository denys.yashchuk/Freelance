
import java.util.ArrayList;
import java.util.Scanner;

public class TaskManager {

    private static TaskDate today;
    private static Scanner sc = new Scanner(System.in);
    private static ArrayList<Task> tasks = new ArrayList<Task>();

    public static void main(String[] args) {
        System.out.println("run:\n");
        System.out.print("Enter the current date.\n");
        System.out.print("Enter current month: ");
        int month = sc.nextInt(); // read entered int;
        System.out.print("Enter current day: ");
        int day = sc.nextInt();
        System.out.print("Enter current year: ");
        int year = sc.nextInt();
        today = new TaskDate(month, day, year);
        int choice;
        boolean stop = false;
        // do while '7' wasn't entered;
        while (!stop) {
            System.out.print("********TASK MANAGER**********\n"
                    + "1 ­ Add task\n"
                    + "2 ­ print all tasks\n"
                    + "3 ­ find tasks in a date range\n"
                    + "4 ­ print today's schedule\n"
                    + "5 ­ delete task\n"
                    + "6 ­ modify task\n"
                    + "7 ­ quit\n"
                    + "Enter choice: ");
            choice = sc.nextInt();
            sc.nextLine(); // finishing line, because nextInt() reads only one int and does not finish the line.
            // use method according to the choice
            switch (choice) {
                case 1:
                    addTask();
                    break;
                case 2:
                    printAllTasks();
                    break;
                case 3:
                    printTasksByDateRange();
                    break;
                case 4:
                    printTodaysSchedule();
                    break;
                case 5:
                    deleteTask();
                    break;
                case 6:
                    modifyTask();
                    break;
                case 7:
                    stop = true; // stop condition
                    break;
                default:
                    System.out.println("Make right choice!");
                    break;
            }
        }
        System.out.print("*** THANK YOU FOR USING OUR TASK MANAGER ***");
    }

    public static void addTask() {
        String name;
        do {
            System.out.print("enter name: ");
            name = sc.nextLine();
        } while (!checkName(name)); // reads name, while unique name won't entered
        System.out.print("enter a short description: ");
        String description = sc.nextLine();
        System.out.print("enter a month (1 to 12): ");
        int month = sc.nextInt();
        System.out.print("enter a day (1 to 31): ");
        int day = sc.nextInt();
        System.out.println("enter a year (from 2010 to 3000):");
        int year = sc.nextInt();
        sc.nextLine();
        tasks.add(new Task(name, description, new TaskDate(day, month, year))); // add new task object to the list;
        int indexOfTheTask = tasks.size() - 1;
        System.out.print("Enter new tag (type 'quit' to quit) ");
        String addTag = sc.nextLine();
        // adds tags, while 'quit' isn't entered, or stops if there is 20 tags.
        while (!addTag.equals("quit") && tasks.get(indexOfTheTask).getTags().size() < 20) {
            System.out.print("Enter new tag (type 'quit' to quit) ");
            tasks.get(indexOfTheTask).addTag(addTag);
            addTag = sc.nextLine();
        }
    }

    // check if the name in unique
    private static boolean checkName(String name) {
        boolean free = true;
        for (Task task : tasks) {
            if (name.equals(task.getName())) {
                free = false;
            }
        }
        if (!free) {
            System.out.println("Duplicating of names. Plese, write another name.");
        }
        return free;
    }

    public static void printAllTasks() {
        System.out.println("\n%%%%%%%%%% TASKS %%%%%%%%%%%\n");
        if (tasks.isEmpty()) {
            System.out.println("No Tasks in List\n");
        } else {
            for (Task task : tasks) {
                System.out.println(task + "\n");
            }
        }
    }

    public static void printTasksByDateRange() {
        //read begin and end dates;
        System.out.print("Enter from date month: ");
        int month = sc.nextInt();
        System.out.print("Enter from date day: ");
        int day = sc.nextInt();
        System.out.print("Enter from date year: ");
        int year = sc.nextInt();
        TaskDate begin = new TaskDate(day, month, year);
        System.out.print("Enter to date month: ");
        month = sc.nextInt();
        System.out.print("Enter to date day: ");
        day = sc.nextInt();
        System.out.print("Enter to date year: ");
        year = sc.nextInt();
        sc.nextLine();
        TaskDate end = new TaskDate(day, month, year);
        // check is begin date is before end date;
        if (begin.compareTo(end) > 0) {
            System.out.println("Wrong date range!");
            return;
        }
        // shows all tasks in this date range;
        for (Task task : tasks) {
            TaskDate td = task.getDueDate();
            if (td.compareTo(begin) > 0 && td.compareTo(end) < 0) {
                System.out.println(task + "\n");
            }
        }
    }

    public static void printTodaysSchedule() {
        //shows past due tasks;
        System.out.println("\n!!!! PAST DUE !!!!\n");
        for (Task task : tasks) {
            TaskDate td = task.getDueDate();
            if (td.compareTo(today) < 0) {
                System.out.println(task + "\n");
            }
        }
        //shows all today's task;
        System.out.println("\n­­Today's Tasks­­\n");
        for (Task task : tasks) {
            TaskDate td = task.getDueDate();
            if (td.compareTo(today) == 0) {
                System.out.println(task + "\n");
            }
        }
        //shows all future tasks;
        System.out.println("\n­­Coming up soon!­­\n");
        for (Task task : tasks) {
            TaskDate td = task.getDueDate();
            if (td.compareTo(today) > 0) {
                System.out.println(task + "\n");
            }
        }
    }

    //finds the index of the task with such name
    public static int findTask(String name) {
        int index = -1;
        for (int i = 0; i < tasks.size(); i++) {
            if (tasks.get(i).getName().equals(name)) {
                index = i;
            }
        }
        return index;
    }

    public static void deleteTask() {
        System.out.print("Enter task name to delete: ");
        String name = sc.nextLine().trim();
        int index = findTask(name); // find the index of the task with such name;
        if (index == -1) {
            System.out.println("\ncouldn't find task.");
        } else {
            tasks.remove(index);//remove task from list;
            System.out.println("\nTask has been removed.");
        }
    }

    public static void modifyTask() {
        System.out.print("Enter task name to modify: ");
        String name = sc.nextLine();
        int index = findTask(name); // find the index of the task with such name;
        if (index == -1) {
            System.out.println("Task does not exists.");
        } else {
            boolean stop = false;
            while (!stop) {
                System.out.print("====>MODIFY\n"
                        + "1 ­ name\n"
                        + "2 ­ description\n"
                        + "3 ­ due date\n"
                        + "4 ­ add tags\n"
                        + "5 ­ delete tags\n"
                        + "6 ­ quit\n"
                        + "Enter choice: ");
                int choice = sc.nextInt();
                sc.nextLine();
                switch (choice) {
                    case 1:
                        System.out.print("Enter new name: ");
                        tasks.get(index).setName(sc.nextLine()); //sets new name;
                        break;
                    case 2:
                        System.out.print("Enter new description: ");
                        tasks.get(index).setDescription(sc.nextLine());//sets new description;
                        break;
                    case 3:
                        System.out.print("Enter new date month: ");
                        int month = sc.nextInt();
                        System.out.print("Enter new date day: ");
                        int day = sc.nextInt();
                        System.out.print("Enter new date year: ");
                        int year = sc.nextInt();
                        sc.nextLine();
                        tasks.get(index).setDueDate(new TaskDate(month, day, year));//sets new date;
                        break;
                    case 4:
                        //adds new tags;
                        System.out.print("Enter new tag (type 'quit' to quit) ");
                        String addTag = sc.nextLine();
                        while (!addTag.equals("quit") && tasks.get(index).getTags().size() < 20) {
                            System.out.print("Enter new tag (type 'quit' to quit) ");
                            tasks.get(index).addTag(addTag);
                            addTag = sc.nextLine();
                        }
                        break;
                    case 5:
                        //deletes tags;
                        System.out.print("Enter tag to delete (type 'quit' to quit) ");
                        String delTag = sc.nextLine();
                        while (!delTag.equals("quit")) {
                            System.out.print("Enter tag to delete (type 'quit' to quit) ");
                            tasks.get(index).delTag(delTag);
                            delTag = sc.nextLine();
                        }
                        break;
                    case 6:
                        //stop condition;
                        stop = true;
                        break;
                }
            }
        }
    }

}


import java.util.ArrayList;

public class Task {

    private String name;
    private String description;
    private TaskDate dueDate;
    ArrayList<String> tags;

    public Task(String name, String description, TaskDate dueDate) {
        this.name = name;
        this.description = description;
        this.dueDate = dueDate;
        this.tags = new ArrayList<String>();
    }

    public void addTag(String tag) {
        tags.add(tag);
    }
    
    public void delTag(String tag){
        int index = -1;
        for (int i = 0; i < tags.size(); i++) {
            if (tags.get(i).equals(name)) {
                index = i;
            }
        }
        if(index != -1)tags.remove(index);
    }

    public ArrayList<String> getTags() {
        return tags;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TaskDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(TaskDate dueDate) {
        this.dueDate = dueDate;
    }

    @Override
    public String toString() {
        String out = "==>" + name + "\n- ";
        out += description + "\n­­­­­­­­­­­­­­­­­­­";
        out += "\nDUE: " + dueDate;
        out += "\nTAGS:";
        if(!tags.isEmpty()){
        for (int i = 0; i < tags.size() - 1; i++) {
            out += tags.get(i) + ";";
        }
        out += tags.get(tags.size() - 1);}
        out += "\n====================\n";
        return out;
    }

}

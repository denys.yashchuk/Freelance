
public class Cylinder {

    private double radius;
    private double height;

    public Cylinder(double radius, double height) {
        this.radius = radius;
        this.height = height;
    }

    public double getVolume() {
        return 2 * Math.PI * Math.pow(radius, 2) * height ;
    }

    public double getSurface() {
        return 2* Math.PI * radius * (height + radius);
    }

}

public class Sphere {
    
    private double radius;

    public Sphere(double radius) {
        this.radius = radius;
    }
    
    public double getVolume() {
        return 4 * Math.PI * Math.pow(radius, 3) / 3 ;
    }

    public double getSurface() {
        return Math.PI * Math.pow(radius, 2) * 4 ;
    }
    
}

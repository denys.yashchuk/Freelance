
import java.util.Scanner;

public class GeometryTUI {

    private Scanner sc;
    private double radius;
    private double height;

    public GeometryTUI() {
        sc = new Scanner(System.in);
        radius = 0;
        height = 0;
    }

    public void run() {
        getRadiusAndHeight();
        displayCone();
        displayCylinder();
        displaySphere();
    }

    public void getRadiusAndHeight() {
        System.out.println("Enter radius: ");
        radius = sc.nextDouble();
        System.out.println("Enter height: ");
        height = sc.nextDouble();
    }

    public void displayCone() {
        Cone cone = new Cone(radius, height);
        System.out.println("Cone volume: " + cone.getVolume() + ";\nCone surface area:" + cone.getSurface() + ";");
    }

    
    public void displayCylinder() {
        Cylinder cylinder = new Cylinder(radius, height);
        System.out.println("Cylinder volume: " + cylinder.getVolume() + ";\nCylinder surface area:" + cylinder.getSurface() + ";");
    }
    
    public void displaySphere() {
        Sphere sphere = new Sphere(radius);
        System.out.println("Sphere volume: " + sphere.getVolume() + ";\nSphere surface area:" + sphere.getSurface() + ";");
    }
    
}

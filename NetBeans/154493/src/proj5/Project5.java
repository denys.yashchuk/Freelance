package proj5;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Project5 {

    public static void main(String[] args) {
        JFrame f = new JFrame();
        Game game;
        boolean play = true;
        String players;
        while (play) {
            try {
                players = JOptionPane.showInputDialog(f,
                        "Enter the number of players: ", "Game", JOptionPane.QUESTION_MESSAGE);
                if (players == null) {
                    System.exit(0);
                }
                game = new Game(Integer.parseInt(players));
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(f, "Wrong input", "Game",
                        JOptionPane.ERROR_MESSAGE);
                System.out.println("Wrong input");
                continue;
            }
            while (game.determineWinner() == -1) {
                game.playRound();
                System.out.println(game);
            }
            int n = JOptionPane.showConfirmDialog(
                    f,
                    "Play again?",
                    "Game",
                    JOptionPane.YES_NO_OPTION);
            if (n==1) {
                System.exit(0);
            }
            System.out.println("");
        }
    }

}

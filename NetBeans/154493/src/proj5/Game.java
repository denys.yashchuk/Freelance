package proj5;
/**
 * Duck Duck Goose Game
 * This class provides game "Duck Duck Goose"
 * @author student
 */
public class Game {

    private boolean[] playing;
    private int count = 0;

    /**
     * Creates Game class, initializes array of players
     * @param numOfPlaying - number of players
     */
    public Game(int numOfPlaying) {
        playing = new boolean[numOfPlaying];
        for (int i = 0; i < playing.length; i++) {
            playing[i] = true;
        }
    }

    /**
     * plays one round of the game
     */
    public void playRound() {
        for (int i = 0; i < playing.length; i++) {
            if (playing[i]) {
                count++;
            }
            if (count == 3) {
                playing[i] = false;
                count = 0;
            }
        }
    }

    /**
     * determines number of still playing players
     * @return number of still playing players
     */
    public int determineNumStillPlaying() {
        int num = 0;
        for (int i = 0; i < playing.length; i++) {
            if (playing[i]) {
                num++;
            }
        }
        return num;
    }

    /**
     * determines winner
     * @return index of player, who wins
     */
    public int determineWinner() {
        if (determineNumStillPlaying() == 1) {
            for (int i = 0; i < playing.length; i++) {
                if (playing[i]) {
                    return i;
                }
            }
        }
        return -1;
    }

    /**
     * builds a String containing whether each player is still
     * playing or has been eliminated.
     * @return String of players with their status
     */
    public String toString() {
        String str = new String();
        for (int i = 0; i < playing.length; i++) {

            str += (i + ":" + (playing[i]?"In":"Out")+ " ");

        }
        return str;
    }
}

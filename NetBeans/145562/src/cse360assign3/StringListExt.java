/*
 * Name: Hassan AlMulla
 * ID: 1206886781
 * Pin: E3
 */

package cse360assign3;

public class StringListExt extends StringList {

	/**
	 * add the String to the end of the list
	 * @param item
	 */
	public void add(String item) {
		list.add(item);
	}
	
	/**
	 * return the String at the designated position. If the list is empty, return an empty String (not a null).
	 * @param index
	 * @return
	 */
	public String get(int index) {
		if (list.isEmpty() || index < 0 || index > list.size() - 1)
			return "";
		
		return list.get(index);
	}
	
	/**
	 * return the count of the Strings in the list
	 * @return
	 */
	public int length() {
		return list.size();
	}
	
	/**
	 * return the last String in the list. If the list is empty, return an empty String (not a null)
	 * @return
	 */
	public String getLast() {
		if (list.isEmpty())
			return "";
		
		return list.get(list.size() - 1);
	}
	
	/**
	 * remove the occurrence of the String from the list. If the String is not in the list, do nothing.
	 * @param item
	 */
	public void remove(String item) {
		list.remove(item);
	}
	
}

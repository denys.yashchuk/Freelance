package cse360assign3;

import java.util.ArrayList;

public class NetworkTable {

	private ArrayList<NetworkEntry> table;
	private StringListExt lookup;

	public NetworkTable() {
		table = new ArrayList<NetworkEntry>(); //Initialize table
		lookup = new StringListExt(); //Initialize lookup
	}

	/**
	 * Print every NetworkEntry
	 */
	public void print() {
		for (NetworkEntry ne : table) {
			System.out.println(ne);
		}
	}

	/**
	 * If node isn't in table create new NetworkEntry and add it to the table and to the lookup,
	 * else add pred to the NetworkEntry
	 * @param node
	 * @param val
	 * @param pred
	 */
	public void add(String node, int val, String pred) {
		int index = lookup.search(node);
		if (index == -1) {
			NetworkEntry ne = new NetworkEntry(node, val, pred);
			table.add(ne);
			lookup.add(node);
		} else {
			table.get(index).addPred(pred);
		}
	}

	/**
	 * Determining all successors for each node in the table
	 */
	public void setSuccessors() {
		for (NetworkEntry ne : table) { //for each entry
			for (int i = 0; i < ne.getPredList().length(); i++) {
				String lable = ne.getPredList().get(i); //get its pred
				int index = lookup.search(lable); //if pred is in the table
				if (index != -1) {
					NetworkEntry pred = table.get(index);//get pred of this NetworkEntry
					pred.addSucc(ne.getLabel());//add this NetworkEntry to its pred
				}
			}
		}
	}

}

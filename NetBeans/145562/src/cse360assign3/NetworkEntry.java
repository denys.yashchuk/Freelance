/*
 * Name: Hassan AlMulla
 * ID: 1206886781
 * Pin: E3
 */
package cse360assign3;

public class NetworkEntry {

	private String name;
	private int cost;
	private StringListExt predList;
	private StringListExt succList;
	
	public NetworkEntry(String name, int cost, String pred){
		this.name = name; //Set name value
		this.cost = cost; // Set cost value
		predList = new StringListExt(); //Initialize predList
		predList.add(pred); //Add pred
		succList = new StringListExt();// Initialize succList
	}

	/**
	 * Getter
	 * @return name
	 */
	public String getLabel() {
		return name;
	}

	/**
	 * Getter
	 * @return cost
	 */
	public int getCost() {
		return cost;
	}

	/**
	 * Getter
	 * @return predList
	 */
	public StringListExt getPredList() {
		return predList;
	}

	/**
	 * Getter
	 * @return succList
	 */
	public StringListExt getSuccList() {
		return succList;
	}
	
	/**
	 * Add succ to succList
	 * @param succ
	 */
	public void addSucc(String succ){
		succList.add(succ);
	}
	
	/**
	 * Add pred to predList
	 * @param pred
	 */
	public void addPred(String pred){
		predList.add(pred);
	}

	@Override
	public String toString() {
		return name+" "+cost+" Pred "+predList+" Succ "+succList ;
	}
	
	
	
}

package waiterOrder;

public class Main {

    public static void main(String[] args) {
        Dish[] menu = new Dish[6];
        menu[0] = new Dish("Main dish", "Grilled Chicken", 3.5);
        menu[1] = new Dish("Main dish", "Baked Hum", 5.);
        menu[2] = new Dish("Dessert", "Ice Cream", 1.5);
        menu[3] = new Dish("Dessert", "Chocolate Cake", 1.);
        menu[4] = new Dish("Drink", "Juice", 1.);
        menu[5] = new Dish("Drink", "Water", 0.5);

        new waiterOrderGUI(menu);
    }
}

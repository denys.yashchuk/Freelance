package waiterOrder;

import carDealerSearchTool.Car;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class waiterOrderGUI extends JFrame {

    Dish[] menu;
    Order order = new Order();

    public waiterOrderGUI(Dish[] menu) {
        this.menu = menu;
        setMinimumSize(new Dimension(300, 400));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Order");
        addComponentsToPane(getContentPane());
        setResizable(false);
        setVisible(true);
    }

    private void addComponentsToPane(Container pane) {
        pane.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets(5, 5, 5, 5);

        JCheckBox discount = new JCheckBox();
        c.gridx = 0;
        c.gridy = 0;
        pane.add(discount, c);

        JLabel discountLable = new JLabel(" discount");
        discountLable.setPreferredSize(new Dimension(80, 20));
        c.gridx = 1;
        c.gridy = 0;
        pane.add(discountLable, c);

        ButtonGroup group = new ButtonGroup();
        JRadioButton addButton = new JRadioButton("Add dish", true);
        group.add(addButton);
        JRadioButton delButton = new JRadioButton("Delete dish", false);
        group.add(delButton);
        c.gridx = 0;
        c.gridy = 1;
        pane.add(addButton, c);

        c.gridx = 1;
        c.gridy = 1;
        pane.add(delButton, c);

        JLabel typeLable = new JLabel("Type: ");
        discountLable.setPreferredSize(new Dimension(80, 20));
        c.gridx = 0;
        c.gridy = 2;
        pane.add(typeLable, c);

        String[] types = {"Main dish", "Dessert", "Drink"};
        JComboBox typesCB = new JComboBox(types);
        c.gridx = 1;
        c.gridy = 2;
        pane.add(typesCB, c);

        JLabel nameLable = new JLabel("Name Of Dish: ");
        discountLable.setPreferredSize(new Dimension(80, 20));
        c.gridx = 0;
        c.gridy = 3;
        pane.add(nameLable, c);

        JComboBox nameCB = new JComboBox();
        for (Dish d : menu) {
            if (d.getType().equals(typesCB.getSelectedItem())) {
                nameCB.addItem(d.getName());
            }
        }
        c.gridx = 1;
        c.gridy = 3;
        pane.add(nameCB, c);

        typesCB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                nameCB.removeAllItems();
                for (Dish d : menu) {
                    if (d.getType().equals(typesCB.getSelectedItem())) {
                        nameCB.addItem(d.getName());
                    }
                }
            }
        });

        JButton addDelButton = new JButton("Add/Delete");
        addDelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (addButton.isSelected()) {
                    order.addItem(menu[findIndex(typesCB.getSelectedItem().toString(),
                            nameCB.getSelectedItem().toString())]);
                } else {
                    order.delItem(menu[findIndex(typesCB.getSelectedItem().toString(),
                            nameCB.getSelectedItem().toString())]);
                }
                System.out.println(order);
            }
        });

        c.gridx = 0;
        c.gridy = 4;
        pane.add(addDelButton, c);

        JButton show = new JButton("Show order");
        show.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order.setDiscount(discount.isSelected());
                showOrder();
            }
        });

        c.gridx = 1;
        c.gridy = 4;
        pane.add(show, c);

        JButton clear = new JButton("Clear List");
        clear.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order = new Order();
            }
        });

        c.gridx = 0;
        c.gridy = 5;
        c.gridwidth = 2;
        pane.add(clear, c);

    }

    private void showOrder() {
        JFrame frame = new JFrame();
        frame.setMinimumSize(new Dimension(300, 400));
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setTitle("Your order");

        JTextArea r = new JTextArea();
        r.setLineWrap(true);
        r.setEnabled(false);
        r.setText(order.toString());
        JScrollPane rSP = new JScrollPane(r);
        rSP.setPreferredSize(new Dimension(300, 400));

        frame.getContentPane().add(rSP);

        frame.setVisible(true);
    }

    private int findIndex(String type, String name) {
        int index = 0;
        boolean find = false;
        while (!find && index < menu.length) {
            if (menu[index].getType().equals(type)
                    && menu[index].getName().equals(name)) {
                find = true;
            } else {
                index++;
            }
        }
        return index;
    }
}

package waiterOrder;

import java.util.ArrayList;

public class Order {

    private final int DISCOUNT = 10;
    private boolean discount;
    ArrayList<Dish> order;

    public Order() {
        this.discount = discount;
        order = new ArrayList<Dish>();
    }

    public void setDiscount(boolean discount) {
        this.discount = discount;
    }

    public void addItem(Dish dish) {
        order.add(dish);
    }

    public void delItem(Dish dish) {
        order.remove(dish);
    }

    @Override
    public String toString() {
        String out = "Your order:\n";
        double sum = 0;
        for (Dish d : order) {
            out += d + ";\n";
            sum += d.getPrice();
        }
        out += "Sum: $" + sum + ";\n";
        out += "Discount: " + (discount ? DISCOUNT : 0) + "%;\n";
        out += "To be paid: $" + (discount ? (sum - sum * (DISCOUNT / 100.)) : sum) + ";\n";
        out += "THANK YOU";
        return out;
    }

}

package vehicles;

public class Vehicle {

    String typeOfVehicle;
    String makeOfVehicle;
    String modelOfVehicle;
    int year;
    double price;

    public Vehicle(String typeOfVehicle, String makeOfVehicle,
            String modelOfVehicle, int year, double price) {
        this.typeOfVehicle = typeOfVehicle;
        this.makeOfVehicle = makeOfVehicle;
        this.modelOfVehicle = modelOfVehicle;
        this.year = year;
        this.price = price;
    }

    @Override
    public String toString() {
        return typeOfVehicle.toUpperCase() + ": " + makeOfVehicle + " "
                + modelOfVehicle + ", year: " + year + "; price: $" + price;
    }

}

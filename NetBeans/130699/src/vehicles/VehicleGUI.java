package vehicles;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class VehicleGUI extends JFrame {

    ArrayList<Vehicle> available;

    public VehicleGUI() {
        available = new ArrayList<Vehicle>();
        setMinimumSize(new Dimension(300, 400));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Vehicles");
        addComponentsToPane(getContentPane());
        setResizable(false);
        setVisible(true);
    }

    private void addComponentsToPane(Container pane) {
        JTabbedPane tabs = new JTabbedPane();
        JPanel addVehicle = new JPanel();
        JPanel buyVehicel = new JPanel();

        // addVehicle tab
        addVehicle.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets(5, 5, 5, 5);

        JLabel typeLable = new JLabel("Vehicle Type:");
        typeLable.setPreferredSize(new Dimension(120, 20));
        c.gridx = 0;
        c.gridy = 0;
        addVehicle.add(typeLable, c);

        JTextField typeTF = new JTextField();
        typeTF.setPreferredSize(new Dimension(120, 20));
        c.gridx = 1;
        c.gridy = 0;
        addVehicle.add(typeTF, c);

        JLabel makeLable = new JLabel("Vehicle Make:");
        makeLable.setPreferredSize(new Dimension(120, 20));
        c.gridx = 0;
        c.gridy = 1;
        addVehicle.add(makeLable, c);

        JTextField makeTF = new JTextField();
        makeTF.setPreferredSize(new Dimension(120, 20));
        c.gridx = 1;
        c.gridy = 1;
        addVehicle.add(makeTF, c);

        JLabel modelLable = new JLabel("Vehicle Model:");
        modelLable.setPreferredSize(new Dimension(120, 20));
        c.gridx = 0;
        c.gridy = 2;
        addVehicle.add(modelLable, c);

        JTextField modelTF = new JTextField();
        modelTF.setPreferredSize(new Dimension(120, 20));
        c.gridx = 1;
        c.gridy = 2;
        addVehicle.add(modelTF, c);

        JLabel yearLable = new JLabel("Year:");
        yearLable.setPreferredSize(new Dimension(120, 20));
        c.gridx = 0;
        c.gridy = 3;
        addVehicle.add(yearLable, c);

        JTextField yearTF = new JTextField();
        yearTF.setPreferredSize(new Dimension(120, 20));
        c.gridx = 1;
        c.gridy = 3;
        addVehicle.add(yearTF, c);

        JLabel priceLable = new JLabel("Price:");
        priceLable.setPreferredSize(new Dimension(120, 20));
        c.gridx = 0;
        c.gridy = 4;
        addVehicle.add(priceLable, c);

        JTextField priceTF = new JTextField();
        priceTF.setPreferredSize(new Dimension(120, 20));
        c.gridx = 1;
        c.gridy = 4;
        addVehicle.add(priceTF, c);

        JButton addVehicleButton = new JButton("Add Vehicle");
        addVehicleButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    double vehiclePrice = Double.parseDouble(priceTF.getText());
                    int vehicleYear = Integer.parseInt(yearTF.getText());

                    Vehicle toAdd = new Vehicle(typeTF.getText(), makeTF.getText(),
                            modelTF.getText(), vehicleYear, vehiclePrice);
                    available.add(toAdd);
                } catch (Exception exc) {
                    JOptionPane.showMessageDialog(new JFrame(), "Bad value of year or price!", "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        c.gridx = 1;
        c.gridy = 5;
        addVehicle.add(addVehicleButton, c);

        tabs.add("Add Vehicle", addVehicle);

        // buyVehicel tab
        buyVehicel.setLayout(new GridBagLayout());

        JTextArea availableTA = new JTextArea();
        availableTA.setLineWrap(true);
        JScrollPane availableSP = new JScrollPane(availableTA);
        availableSP.setPreferredSize(new Dimension(240, 200));

        JButton showAvaliable = new JButton("List available vehicles");
        showAvaliable.setPreferredSize(new Dimension(240, 20));
        showAvaliable.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (available.isEmpty()) {
                    availableTA.setText("The list is empty");
                } else {
                    availableTA.setText(getAvailable());
                }
            }
        });

        c.gridx = 0;
        c.gridy = 0;
        buyVehicel.add(showAvaliable, c);

        c.gridx = 0;
        c.gridy = 1;
        buyVehicel.add(availableSP, c);

        tabs.add("Buy Vehicle", buyVehicel);
        pane.add(tabs);
    }

    private String getAvailable() {
        String out = "";
        for (Vehicle v : available) {
            out += v + "\n";
        }
        return out;
    }

}

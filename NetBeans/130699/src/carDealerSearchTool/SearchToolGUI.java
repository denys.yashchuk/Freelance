package carDealerSearchTool;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SearchToolGUI extends JFrame {

    Car[] cars;

    public SearchToolGUI(Car[] cars) {
        this.cars = cars; 
        setMinimumSize(new Dimension(650, 300)); // sets the size of window
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // closes the program, when the window is closed
        setTitle("Search Tool"); // sets the title
        addComponentsToPane(getContentPane()); // adds components to container
        setResizable(false); // the window is not resizable
        setVisible(true); // shows the window
    }

    private void addComponentsToPane(Container pane) {
        pane.setLayout(new GridBagLayout()); // sets layout to the container
        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets(5, 5, 5, 5); // sets insets

        JLabel makeLabel = new JLabel("Make:"); // creates label "Make:"
        makeLabel.setPreferredSize(new Dimension(40, 20)); // sets the size of the label
        c.gridx = 0; // sets x coordinate to the element
        c.gridy = 0; // sets y coordinate to the element
        pane.add(makeLabel, c); // adds element to the container

        JComboBox makeCB = new JComboBox(); // creates drop down menu
        makeCB.setPreferredSize(new Dimension(80, 20));
        // next loop adds different elements to the drop down menu
        for (int i = 0; i < cars.length; i++) {
            boolean isEqual = false;
            for (int j = i; j >= 0; j--) {
                if (i != j && cars[i].getMake().equals(cars[j].getMake())) {
                    isEqual = true;
                }
            }
            if (!isEqual) {
                makeCB.addItem(cars[i].getMake());
            }
        }

        c.gridx = 1;
        c.gridy = 0;
        pane.add(makeCB, c);

        JLabel modelLabel = new JLabel("Model:");
        modelLabel.setPreferredSize(new Dimension(60, 20));
        c.gridx = 2;
        c.gridy = 0;
        pane.add(modelLabel, c);

        JComboBox modelCB = new JComboBox();
        modelCB.setSize(new Dimension(60, 20));
        for (int i = 0; i < cars.length; i++) {
            boolean isEqual = false;
            if (cars[i].getMake().equals(makeCB.getSelectedItem())) {
                for (int j = i; j >= 0; j--) {
                    if (i != j && cars[i].getModel().equals(cars[j].getModel())) {
                        isEqual = true;
                    }
                }
                if (!isEqual) {
                    modelCB.addItem(cars[i].getModel());
                }
            }
        }
        c.gridx = 3;
        c.gridy = 0;
        pane.add(modelCB, c);

        // next listener changes the content of model drop down menu, when make drop down menu changes;
        makeCB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                modelCB.removeAllItems(); // removes all elements from model drop down menu;
                // adds adds different elements to the drop down menu
                for (int i = 0; i < cars.length; i++) {
                    boolean isEqual = false;
                    if (cars[i].getMake().equals(makeCB.getSelectedItem())) {
                        for (int j = i; j >= 0; j--) {
                            if (i != j && cars[i].getModel().equals(cars[j].getModel())) {
                                isEqual = true;
                            }
                        }
                        if (!isEqual) {
                            modelCB.addItem(cars[i].getModel());
                        }
                    }
                }
            }
        });

        JLabel maxPriceLabel = new JLabel("Maximum price:");
        maxPriceLabel.setPreferredSize(new Dimension(100, 20));
        c.gridx = 4;
        c.gridy = 0;
        pane.add(maxPriceLabel, c);

        JTextField maxPriceTF = new JTextField();
        maxPriceTF.setPreferredSize(new Dimension(100, 20));
        c.gridx = 5;
        c.gridy = 0;
        pane.add(maxPriceTF, c);

        JButton search = new JButton("Search");
        search.setPreferredSize(new Dimension(80, 20));
        c.gridx = 6;
        c.gridy = 0;
        pane.add(search, c);

        JTextArea list = new JTextArea();
        list.setLineWrap(true); // sets line wrapping
        JScrollPane listSP = new JScrollPane(list); // adds text area to scroll pane for scrolling 
        listSP.setPreferredSize(new Dimension(460, 200));
        c.gridx = 0;
        c.gridy = 1;
        c.gridwidth = 7; // this element has the width of 7 cells
        pane.add(listSP, c);

        // adds action listener to button.
        search.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // adds the list of  all the cars that met all the criteria
                boolean find = false;
                String add = "";
                String make = (String) makeCB.getSelectedItem();
                String model = (String) modelCB.getSelectedItem();
                try {
                    double price = Double.parseDouble(maxPriceTF.getText());
                    for (Car car : cars) {
                        if (car.getMake().equals(make)
                                && car.getModel().equals(model)
                                && car.getPrice() <= price) {
                            find = true;
                            add += car + "\n";
                        }
                    }
                } catch (Exception exc) {
                    // shows arror message when the exception occurs 
                    JOptionPane.showMessageDialog(new JFrame(), "Bad value of price!", "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
                if(!find) add += "No cars are available!";
                list.setText(add);
            }
        });

    }

}

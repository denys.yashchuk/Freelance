package carDealerSearchTool;

public class Car {

    private String make;
    private String model;
    private Double price;

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }

    public Double getPrice() {
        return price;
    }

    public Car(String make, String model, Double price) {
        this.make = make;
        this.model = model;
        this.price = price;
    }

    @Override
    public String toString() {
        return make + " " + model + " price = $" + price;
    }

}

package reservation;

public class Reservation {

    Destination[] dataBase;

    public Reservation(Destination[] dataBase) {
        this.dataBase = dataBase;
    }

    public String addReservation(String personName, String destination, String date) {
        int destIndex = findDestIndex(destination);
        if (destIndex == -1) {
            return "No reservation is available for that date";
        } else {
            int dateIndex = dataBase[destIndex].findDateIndex(date);
            if (dateIndex == -1) {
                return "No reservation is available for that date";
            } else {
                String[][] reservation = dataBase[destIndex].getReservation();
                for (int i = 1; i < reservation[dateIndex].length; i++) {
                    if (reservation[dateIndex][i].equals("free")) {
                        reservation[dateIndex][i] = personName;
                        return "Your reservation number for the trip\nto "
                                + destination + "\non " + date + "\nis " + i + ".";
                    }
                }
                return "No reservation is available for that date";
            }
        }
    }

    private int findDestIndex(String destination) {
        int index = 0;
        while (index < dataBase.length) {
            if (dataBase[index].getDestination().equals(destination.toLowerCase())) {
                break;
            } else {
                index++;
            }
        }
        if (index == dataBase.length) {
            return -1;
        } else {
            return index;
        }
    }

}

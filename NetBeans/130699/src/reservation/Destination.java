package reservation;

public class Destination {

    String destination;
    String[][] reservation;

    public Destination(String destination, String[][] reservation) {
        this.destination = destination.toLowerCase();
        this.reservation = reservation;
    }

    public String getDestination() {
        return destination;
    }

    public String[][] getReservation() {
        return reservation;
    }

    public int findDateIndex(String date) {
        int index = 0;
        while (index < reservation.length) {
            if (reservation[index][0].equals(date)) {
                break;
            } else {
                index++;
            }
        }
        if (index == reservation.length) {
            return -1;
        } else {
            return index;
        }
    }

}

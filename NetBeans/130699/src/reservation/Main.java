package reservation;

public class Main {

    public static void main(String[] args) {
        String[][] res = {
            {"11.09.2001", "free", "Jack"},
            {"12.03.2002", "Diana", "Noah"}
        };
        String[][] res2 = {
            {"11.03.2002", "Jacob", "William"},
            {"12.03.2002", "Mia", "free"}
        };
        Destination[] d = new Destination[2];
        d[0] = new Destination("New York", res);
        d[1] = new Destination("Paris", res2);
        ReservationGUI r = new ReservationGUI(d);
    }

}

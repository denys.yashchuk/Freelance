package reservation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.Pattern;

public class ReservationGUI extends JFrame {

    Reservation reservationBase;

    public ReservationGUI(Destination[] dataBase) {
        this.reservationBase = new Reservation(dataBase);
        setMinimumSize(new Dimension(300, 300));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Reservation");
        addComponentsToPane(getContentPane());
        setResizable(false);
        setVisible(true);
    }

    private void addComponentsToPane(Container pane) {
        pane.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets(5, 5, 5, 5);

        JLabel name = new JLabel("Your Name:");
        name.setPreferredSize(new Dimension(80, 20));
        c.gridx = 0;
        c.gridy = 0;
        pane.add(name, c);

        JTextField nameTF = new JTextField();
        nameTF.setPreferredSize(new Dimension(160, 20));
        c.gridx = 1;
        c.gridy = 0;
        pane.add(nameTF, c);
        
        JLabel destination = new JLabel("Destination:");
        destination.setPreferredSize(new Dimension(80, 20));
        c.gridx = 0;
        c.gridy = 1;
        pane.add(destination, c);

        JTextField destinationTF = new JTextField();
        destinationTF.setPreferredSize(new Dimension(160, 20));
        c.gridx = 1;
        c.gridy = 1;
        pane.add(destinationTF, c);

        JLabel date = new JLabel("Date:");
        date.setPreferredSize(new Dimension(80, 20));
        c.gridx = 0;
        c.gridy = 2;
        pane.add(date, c);

        JTextField dateTF = new JTextField("DD.MM.YYYY");
        dateTF.setPreferredSize(new Dimension(160, 20));
        c.gridx = 1;
        c.gridy = 2;
        pane.add(dateTF, c);        

        JButton reserve = new JButton("Reserve");
        reserve.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String name = nameTF.getText();
                String destination = destinationTF.getText();
                String date = dateTF.getText();
                String datePattern = "^[0-9][0-9].[0-9][0-9].[0-9][0-9][0-9][0-9]";
                if(!date.matches(datePattern)){
                    showAnswer("Write right date value in format (DD.MM.YYY)");
                }
                else{
                    showAnswer(reservationBase.addReservation(name, destination, date));
                }
            }
        });
        c.gridx = 0;
        c.gridy = 3;
        c.gridwidth = 2;
        pane.add(reserve, c);
    }

    private JFrame showAnswer(String s) {
        JFrame frame = new JFrame();
        frame.setMinimumSize(new Dimension(300, 120));
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setTitle("Reservation");
        frame.setVisible(true);

        JTextArea r = new JTextArea();
        r.setLineWrap(true);
        r.setEnabled(false);
        r.setText(s);
        JScrollPane rSP = new JScrollPane(r);
        rSP.setPreferredSize(new Dimension(300, 120));

        frame.getContentPane().add(rSP);

        return frame;
    }

}

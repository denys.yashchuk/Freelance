package Question1;

public class Car extends Vehicle {

    private int tankCapacity;

    public Car(String make, String model, int maxSpeed, int tankCapacity) {
        super(make, model, maxSpeed);
        this.tankCapacity = tankCapacity;
    }

    public int getTankCapacity() {
        return tankCapacity;
    }

    public void setTankCapacity(int tankCapacity) {
        this.tankCapacity = tankCapacity;
    }

    @Override
    public String toString() {
        return "Car: " + super.toString() + " tankCapacity=" + tankCapacity+" L";
    }

}

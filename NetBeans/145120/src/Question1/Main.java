package Question1;

public class Main {

    public static void main(String[] args){
        Vehicle[] vehicles = new Vehicle[4];
        vehicles[0] = new Car("Ford", "Focus", 220, 40);
        vehicles[1] = new Car("Ferrari", "488 GTB", 330, 78);
        vehicles[2] = new Bus("Mersedes", "Vito", 200, 18);
        vehicles[3] = new Bus("Tomas", "SAF-T-LINER C2", 160, 81);
        vehicles[3].setMaxSpeed(180);
        Car ford = (Car) vehicles[0];
        ford.setTankCapacity(35);
        for(Vehicle v : vehicles){
            System.out.println(v);
        }
    }
    
}

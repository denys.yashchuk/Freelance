package Question1;

public class Bus extends Vehicle {

    private int numberOfSeats;

    public Bus(String make, String model, int maxSpeed, int numberOfSeats) {
        super(make, model, maxSpeed);
        this.numberOfSeats = numberOfSeats;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(int numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    @Override
    public String toString() {
        return "Bus: " + super.toString() + " numberOfSeats=" + numberOfSeats;
    }

}

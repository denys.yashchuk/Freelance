package Question2;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class GamePlay {

    private Random r;
    private ArrayList<Integer> choices;
    private Scanner sc;

    public GamePlay() {
        r = new Random();
        choices = new ArrayList<Integer>();
        sc = new Scanner(System.in);
    }

    public void play() {
        boolean play = true;
        int numberOfGames = 0;
        System.out.println("Welcome to the game");
        while (play) {
            choices.clear();
            int secretNumber = r.nextInt(9) + 1;
            int choice;
            numberOfGames++;
            System.out.print("Enter your choice: ");
            choice = sc.nextInt();
            choices.add(choice);
            while (choice != secretNumber) {
                System.out.println("Try again!");
                System.out.print("Enter your choice: ");
                choice = sc.nextInt();
                choices.add(choice);
            }
            System.out.println("You are right. The number is " + choice + ".\nYour choices: ");
            choices.stream().forEach((i) -> {
                System.out.print(i + " ");
            });
            if (numberOfGames <= 2) {
                System.out.println("Play again?(y/n): ");
                if (sc.next().toLowerCase().equals("y")) {
                    play = true;
                } else {
                    play = false;
                }
            } else {
                play = false;
            }
        }
    }

}

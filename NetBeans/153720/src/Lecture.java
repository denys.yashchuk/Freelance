
public class Lecture extends Event {

    public Lecture(Time startTime, String title) {
        super(startTime, 60, title);
    }

    @Override
    public String toString() {
        return "Lecture: " + super.toString();
    }

}


import java.util.Scanner;

public class Application {

    private static Scheduler s;
    private static boolean done;
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Welcome to Scheduler Application !");
        s = new Scheduler();
        while (!done) {
            showMainMenu();
            makeMainChoice();
        }
    }

    private static void showMainMenu() {
        System.out.println(">> MAIN MENU <<");
        System.out.println("1. Add an event");
        System.out.println("2. Display events");
        System.out.println("3. Schedule");
        System.out.println("4. Exit");
        System.out.print(">> Select your option (1-4): ");
    }

    private static void makeMainChoice() {
        String choice = sc.nextLine();
        System.out.println("");
        switch (choice) {
            case "1":
                addEvent();
                break;
            case "2":
                System.out.println(s.showEvents());
                break;
            case "3":
                s.clearSchedule();
                s.schedule();
                System.out.println(s.showSchedule());
                break;
            case "4":
                done = true;
                break;
            default:
                System.out.print("Wrong choice!!!\nTry again: ");
                makeMainChoice();
                break;
        }
    }

    private static void addEvent() {
        System.out.println(">> EVENT MENU <<");
        System.out.println("1. Add Talk");
        System.out.println("2. Add Lecture");
        System.out.println("3. Return to Main Menu");
        System.out.print(">> Select your option (1-3): ");

        String choice = sc.nextLine();
        System.out.println("");

        switch (choice) {
            case "1":
                addTalk();
                break;
            case "2":
                addLecture();
                break;
            case "3":
                break;
            default:
                System.out.print("Wrong choice!!!\nTry again: ");
                makeMainChoice();
                break;
        }
    }

    private static void addTalk() {
        System.out.print("Enter the title of the talk: ");
        String title = sc.nextLine();
        System.out.print("\nEnter the start time\nHours (0-23): ");
        String hours = sc.nextLine();
        System.out.print("Minute (0-59): ");
        String minutes = sc.nextLine();
        System.out.print("\nEnter talk's duration (in minutes): ");
        String duration = sc.nextLine();

        try {
            int h = Integer.parseInt(hours);
            int m = Integer.parseInt(minutes);
            int d = Integer.parseInt(duration);
            s.addEvent(new Talk(new Time(h, m), d, title));
            System.out.println("");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal values: " + ex.getMessage()+"\n");
        }
    }

    private static void addLecture() {
        System.out.print("Enter the title of the lecture: ");
        String title = sc.nextLine();
        System.out.print("\nEnter the start time\nHours (0-23): ");
        String hours = sc.nextLine();
        System.out.print("Minute (0-59): ");
        String minutes = sc.nextLine();

        try {
            int h = Integer.parseInt(hours);
            int m = Integer.parseInt(minutes);
            s.addEvent(new Lecture(new Time(h, m), title));
            System.out.println("");
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal values: " + ex.getMessage()+"\n");
        }
    }

}

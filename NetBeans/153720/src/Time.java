
public class Time implements Comparable<Time> {

    private int hours;
    private int minutes;

    public Time(int hours, int minutes) throws IllegalArgumentException {
        if (hours < 0 || hours > 24 || minutes < 0 || minutes > 59) {
            throw new IllegalArgumentException("0 <= hours <= 23; 0 <= minutes <= 59");
        } else {
            this.hours = hours;
            this.minutes = minutes;
        }

    }

    public Time timeAfter(int min) {
        int newHours = this.hours + min / 60;
        int newMinutes = this.minutes + min % 60;
        if (newMinutes > 59) {
            newHours++;
            newMinutes = newMinutes % 60;
        }
        return new Time(newHours, newMinutes);
    }

    @Override
    public String toString() {
        return (hours > 12 ? hours % 12 + ":" + (minutes < 10 ? "0" : "") + minutes + " PM"
                : hours + ":" + (minutes < 10 ? "0" : "") + minutes + " AM");
    }

    @Override
    public int compareTo(Time o) {
        if (this.hours == o.hours && this.minutes == o.minutes) {
            return 0;
        } else if (this.hours > o.hours) {
            return 1;
        } else if (this.hours == o.hours && this.minutes > o.minutes) {
            return 1;
        } else {
            return -1;
        }
    }

}


public class Talk extends Event {

    public Talk(Time startTime, int duration, String title) {
        super(startTime, duration, title);
    }

    @Override
    public String toString() {
        return "Talk: " + super.toString();
    }

}


import java.util.ArrayList;

public class Scheduler {

    private ArrayList<Event> schedule;
    private ArrayList<Event> events;

    public Scheduler() {
        this.events = new ArrayList<Event>();
        this.schedule = new ArrayList<Event>();
    }

    public void addEvent(Event e) {
        events.add(e);
    }

    public void schedule() {
        sortEvents();
        schedule.add(events.get(0));
        for (int i = 1; i < events.size(); i++) {
            if (events.get(i).getStartTime().compareTo(schedule.get(schedule.size() - 1).getEndTime()) > 0) {
                schedule.add(events.get(i));
            }
        }
    }

    public String showEvents() {
        String out = "";
        for (Event e : events) {
            out += e + "\n";
        }
        return out;
    }

    public String showSchedule() {
        String out = "";
        for (Event e : schedule) {
            out += e + "\n";
        }
        return out;
    }

    private void sortEvents() {
        for (int i = 0; i < events.size() - 1; i++) {
            for (int j = i + 1; j < events.size(); j++) {
                if (events.get(j).compareTo(events.get(i)) < 0) {
                    Event tmp = events.get(i);
                    events.set(i, events.get(j));
                    events.set(j, tmp);
                }
            }
        }
    }
    
    public void clearSchedule(){
        schedule.clear();
    }

}

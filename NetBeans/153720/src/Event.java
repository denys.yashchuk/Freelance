    
public class Event implements Comparable<Event> {

    private Time startTime;
    private Time endTime;
    private String title;

    public Event(Time startTime, int duration, String title) {
        this.startTime = startTime;
        this.endTime = startTime.timeAfter(duration);
        this.title = title;
    }

    public Time getStartTime() {
        return startTime;
    }

    public Time getEndTime() {
        return endTime;
    }

    @Override
    public String toString() {
        return title + " (" + startTime + " to " + endTime + ")";
    }

    @Override
    public int compareTo(Event o) {
        return this.endTime.compareTo(o.endTime);
    }

}

/*
 * Assignment No.: 1
 * Your Name:
 * Your Id:
 * Submission date:
 * Instructor’s name: Syed Tanbeer
*/

public class Main {
    
    public static void main(String[] args){
        Game game = new Game();
        System.out.println("Card Game simulation by <Your Name (ID)>");
        game.play();
    }
    
}

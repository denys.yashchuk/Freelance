
import java.util.LinkedList;
import java.util.Queue;

/*
 * Assignment No.: 1
 * Your Name:
 * Your Id:
 * Submission date:
 * Instructor’s name: Syed Tanbeer
*/

public class Player {
    
    private Queue cards;

    public Player() {
        cards = new LinkedList();
    }
    
    public void addCard(Card card){
        cards.add(card);
    }
    
    public Card getCard(){
        return (Card) cards.remove();
    }
    
    public Object[] showCard(){
        return cards.toArray();
    }
}

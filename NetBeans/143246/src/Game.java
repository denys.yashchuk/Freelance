
import java.util.Random;

/*
 * Assignment No.: 1
 * Your Name:
 * Your Id:
 * Submission date:
 * Instructor’s name: Syed Tanbeer
 */
public class Game {

    private Card[] deck;
    private Player[] players;
    private Card[] deal;
    private int[] wins;

    public Game() {
        players = new Player[4];
        for (int player = 0; player < 4; player++) {
            players[player] = new Player();
        }
        deck = new Card[52];
        for (int suit = 0; suit < 4; suit++) {
            for (int cardFace = 2; cardFace < 15; cardFace++) {
                deck[suit * 13 + (cardFace - 2)] = new Card(suit, cardFace);
            }
        }
        deal = new Card[4];
        wins = new int[4];
    }

    public void shuffle() {
        Random r = new Random();
        for (int i = deck.length - 1; i > 0; i--) {
            int j = r.nextInt(i + 1);
            Card tmp = deck[j];
            deck[j] = deck[i];
            deck[i] = tmp;
        }
    }

    public void distribute() {
        for (int i = 0; i < deck.length; i++) {
            players[i % 4].addCard(deck[i]);
        }
    }

    public void playDeal() {
        for (int i = 0; i < 4; i++) {
            deal[i] = players[i].getCard();
        }
    }

    public void findDealWinner() {
        int winner = 0;
        for (int i = 1; i < 4; i++) {
            if (deal[winner].compareTo(deal[i]) < 0) {
                winner = i;
            }
        }
        wins[winner]++;
    }
    
    public void getWinner(){
        System.out.println("Deal winers:\n"
                + "Player:\tNo. of Deal(s) Won");
        for(int i = 1; i <=4; i++){
            System.out.println(i+":\t"+wins[i-1]);
        }
        int winner = 0;
        for (int i = 1; i < 4; i++) {
            if (wins[winner]<wins[i]) {
                winner = i;
            }
        }
        System.out.print("Overall winner(s): Player-"+(winner+1));
        for(int i = 0; i<4; i++){
            if(i!=winner&&wins[i]==wins[winner])
                System.out.print(", Player-"+(i+1));
        }
    }
    
    public void play(){
        shuffle();
        distribute();
        showPlayersCards();
        System.out.println("Status of all deals after 13:");
        for(int i = 0; i < 13; i++){
            playDeal();
            showDeal(i+1);
            findDealWinner();
        }
        System.out.println("");
        getWinner();
    }
    
    public void showDeal(int i){
        System.out.format("Deal number %02d: ",i);
        for(int j = 0; j<3; j++)
            System.out.print(deal[j]+", ");
        System.out.println(deal[3]);
    }
    
    public void showPlayersCards() {
        System.out.println("Distribution of cards:");
        for (int i = 0; i < 4; i++) {
            System.out.println("Cards for player " + (i+1));
            for (int j = 0; j < 13; j++) {
                System.out.println((Card)players[i].showCard()[j]);
            }
            System.out.println("");
        }
    }

}

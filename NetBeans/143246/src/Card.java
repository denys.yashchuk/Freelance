/*
 * Assignment No.: 1
 * Your Name:
 * Your Id:
 * Submission date:
 * Instructor’s name: Syed Tanbeer
*/

public class Card {

    private int suit;
    private int cardFace;

    public Card(int suit, int cardFace) {
        this.suit = suit;
        this.cardFace = cardFace;
    }

    public int compareTo(Card otherCard){
        if(this.cardFace - otherCard.cardFace != 0)
            return this.cardFace - otherCard.cardFace;
        else
            return this.suit - otherCard.suit;
    }
    
    @Override
    public String toString() {
        return suit + " " + cardFace;
    }
    
    
    
}

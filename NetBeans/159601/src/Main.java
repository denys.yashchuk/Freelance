
import java.util.Scanner;

public class Main {

    static Scanner sc = new Scanner(System.in);
    static Theater[] theaters;

    public static void main(String[] args) {
        createTheaters();
        int index;
        boolean work = true;
        do {
            showMenu();
            switch (sc.nextLine()) {
                case "a":
                    for (int i = 0; i < theaters.length; i++) {
                        System.out.println((i + 1) + ". " + theaters[i].getName());
                    }
                    System.out.print("Enter the number of Theater: ");
                    index = sc.nextInt() - 1;
                    sc.nextLine();
                    theaters[index].scheduleTime(sc);
                    break;
                case "b":
                    for (int i = 0; i < theaters.length; i++) {
                        System.out.println((i + 1) + ". " + theaters[i].getName());
                    }
                    System.out.print("Enter the number of Theater: ");
                    index = sc.nextInt() - 1;
                    sc.nextLine();
                    System.out.print("Enter the name and rating of the movie in format(NAME&RATING): ");
                    theaters[index].scheduleOneMovieAllDay(sc.nextLine());
                    break;
                case "c":
                    for (int i = 0; i < theaters.length; i++) {
                        System.out.println((i + 1) + ". " + theaters[i].getName());
                    }
                    System.out.print("Enter the number of Theater: ");
                    index = sc.nextInt() - 1;
                    sc.nextLine();
                    theaters[index].scheduleForEverySingleTime(sc);
                    break;
                case "d":
                    for (int i = 0; i < theaters.length; i++) {
                        System.out.println((i + 1) + ". " + theaters[i].getName());
                    }
                    System.out.print("Enter the number of Theater: ");
                    index = sc.nextInt() - 1;
                    sc.nextLine();
                    theaters[index].clear();
                    break;
                case "e":
                    System.out.print("Enter movie name: ");
                    String name = sc.nextLine();
                    for (Theater t : theaters) {
                        String res = t.search(name);
                        if (!res.equals("")) {
                            System.out.println(res);
                        }
                    }
                    break;
                case "f":
                    System.out.print("Enter minimum rating: ");
                    Double raiting = Double.parseDouble(sc.nextLine());
                    for (Theater t : theaters) {
                        String res = t.search(raiting);
                        if (!res.equals("")) {
                            System.out.println(res);
                        }
                    }
                    break;
                case "q":
                    work = false;
                    break;
                default:
                    System.out.println("Make right choice!!!");
                    break;
            }
        } while (work);
    }

    static void showMenu() {
        System.out.println("a. Schedule a specific theater and time\n"
                + "b. Schedule a theater for one movie all day\n"
                + "c. Schedule a movie for every single time\n"
                + "d. Clear out a theater\n"
                + "e. Search for all movie times for a specific movie\n"
                + "f. Search for all movies of a specific rating\n"
                + "q. Quit");
    }

    static void createTheaters() {
        theaters = new Theater[6];
        theaters[0] = new Theater("Olympia Music Hall");
        theaters[1] = new Theater("Sci-fi Dine-in Theater");
        theaters[2] = new Theater("Urania National Film Theatre");
        theaters[3] = new Theater("Kurshumli An In Skopje");
        theaters[4] = new Theater("Fox Theater");
        theaters[5] = new Theater("Hot Tube Cinema");
    }
}


import java.util.Scanner;

public class Theater {

    private String name;
    private String[][] schedule;

    public Theater(String name) {
        this.name = name;
        clear();
    }

    public String getName() {
        return name;
    }

    public void scheduleTime(Scanner sc) {
        for (int i = 0; i < schedule.length; i++) {
            System.out.println((i + 1) + ". " + schedule[i][0]);
        }
        System.out.print("Enter the number of time: ");
        int index = sc.nextInt() - 1;
        sc.nextLine();
        System.out.print("Enter the name and rating of the movie in format(NAME&RATING): ");
        schedule[index][1] = sc.nextLine();
    }

    public void scheduleOneMovieAllDay(String movie) {
        schedule = new String[1][2];
        schedule[0][0] = "All day";
        schedule[0][1] = movie;
    }

    public void scheduleForEverySingleTime(Scanner sc) {
        for (int i = 0; i < schedule.length; i++) {
            System.out.println("Enter movie for " + schedule[i][0]);
            schedule[i][1] = sc.nextLine();
        }
    }

    public void clear() {
        schedule = new String[5][2];
        for (int i = 12; i < 21; i = i + 2) {
            schedule[i / 2 - 6][0] = i + ":00";
        }
    }

    public String search(String movieName) {
        String out = "";
        for (int i = 0; i < schedule.length; i++) {
            if (schedule[i][1] != null && schedule[i][1].split("&")[0].equals(movieName)) {
                out += name + ": " + schedule[i][0] + "\n";
            }
        }
        return out;
    }

    public String search(double raiting) {
        String out = "";
        for (int i = 0; i < schedule.length; i++) {
            if (schedule[i][1] != null) {
                System.out.println(schedule[i][1].split("|")[1]);
                double movieRaiting = Double.parseDouble(schedule[i][1].split("&")[1]);
                if (movieRaiting >= raiting) {
                    out += name + ": " + schedule[i][0] + " " + schedule[i][1] + "\n";
                }
            }
        }
        return out;
    }

}

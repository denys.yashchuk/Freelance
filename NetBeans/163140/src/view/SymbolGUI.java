package GUI;

import static game.Constants.*;
import game.Symbol;
import javafx.scene.Node;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class SymbolGUI extends StackPane {

    private int x, y;

    public SymbolGUI(int x, int y, game.Symbol symbol) {
        this.x = x;
        this.y = y;
        Rectangle border = new Rectangle(SQUARE_SIZE - 2, SQUARE_SIZE - 2);
        border.setStroke(Color.GREY);
        border.setFill(Color.LIGHTGRAY);
        getChildren().add(border);
        Node symb = getSymbol(symbol);
        if (symb != null) {
            getChildren().add(getSymbol(symbol));
        }
        setTranslateX(x * SQUARE_SIZE + 5);
        setTranslateY(y * SQUARE_SIZE + 5);
    }
    
    public SymbolGUI(game.Symbol symbol) {
        Rectangle border = new Rectangle(SQUARE_SIZE - 2, SQUARE_SIZE - 2);
        border.setStroke(Color.GREY);
        border.setFill(Color.LIGHTGRAY);
        getChildren().add(border);
        Node symb = getSymbol(symbol);
        if (symb != null) {
            getChildren().add(getSymbol(symbol));
        }
    }

    public Node getSymbol(Symbol symbol) {
        switch (symbol.getNum()) {
            case 1:
                Rectangle yellowSquare = new Rectangle(SQUARE_SIZE - 10, SQUARE_SIZE - 10);
                yellowSquare.setFill(Color.YELLOW);
                return yellowSquare;
            case 2:
                Circle orangeRing = new Circle(SQUARE_SIZE / 2 - 5);
                orangeRing.setStroke(Color.ORANGE);
                orangeRing.setStrokeWidth(3);
                orangeRing.setFill(Color.LIGHTGRAY);
                orangeRing.setCenterX(15);
                orangeRing.setCenterY(15);
                return orangeRing;
            case 3:
                Text redX = new Text();
                redX.setText("X");
                redX.setFont(Font.font(null, FontWeight.BOLD, 20));
                redX.setFill(Color.RED);
                return redX;
            case 4:
                Text greenFlower = new Text();
                greenFlower.setText("\u273F");
                greenFlower.setFont(Font.font(20));
                greenFlower.setFill(Color.GREEN);
                return greenFlower;
            case 5:
                Polygon blueRomb = new Polygon();
                blueRomb.getPoints().addAll(new Double[]{
                    13.0, 0.0,
                    0.0, 13.0,
                    13.0, 26.0,
                    26.0, 13.0
                });
                blueRomb.setFill(Color.BLUE);
                return blueRomb;
            case 6:
                Text cyanCross = new Text();
                cyanCross.setText("\u271D");
                cyanCross.setFont(Font.font(null, FontWeight.BOLD, 20));
                cyanCross.setFill(Color.CYAN);
                return cyanCross;
            case 7:
                Circle whiteCircle = new Circle(SQUARE_SIZE / 2 - 5);
                whiteCircle.setStrokeWidth(3);
                whiteCircle.setFill(Color.WHITE);
                whiteCircle.setCenterX(15);
                whiteCircle.setCenterY(15);
                return whiteCircle;
            default:
                return null;
        }
    }

}

package GUI;

import game.Board;
import static game.Constants.*;
import game.Game;
import game.Symbol;
import game.Tile;
import java.util.List;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Start extends Application {

    private SymbolGUI[][] boardGUI = new SymbolGUI[ROWS][COLS];
    private static Game game;
    private int playerNumber = 1;
    
    public static void main(String[] args) {
        Board board = new Board();
        game = new Game(board);
        launch(args);
    }

    private Parent createContent() {
        Pane root = new Pane();
        root.setPrefSize(WINDOW_W+200, WINDOW_H+200);
        root.setStyle("-fx-background-color: #ECEFF1");
        
        Text playerNumberText = new Text();
        playerNumberText.setText("Player #"+playerNumber);
        playerNumberText.setFont(Font.font(36));
        playerNumberText.setTranslateX(10);
        playerNumberText.setTranslateY(40);

        Text instructionText = new Text();
        instructionText.setText("Select tile from Tile Board, then select 2 "
                + "symbols from the Game Board and click \"Move\" button.");
        instructionText.setFont(Font.font(16)); 
        instructionText.setTranslateX(10);
        instructionText.setTranslateY(playerNumberText.getTranslateY() + 46);
        
        // Create Game Board
        Pane gameBoard = new Pane();
        gameBoard.setPrefSize(GAME_BOARD_W, GAME_BOARD_H);
        gameBoard.setStyle("-fx-background-color: #CFD8DC");
        gameBoard.setTranslateX(10);
        gameBoard.setTranslateY(instructionText.getTranslateY() + 26);
        Symbol[][] symbols = game.getBoard().getSymbols();
        for(int x = 0; x < COLS; x++){
            for(int y = 0; y < ROWS; y++){
                SymbolGUI t = new SymbolGUI(x, y, symbols[y][x] == null ? Symbol.values()[7]:symbols[y][x]);
                boardGUI[x][y] = t;
                gameBoard.getChildren().add(t);
            }
        }
        
        // Create Player's tiles
        HBox playerTiles = new HBox();
        playerTiles.setPadding(new Insets(5, 5, 5, 5));
        playerTiles.setSpacing(5);
        playerTiles.setPrefSize(PLAYER_BOARD_W, PLAYER_BOARD_H);
        playerTiles.setStyle("-fx-background-color: #CFD8DC");
        playerTiles.setTranslateX(10);
        playerTiles.setTranslateY(GAME_BOARD_H + gameBoard.getTranslateY() + 10);
        List<Tile> playerTilesList = game.getPlayer1();
        for(Tile tile:playerTilesList){
            PlayerTile pt = new PlayerTile(tile);
            playerTiles.getChildren().add(pt);
        };
        
        root.getChildren().addAll(playerNumberText, instructionText, gameBoard, playerTiles);
        return root;
    }

    @Override
    public void start(Stage stage) {
        Scene scene = new Scene(createContent());
        stage.setTitle("GAME");
        stage.setScene(scene);
        stage.show();
    }

}

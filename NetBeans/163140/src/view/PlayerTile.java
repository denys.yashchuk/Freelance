package GUI;

import static game.Constants.*;
import javafx.geometry.Pos;
import javafx.scene.layout.HBox;

public class PlayerTile extends HBox {

    public PlayerTile(game.Tile tile) {
        setMaxSize(TILE_W, TILE_H);
        setStyle("-fx-background-color: #607D8B");
        setAlignment(Pos.CENTER);
        getChildren().addAll(new SymbolGUI(tile.getFirstSymbol()), new SymbolGUI(tile.getSecondSymbol()));
    }

}

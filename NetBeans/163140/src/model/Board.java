
package game;

import static game.Constants.COLS;
import static game.Constants.RAND;
import static game.Constants.ROWS;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Board {

    private Symbol[][] symbols;

    public Symbol[][] getSymbols() {
        return symbols;
    }

    private List<Tile> tiles;

    public Board() {
        assert (ROWS % 2 != 0 && COLS % 2 != 0); // To make sure the board has a proper shape.
        symbols = new Symbol[ROWS][COLS];
        tiles = new ArrayList<>();

        do {
            setupSymbols();

        } while (!satisfiedConditionA());

        do {
            tiles.clear();
            shuffle();
            collectTiles();

        } while (!satisfiedConditionB());

    }

    private void setupSymbols() {

        for (int row = 1, shift = 0; row <= ROWS; row++, shift = (row <= ROWS / 2 + 1 ? shift + 1 : shift - 1)) {
            for (int col = 1; col <= COLS; col++) {

                if (col >= COLS / 2 + 1 - shift && col <= COLS / 2 + 1 + shift
                        ^ (row <= COLS / 2 + 2 && row >= COLS / 2 && col == COLS / 2 + 1
                        || // Make 3 horizontal squares at the middle empty.
                        col <= COLS / 2 + 2 && col >= COLS / 2 && row == COLS / 2 + 1)) // Make 3 vertical squares at the middle empty.
                {
                    symbols[row - 1][col - 1] = Symbol.values()[RAND.nextInt(7)];
                }
                
            }
        }
    }

    private void collectTiles() {

        for (int row = 1, shift = 0; row <= ROWS; row++, shift = (row <= ROWS / 2 + 1 ? shift + 1 : shift - 1)) {
            for (int col = 1; col <= COLS; col++) {
                if (col >= COLS / 2 + 1 - shift && col <= COLS / 2 + 1 + shift
                        ^ (row <= COLS / 2 + 2 && row >= COLS / 2 && col == COLS / 2 + 1
                        || // Make 3 horizontal squares at the middle empty.
                        col <= COLS / 2 + 2 && col >= COLS / 2 && row == COLS / 2 + 1)) // Make 3 vertical squares at the middle empty.
                {
                    try {
                        if (symbols[row - 1][col - 1] != null && symbols[row][col - 1] != null) {
                            tiles.add(new Tile(symbols[row - 1][col - 1], symbols[row][col - 1])); // Down.
                        }
                    } catch (Exception exc) {
                    }

                    try {
                        if (symbols[row - 1][col - 1] != null && symbols[row - 1][col - 2] != null) {
                            tiles.add(new Tile(symbols[row - 1][col - 1], symbols[row - 1][col - 2])); // Left.
                        }
                    } catch (Exception exc) {
                    }

                    try {
                        if (symbols[row - 1][col - 1] != null && symbols[row - 2][col - 1] != null) {
                            tiles.add(new Tile(symbols[row - 1][col - 1], symbols[row - 2][col - 1])); // Up.
                        }
                    } catch (Exception exc) {
                    }

                    try {
                        if (symbols[row - 1][col - 1] != null && symbols[row - 1][col] != null) {
                            tiles.add(new Tile(symbols[row - 1][col - 1], symbols[row - 1][col])); // Right.
                        }
                    } catch (Exception exc) {
                    } // To avoid out of bounds.

                }
            }
        }

    }

    public void showBoard() {

        /*
		 * Setting up x-coord for user.
         */
        int yCoord = 0, xCoord = 0;

        for (int i = 0; i < COLS + 6; i++) {
            if (i <= 5) {
                System.out.print(" ");
            } else {
                System.out.print(" " + xCoord++ + (xCoord == 11 ? "" : " "));
            }

        }
        System.out.println("\n");
        for (int row = 1, shift = 0; row <= ROWS; row++, shift = (row <= ROWS / 2 + 1 ? shift + 1 : shift - 1)) {

            for (int col = 0; col <= COLS; col++) {
                /*
				 * Setting up y-coord for user.
                 */
                if (col == 0) {
                    System.out.print(" " + yCoord++ + (yCoord == 11 ? "" : " "));
                }

                if (col >= COLS / 2 + 1 - shift && col <= COLS / 2 + 1 + shift
                        ^ (row <= COLS / 2 + 2 && row >= COLS / 2 && col == COLS / 2 + 1
                        || // Make 3 horizontal squares at the middle empty.
                        col <= COLS / 2 + 2 && col >= COLS / 2 && row == COLS / 2 + 1)) // Make 3 vertical squares at the middle empty.
                {
                    System.out.print(symbols[row - 1][col - 1]);
                } else {
                    System.out.print("   ");
                }
            }
            System.out.println();
        }
    }

    private boolean satisfiedConditionA() {
        /*
		 * a) condition.
         */
        for (Symbol symbol : Symbol.values()) {
            if (symbol.getNum() != 8 // Avoid counting Moved 8th symbol.
                    && getTimesSymbolShown(symbol) != 8) {
                return false;
            }
        }
        return true;
    }

    private void shuffle() {
        for (int i = symbols.length - 1; i > 0; i--) {
            for (int j = symbols[i].length - 1; j > 0; j--) {
                Symbol temp = symbols[i][j];
                if (temp != null) {
                    int m, n;
                    do {
                        m = RAND.nextInt(i + 1);
                        n = RAND.nextInt(j + 1);

                    } while (symbols[m][n] == null);
                    symbols[i][j] = symbols[m][n];
                    symbols[m][n] = temp;
                }
            }
        }
    }

    private int getTimesSymbolShown(Symbol symb) {
        int times = 0;
        for (int row = 0; row < symbols.length; row++) {
            for (int col = 0; col < symbols.length; col++) {
                if (symbols[row][col] == symb) {
                    times++;
                }
            }
        }
        return times;
    }

    public List<Tile> get14TilesRandomly() {
        List<Tile> temp = new ArrayList<>(tiles);
        Collections.shuffle(temp);

        List<Tile> result = temp.subList(0, 14);

        tiles = temp;

        return result;

    }

    public boolean move(int[] moves, Tile tile) {
        if (Math.abs(moves[0] - moves[2]) > 1 || Math.abs(moves[1] - moves[3]) > 1) // To avoid select symbols which are not 
        // concatenated.
        {
            return false;
        }

        if (Math.abs(moves[0] - moves[2]) == 1 && Math.abs(moves[1] - moves[3]) == 1) // To avoid select symbols which are not 
        // domino.
        {
            return false;
        }

        if (moves[0] == moves[2] && moves[1] == moves[3]) // To avoid selection the same symbols in one place.
        {
            return false;
        }

        /*
		 * Allow using domino for both sides (1-4 == 4-1).
         */
        if (tile.getFirstSymbol() == symbols[moves[0]][moves[1]]
                && // For the
                tile.getSecondSymbol() == symbols[moves[2]][moves[3]]
                || // first side (4-1).
                tile.getSecondSymbol() == symbols[moves[0]][moves[1]]
                && // For the
                tile.getFirstSymbol() == symbols[moves[2]][moves[3]]) // second side (1-4).
        {
            symbols[moves[0]][moves[1]] = Symbol.Moved; // Clear first symbol.
            symbols[moves[2]][moves[3]] = Symbol.Moved; // Clear second symbol.
            return true;
        }
        return false;
    }

    private boolean satisfiedConditionB() {
        /*
		 * b) condition.
         */
        Map<Tile, Integer> map_tiles = new HashMap<>();
        Set<Tile> setTiles = new HashSet<>(tiles);
        for (Tile tile : setTiles) {
            if (map_tiles.containsKey(tile)) {
                int count = map_tiles.get(tile);
                map_tiles.put(tile, count + 1);
            } else {
                map_tiles.put(tile, 0);
            }
        }

        for (int i : map_tiles.values()) {
            if (i != 0 && i != 3) {
                return false;
            }
        }
        return true;

    }

}

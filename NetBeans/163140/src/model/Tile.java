
package game;

public class Tile {

    private Symbol firstSymbol;

    private Symbol secondSymbol;

    public Tile(Symbol firstSymbol, Symbol secondSymbol) {
        this.firstSymbol = firstSymbol;
        this.secondSymbol = secondSymbol;
    }

    public Tile() {

    }

    public Symbol getFirstSymbol() {
        return firstSymbol;
    }

    public void setFirstSymbol(Symbol firstSymbol) {
        this.firstSymbol = firstSymbol;
    }

    public Symbol getSecondSymbol() {
        return secondSymbol;
    }

    public void setSecondSymbol(Symbol secondSymbol) {
        this.secondSymbol = secondSymbol;
    }

    @Override
    public String toString() {
        return firstSymbol + "-" + secondSymbol;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Math.min(firstSymbol.getNum(), secondSymbol.getNum());
        result = prime * result + Math.max(firstSymbol.getNum(), secondSymbol.getNum());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Tile other = (Tile) obj;
        if (firstSymbol == other.firstSymbol && secondSymbol == other.secondSymbol
                || firstSymbol == other.secondSymbol && secondSymbol == other.firstSymbol) {
            return true;
        }
        return false;
    }

}

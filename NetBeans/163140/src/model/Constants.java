package game;

import java.util.Random;

public class Constants {

    public final static int ROWS = 11;

    public final static int COLS = 11;

    /**
     * Width and height of 1 square in pxls.
     */
    public final static int SQUARE_SIZE = 30;

    /**
     * Width and height of game window.
     */
    public static final int WINDOW_W = 14 * SQUARE_SIZE + 300;

    public static final int WINDOW_H = ROWS * SQUARE_SIZE + 30;
    
    /**
     * Width and height of game board
     */  
    public static final int GAME_BOARD_W = COLS * SQUARE_SIZE + 10;

    public static final int GAME_BOARD_H = ROWS * SQUARE_SIZE + 10;
    
    /**
     * Width and height of game board
     */
    public static final int TILE_W = SQUARE_SIZE * 2;

    public static final int TILE_H = SQUARE_SIZE + 5;
    
    /**
     * Width and height of player board
     */
    public static final int PLAYER_BOARD_W = TILE_W * 14;

    public static final int PLAYER_BOARD_H = TILE_H + 10;
    
    public final static Random RAND = new Random();

}

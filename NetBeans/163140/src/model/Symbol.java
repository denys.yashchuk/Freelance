
package game;

public enum Symbol {

    YellowSquare(1), OrangeRing(2), RedX(3), GreenFlower(4), BlueRhomb(5), CyanCross(6), WhiteCircle(7), Moved(8);

    private final int num;

    private Symbol(int num) {
        this.num = num;
    }

    @Override
    public String toString() {
        return " " + (num == 8 ? " " : num) + " ";
    }

    public int getNum() {
        return num;
    }

}

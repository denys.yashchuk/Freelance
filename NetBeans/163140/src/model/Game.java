package game;

import java.util.List;
import java.util.Scanner;

public class Game {

    private Board board;
    private List<Tile> player1;
    private List<Tile> player2;

    public Game(Board board) {
        this.board = board;
        player1 = board.get14TilesRandomly();
        player2 = board.get14TilesRandomly();
    }

    public Board getBoard() {
        return board;
    }

    public List<Tile> getPlayer1() {
        return player1;
    }

    public List<Tile> getPlayer2() {
        return player2;
    }

    public void play() {
        try (Scanner sc = new Scanner(System.in)) {

            boolean game = true;

            int attempt = 0; // Stores number of attempts to move of current player.

            while (game) {
                System.out.println("Tiles for player 1:");
                System.out.println(player1);

                while (attempt != 3) {
                    try {
                        System.out.print("Player 1: ");
                        String move = sc.nextLine();
                        String[] coords = move.split(" ");
                        int moves[] = {
                            Integer.parseInt(coords[2]),
                            Integer.parseInt(coords[1]),
                            Integer.parseInt(coords[4]),
                            Integer.parseInt(coords[3])};
                        int tilesIndex = Integer.parseInt(coords[0]);
                        if (board.move(moves, player1.get(tilesIndex))) {
                            player1.remove(tilesIndex);
                            board.showBoard();
                            break;
                        } else {
                            System.out.println("Failed");
                        }

                    } catch (Exception exc) {
                        System.out.println("Wrong move");
                    }
                    attempt++;
                } // while

                if (attempt >= 3) // Allow the user 3 attempts before losing.
                {
                    System.out.println("Player 2 win!");
                    game = false;
                    continue;
                }
                attempt = 0;

                System.out.println("Tiles for player 2:");
                System.out.println(player2);

                while (attempt != 3) {
                    try {
                        System.out.print("Player 2: ");
                        String move2 = sc.nextLine();
                        String[] coords2 = move2.split(" ");
                        int moves2[] = {
                            Integer.parseInt(coords2[2]),
                            Integer.parseInt(coords2[1]),
                            Integer.parseInt(coords2[4]),
                            Integer.parseInt(coords2[3])};
                        int tilesIndex = Integer.parseInt(coords2[0]);
                        if (board.move(moves2, player2.get(tilesIndex))) {
                            player2.remove(tilesIndex);
                            board.showBoard();
                            break;
                        } else {
                            System.out.println("Failed");
                        }
                    } catch (Exception exc) {
                        System.out.println("Wrong move");
                    }
                    attempt++;
                } // while

                if (attempt >= 3) // Allow the user 3 attempts before losing.
                {
                    System.out.println("Player 1 win!");
                    game = false;
                    continue;
                }
                attempt = 0;

            } // while game
        } // try scanner
    } // play

} // class

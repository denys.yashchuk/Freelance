

public class Main {

    public static void main(String[] args) {
        Car[] car = new Car[6]; // Creates data base of cars
        car[0] = new Car("Ford", "Mustang", 1000000.); // Adds car to data base
        car[1] = new Car("Ford", "Focus", 1000.);
        car[2] = new Car("Dodge", "Challenger", 500000.);
        car[3] = new Car("Ford", "Mustang", 1500000.);
        car[4] = new Car("Ford", "Focus", 5000.);
        car[5] = new Car("Dodge", "Challenger", 700000.);
        new SearchToolGUI(car); // Creates the GUI
    }

}

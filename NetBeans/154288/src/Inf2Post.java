
import java.util.Scanner;
import java.util.Stack;
import java.util.StringTokenizer;

public class Inf2Post {

    private Stack<String> theStack;
    private String output = "";
    private StringTokenizer st;

    public Inf2Post(String in) {
        st = new StringTokenizer(in);
        theStack = new Stack<String>();
    }

    public Inf2Post() {
        theStack = new Stack<String>();
    }

    public String doTrans() {
        while (st.hasMoreTokens()) {
            String token = st.nextToken();
            switch (token) {
                case "+":
                case "-":
                    gotOper(token, 1);
                    break;
                case "*":
                case "/":
                    gotOper(token, 2);
                    break;
                case "(":
                    theStack.push(token);
                    break;
                case ")":
                    gotParen(token);
                    break;
                default:
                    output = output + token + " ";
                    break;
            }
        }
        while (!theStack.isEmpty()) {
            if(theStack.peek().equals("(")){
                return "Extra \"(\" in the expression!";
            }
            output += theStack.pop() + " ";
        }
        return output;
    }

    public void gotOper(String opThis, int prec1) {
        while (!theStack.isEmpty()) {
            String opTop = theStack.pop();
            if (opTop.equals("(")) {
                theStack.push(opTop);
                break;
            } else {
                int prec2;
                if (opTop.equals("+") || opTop.equals("-")) {
                    prec2 = 1;
                } else {
                    prec2 = 2;
                }
                if (prec2 < prec1) {
                    theStack.push(opTop);
                    break;
                } else {
                    output = output + opTop + " ";
                }
            }
        }
        theStack.push(opThis);
    }

    public void gotParen(String ch) {
        while (!theStack.isEmpty()) {
            String chx = theStack.pop();
            if (chx.equals("(")) {
                break;
            } else {
                output = output + chx + " ";
            }
        }
    }

    public void setString(String in) {
        output = "";
        st = new StringTokenizer(in);
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String expression, answer;
        Inf2Post i2p = new Inf2Post();

        System.out.println("Please type an arithmetic expression made from");
        System.out.println("unsigned double numbers and the operations + - * /.");
        System.out.println("Numbers and operators must be separated with spaces.");

        do {
            System.out.print("Your expression: ");
            expression = sc.nextLine();
            try {
                i2p.setString(expression);
                answer = i2p.doTrans();
                System.out.println(answer);
            } catch (Exception e) {
                System.out.println("Error." + e.toString());
            }
        } while (query(sc, "Another string?"));

    }

    public static boolean query(Scanner input, String prompt) {
        String answer;

        System.out.print(prompt + " [Y or N]: ");
        answer = input.nextLine().toUpperCase();
        while (!answer.startsWith("Y") && !answer.startsWith("N")) {
            System.out.print("Invalid response. Please type Y or N: ");
            answer = input.nextLine().toUpperCase();
        }

        return answer.startsWith("Y");
    }

}

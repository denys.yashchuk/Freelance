
public class Address {

    private String street;
    private String city;
    private String state;
    private String zipCode;

    /**
     * Constructor
     * @param street
     * @param city
     * @param state
     * @param zipCode
     * @throws IllegalArgumentException if illegal argument was entered
     */
    public Address(String street, String city, String state, String zipCode) throws IllegalArgumentException {
        this.street = street;
        this.city = city;
        if (state.length() == 2) {
            this.state = state;
        } else {
            throw new IllegalArgumentException("Use only 2 characters for state");
        }
        if (zipCode.length() == 5 && zipCode.matches("[0-9]+")) {
            this.zipCode = zipCode;
        } else {
            throw new IllegalArgumentException("Use only 5 digits for zip code");
        }
    }

    /**
     * Getter
     * @return street
     */
    public String getStreet() {
        return street;
    }

    /**
     * Setter
     * @param street 
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * Getter
     * @return city
     */
    public String getCity() {
        return city;
    }

    /**
     * Setter
     * @param city 
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * Getter
     * @return state
     */
    public String getState() {
        return state;
    }

    /**
     * Setter
     * @param state 
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * Getter
     * @return zip code
     */
    public String getZipCode() {
        return zipCode;
    }

    /**
     * Setter
     * @param zipCode 
     */
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    @Override
    public String toString() {
        return city + ", " + street + "str., " + state + ", " + zipCode;
    }

}

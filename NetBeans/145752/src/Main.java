
import java.util.Scanner;

public class Main {

    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        Employee[] em = new Employee[3];
        for (int i = 0; i < 3; i++) {
            em[i] = new Employee(enterName(), enterAddress(), enterDate()); // creates 3 employees
        }
        for (Employee e : em) {
            System.out.println(e); // shows 3 employees
        }
    }

    /**
     * Get user input for employee's name
     * @return name
     */
    private static Name enterName() {
        System.out.println("Enter first name for employee:");
        String fName = sc.nextLine();
        System.out.println("Enter second name for employee:");
        String sName = sc.nextLine();
        return new Name(fName, sName);
    }

    /**
     * Get user input for employee's address
     * @return address
     */
    private static Address enterAddress() {
        try {
            System.out.println("Enter street for employee:");
            String street = sc.nextLine();
            System.out.println("Enter city for employee:");
            String city = sc.nextLine();
            System.out.println("Enter state for employee:");
            String state = sc.nextLine();
            System.out.println("Enter zip code for employee:");
            String zip = sc.nextLine();
            return new Address(street, city, state, zip);
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            return enterAddress();
        }
    }

    /**
     * Get user input for employee's hire date
     * @return hire date
     */
    private static Date enterDate() {
        try {
            System.out.println("Enter hire month for employee (1..12):");
            int month = sc.nextInt();
            System.out.println("Enter hire day for employee (1..31):");
            int day = sc.nextInt();
            System.out.println("Enter hire year for employee (1900..2020):");
            int year = sc.nextInt();
            sc.nextLine();
            return new Date(month, day, year);
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            return enterDate();
        }
    }

}


public class Date {

    private int month;
    private int day;
    private int year;

    /**
     * Constructor
     * @param month
     * @param day
     * @param year
     * @throws IllegalArgumentException if illegal argument was entered
     */
    public Date(int month, int day, int year) throws IllegalArgumentException {
        if (month > 0 && month < 13) {
            this.month = month;
        } else {
            throw new IllegalArgumentException("Month must be in the range 1..12");
        }
        if (day > 0 && day < 32) {
            this.day = day;
        } else {
            throw new IllegalArgumentException("Day must be in the range 1..31");
        }
        if (year > 1900 && year < 2020) {
            this.year = year;
        } else {
            throw new IllegalArgumentException("Year must be in the range 1900..2020");
        }
    }

    /**
     * Getter
     * @return month
     */
    public int getMonth() {
        return month;
    }

    /**
     * Setter
     * @param month 
     */
    public void setMonth(int month) {
        this.month = month;
    }

    /**
     * Getter
     * @return day
     */
    public int getDay() {
        return day;
    }

    /**
     * Setter
     * @param day 
     */
    public void setDay(int day) {
        this.day = day;
    }

    /**
     * Getter
     * @return year
     */
    public int getYear() {
        return year;
    }

    /**
     * Setter
     * @param year 
     */
    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return month + "\\" + day + "\\" + year;
    }

}

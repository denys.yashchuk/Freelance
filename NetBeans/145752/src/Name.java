
public class Name {

    private String firstName;
    private String lastName;

    /**
     * Constructor
     * @param firstName
     * @param lastName 
     */
    public Name(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    /**
     * Getter
     * @return first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Setter
     * @param firstName 
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Getter
     * @return last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Setter
     * @param lastName 
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return firstName + " " + lastName;
    }

}

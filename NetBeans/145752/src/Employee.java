
public class Employee {

    private Name name;
    private Address address;
    private Date hireDate;

    /**
     * Constructor
     * @param name
     * @param address
     * @param hireDate 
     */
    public Employee(Name name, Address address, Date hireDate) {
        this.name = name;
        this.address = address;
        this.hireDate = hireDate;
    }

    /**
     * Getter
     * @return name
     */
    public Name getName() {
        return name;
    }

    /**
     * Setter
     * @param name 
     */
    public void setName(Name name) {
        this.name = name;
    }

    /**
     * Getter
     * @return address
     */
    public Address getAddress() {
        return address;
    }

    /**
     * Setter
     * @param address 
     */
    public void setAddress(Address address) {
        this.address = address;
    }

    /**
     * Getter
     * @return hire date
     */
    public Date getHireDate() {
        return hireDate;
    }

    /**
     * Setter
     * @param hireDate 
     */
    public void setHireDate(Date hireDate) {
        this.hireDate = hireDate;
    }

    @Override
    public String toString() {
        return name + ", address: " + address + ", hire date:" + hireDate;
    }

}

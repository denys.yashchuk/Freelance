
import java.util.Scanner;

public class BankManagement {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in); // Scanner for reading from the console
        System.out.print("Welcome to Bank Management System\n\n"
                + "Creating a new Account ...\n"
                + "Enter the account holder's name :");
        String name = sc.nextLine(); // read name from the console
        double balance;
        do {
            System.out.print("Enter the initial balance : $");
            balance = Account.getValue(sc.nextLine());
        } while (balance < 0); // read positive double value for balance from the console
        Account acc = new Account(balance, name); // creates Account variable
        System.out.println("");
        String choice;
        double tmp;
        while (true) {
            System.out.print(">> MAIN MENU <<\n"
                    + "1. Deposit\n"
                    + "2. Withdraw\n"
                    + "3. Balance Inquiry\n"
                    + "4. Get future value\n"
                    + "5. Get investment period\n"
                    + "6. Exit\n"
                    + ">> Select your option (1 - 6): ");
            choice = sc.nextLine(); // read choice from the console
            switch (choice) {
                case "1":
                    do {
                        System.out.print("Enter amount to deposit : $");
                        tmp = Account.getValue(sc.nextLine());
                    } while (tmp < 0); // read how deposite value from the console
                    acc.deposite(tmp); // deposite
                    System.out.printf("\n***You deposited $%.2f\n", tmp); // show message
                    System.out.println("");
                    break;
                case "2":
                    do {
                        System.out.print("Enter amount to withdraw : $");
                        tmp = Account.getValue(sc.nextLine());
                    } while (tmp < 0);
                    double withdraw = acc.withdraw(tmp);
                    if (withdraw != -1) {
                        System.out.printf("\n***You withdrew $%.2f\n", tmp);
                    } else {
                        System.out.println("\n***You do not have enough money in the account!");
                    }
                    System.out.println("");
                    break;
                case "3":
                    System.out.println("\n" + acc.balanceInquiry()); // shows balance inquiry
                    System.out.println("");
                    break;
                case "4":
                    do {
                        System.out.print("Enter the investment time (in years ): ");
                        tmp = Account.getValue(sc.nextLine());
                    } while (tmp < 0);
                    System.out.printf("\n*** After " + tmp + " year (s): $%.2f\n", acc.futureValue(tmp));
                    System.out.println("");
                    break;
                case "5":
                    do {
                        System.out.print("Enter your goal value: $");
                        tmp = Account.getValue(sc.nextLine());
                    } while (tmp < 0);
                    System.out.printf("*** It will take %.1f years to reach your goal!\n", acc.investmentPeriod(tmp));
                    System.out.println("");
                    break;
                case "6":
                    return; // exit the loop
                default:
                    System.out.println("Make right choice!\n"); // if entered wrong choice show message and try again
                    break;
            }
        }
    }

}

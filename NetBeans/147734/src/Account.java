
public class Account {

    private final double ANNUAL_INTEREST = 0.03; // constant value

    private double balance;
    private String holderName;

    /**
     * Constructor
     *
     * @param balance
     * @param holderName
     */
    public Account(double balance, String holderName) {
        this.balance = balance;
        this.holderName = holderName;
    }

    /**
     * Deposit method
     *
     * @param sum to deposite
     * @return new balance value
     */
    public double deposite(double sum) {
        return balance += sum;
    }

    /**
     * Withdraw method
     *
     * @param sum to withdraw
     * @return new balance value or -1 if it is not enough money in the account
     */
    public double withdraw(double sum) {
        if (sum < balance) {
            return balance -= sum;
        } else {
            return -1;
        }
    }

    /**
     * balance inquiry method
     *
     * @return String with holder's name and balance
     */
    public String balanceInquiry() {
        return (holderName + "\'s balance: $" + balance);
    }

    /**
     * future value method
     *
     * @param years
     * @return balance after @param years
     */
    public double futureValue(double years) {
        return (balance * Math.pow(1 + ANNUAL_INTEREST, years));
    }

    /**
     * investment period method
     *
     * @param sum
     * @return how much years is need to get @param sum
     */
    public double investmentPeriod(double sum) {
        return (Math.log(sum / balance) / Math.log(1 + ANNUAL_INTEREST));
    }

    /**
     * get double value method
     *
     * @param s String with value
     * @return double value of String s or -1, if it is imposible to parse
     * double from the String
     */
    public static double getValue(String s) {
        double out;
        try {
            out = Double.parseDouble(s);
            return out;
        } catch (Exception c) {
            return -1;
        }
    }
}

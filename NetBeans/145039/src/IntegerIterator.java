
import java.util.Iterator;
import java.util.LinkedList;

public class IntegerIterator {

    private LinkedList<Integer> list;

    /**
     * Constructor;
     */
    public IntegerIterator() {
        list = new LinkedList<Integer>();
    }

    /**
     * Adds Integer to the LinkedList
     *
     * @param i - Integer for adding
     */
    public void addValue(int i) {
        list.add(i);
    }

    /**
     * Removes value from the list in the range
     *
     * @param it - IntegetIterator from which list remove Integer
     * @param value - the value which will be removed
     * @param minimum - starting index
     * @param maximum - ending index
     * @throws IllegalArgumentException
     */
    public void removeValueInRange(IntegerIterator it, int value,
            int minimum, int maximum) throws IllegalArgumentException {
        if (it.list.isEmpty() || minimum >= it.list.size() || maximum >= it.list.size()) {
            throw new IllegalArgumentException("Illegal Arguments!");
        }
        Iterator<Integer> iterator = it.list.iterator();
        for (int i = 0; i < maximum; i++) {
            int element = iterator.next();
            if (i >= minimum && i < maximum) {
                if (element == value) {
                    iterator.remove();
                }
            }
        }
    }

    @Override
    public String toString() {
        String out = "";
        for (int i : list) {
            out += i + " ";
        }
        return out;
    }

}

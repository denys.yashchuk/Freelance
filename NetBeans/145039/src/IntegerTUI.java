
import java.util.InputMismatchException;
import java.util.Scanner;

public class IntegerTUI {

    private IntegerIterator intIt;
    private Scanner sc;

    /**
     * Constructor;
     */
    public IntegerTUI() {
        intIt = new IntegerIterator();
        sc = new Scanner(System.in);
    }

    /**
     * Displays greeting message;
     */
    private void greetUser() {
        System.out.println("Welcome to the Integer Iterator example application");
    }

    /**
     * Displays menu;
     */
    private void displayMenu() {
        System.out.println("\n\t1 - Add integers to your list\n"
                + "\t2 - Remove Integers from the list\n"
                + "\t3 - Quit\n");
    }

    /**
     * Adds Integer to the list
     */
    private void addIntegers() {
        System.out.print("Please enter an integer to add to the list: ");
        try {
            int value = sc.nextInt();
            intIt.addValue(value);
        } catch (InputMismatchException e) {
            System.out.println("Enter integer");
        }
        displayList();
    }

    /**
     * Displays list;
     */
    private void displayList() {
        System.out.println("\nThe list of integers is: " + intIt);
    }

    /**
     * Removes Integer from the list;
     */
    private void removeIntegers() {
        System.out.print("Please enter the starting index: ");
        int minimum = sc.nextInt();
        System.out.print("Please enter the endinx index: ");
        int maximum = sc.nextInt();
        System.out.print("Please enter the value to remove: ");
        int value = sc.nextInt();
        try {
            intIt.removeValueInRange(intIt, value, minimum, maximum);
        } catch (IllegalArgumentException e) {
            System.out.println(e);
        }
        displayList();
    }

    /**
     * Displays quit message;
     */
    private void quit() {
        System.out.println("Thank you for using the Integer Iterator. Goodbye!");
    }

    /**
     * Runs the programm;
     */
    public void run() {
        greetUser();
        String choice;
        do {
            displayMenu();
            System.out.print("Please enter your choice: ");
            choice = sc.next();
            switch (choice) {
                case "1":
                    addIntegers();
                    break;
                case "2":
                    removeIntegers();
                    break;
                case "3":
                    quit();
                    break;
                default:
                    break;
            }
        } while (!choice.equals("3"));
    }

}


import java.io.File;
import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EncryptorTest {

    public static void main(String[] args) {
        // creates file variables
        File keyword = new File("keyword.txt");
        File input = new File("input.txt");
        File output = new File("output.txt");
        File encrypt = new File("encrypt.txt");
        String key = readStringFromFile(keyword);
        // creates object of encryptor
        Encryptor en = new Encryptor(key);
        // sets text to encode
        en.setInput(readStringFromFile(input));
        // encode string
        String encoded = en.encode();
        // write encoded string to file
        writeStringToFile(encoded, encrypt);
        // sets text to decode
        en.setInput(readStringFromFile(encrypt));
        // decode string
        String decoded = en.decode();
        // write decoded string to file
        writeStringToFile(decoded, output);
    }

    /**
     * Reads text from file
     * @param f - file to read from
     * @return read text in String
     */
    private static String readStringFromFile(File f) {
        String out = "";
        Scanner sc;
        try {
            sc = new Scanner(f);
            while (sc.hasNext()) {
                out += sc.nextLine();
            }
            sc.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(EncryptorTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return out;
    }

    /**
     * Writes text to file
     * @param s - String for writing
     * @param file - file for writing to
     */
    private static void writeStringToFile(String s, File file) {
        Formatter f;
        try {
            f = new Formatter(file);
            f.format("%s", s);
            f.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(EncryptorTest.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}

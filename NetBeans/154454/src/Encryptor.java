
public class Encryptor {

    private String input;
    private char[] key;
    private char[] alphabet;

    /**
     * Construcor
     * @param key 
     */
    public Encryptor(String key) {
        key = key.toUpperCase().trim();
        this.key = new char[26];
        this.alphabet = new char[26];

        // fills the alphabet
        int j = 0;
        for (char i = 'A'; i <= 'Z'; i++, j++) {
            this.alphabet[j] = i;
        }

        // fills key
        j = 0;
        for (char c : key.toCharArray()) {
            if (indexOfChar(c, false) == -1) {
                this.key[j] = c;
                j++;
            }
        }
        for (char i = 'Z'; i >= 'A'; i--) {
            if (key.indexOf(i) == -1) {
                this.key[j] = i;
                j++;
            }
        }
        // shows keyword, alphabet and key
        System.out.println("Raw key input: " + key);
        System.out.println("Unique character key used: "+key);
        System.out.println("Alphabet and encrypted characters: ");
        for(char c : alphabet){
            System.out.print(c);
        }
        System.out.println("");
        for(char c : this.key){
            System.out.print(c);
        }
        System.out.println("");
    }

    /**
     * Sets input String
     * @param input 
     */
    public void setInput(String input) {
        this.input = input;
    }

    /**
     * Encodes plaintext 
     * @return encoded text as String
     */
    public String encode() {
        String out = "";
        char[] in = input.toCharArray();
        for (char c : in) {
            if (c > 64 && c < 91) {
                char en = key[indexOfChar(c, true)];
                out += en;
                System.out.println("Plaintext byte " + (int) c + " (" + c + ") encoded as " + en);
            } else {
                out += c;
                System.out.println("Plaintext byte " + (int) c + " (" + c + ") was not encoded");
            }
        }
        return out;
    }

    /**
     * Find the index of the character in the array
     * @param c - character, index of which is need to find
     * @param type - true - search in alphabet array, false - in key array
     * @return index of character in array or -1 if there is not this character
     */
    private int indexOfChar(char c, boolean type) {
        if (type) {
            for (int i = 0; i < alphabet.length; i++) {
                if (c == alphabet[i]) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < key.length; i++) {
                if (c == key[i]) {
                    return i;
                }
            }
        }
        return -1;
    }

    /**
     * Decodes encrypted plaintext 
     * @return decoded text as String
     */
    public String decode() {
        String out = "";
        char[] in = input.toCharArray();
        for (char c : in) {
            if (c > 64 && c < 91) {
                char en = alphabet[indexOfChar(c, false)];
                out += en;
                System.out.println("Cyphertext byte " + (int) c + " (" + c + ") decoded as " + en);
            } else {
                out += c;
                System.out.println("Cyphertext byte " + (int) c + " (" + c + ") was not decoded");
            }
        }
        return out;
    }

}

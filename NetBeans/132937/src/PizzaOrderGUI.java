
import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * The top section has the centered title telling the name of the company and
 * instructions to choose pizza size and toppings. The middle section has four
 * radio buttons (available pizza sizes) and list of toppings. The bottom
 * section has the total pizza price and checkbox promoting a $5.00 side order
 * of hot wings. The size of Frame is 650*300 px. The frame isn't resizable.
 *
 * @author Denys
 */
public class PizzaOrderGUI extends JFrame {

    /**
     * Price variable for the size price (which could include the extra wings
     * price) Price variable for the topping price Total price variable
     */
    private double sizePrice;
    private double toppingsPrice;
    private double totalPrice;

    /**
     * Returns an PizzaOrderGUI object and creates frame
     *
     * @param title an title that sets to the window
     */
    public PizzaOrderGUI(String title) {
        sizePrice = 0;
        toppingsPrice = 0;
        setSize(new Dimension(650, 300));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle(title);
        addComponentsToPane(getContentPane());
        setResizable(false);
        setVisible(true);
    }

    /**
     * Adds components (JLabels, JRadioButtons, JCheckBoxes, etc.)
     *
     * @param pane adds components to this pane
     */
    private void addComponentsToPane(Container pane) {
        
        pane.setLayout(new BorderLayout());

        JPanel top = new JPanel();

        top.setLayout(new GridLayout(2, 1));
        top.setBackground(Color.yellow);

        JLabel companyName = new JLabel("HVCC Pizza Parlor");
        companyName.setFont(new Font("Times New Roman", Font.BOLD, 32));
        companyName.setForeground(Color.red);
        companyName.setHorizontalAlignment(JLabel.CENTER);
        top.add(companyName);

        JLabel instructions = new JLabel("Choose your pizza size and toppings ($1 each)");
        instructions.setFont(new Font("Arial", Font.BOLD, 16));
        instructions.setForeground(Color.red);
        instructions.setHorizontalAlignment(JLabel.CENTER);
        top.add(instructions);

        pane.add(top, BorderLayout.NORTH);

        JPanel middle = new JPanel();

        middle.setLayout(new GridLayout(1, 5));

        ButtonGroup sizes = new ButtonGroup();
        final JRadioButton small = new JRadioButton("Small($7)", false);
        sizes.add(small);
        final JRadioButton medium = new JRadioButton("Medium($9)", false);
        sizes.add(medium);
        final JRadioButton large = new JRadioButton("Large($11)", false);
        sizes.add(large);
        final JRadioButton xLarge = new JRadioButton("X-Large($13)", false);
        sizes.add(xLarge);

        middle.add(small);
        middle.add(medium);
        middle.add(large);
        middle.add(xLarge);

        String[] toppingsNames = {"Eggplant", "Green Peppers", "Hot Peppers", "Pepperoni",
            "Sausage", "Mushrooms", "Anchovies"};
        final JList toppings = new JList(toppingsNames);
        toppings.setForeground(Color.red);
        toppings.setSelectionForeground(Color.black);
        middle.add(toppings);

        pane.add(middle, BorderLayout.CENTER);

        JPanel bottom = new JPanel();

        bottom.setLayout(new GridLayout(1, 2));
        bottom.setBackground(Color.yellow);

        final JLabel price = new JLabel();
        totalPrice = sizePrice + toppingsPrice;
        price.setText(String.format("Price of your pizza is $%.2f", totalPrice));
        price.setForeground(Color.red);
        price.setFont(new Font("Times New Roman", Font.BOLD, 20));
        bottom.add(price);

        final JCheckBox hotWings = new JCheckBox("Side Order Hot Wings - add $5.00");
        hotWings.setForeground(Color.red);
        hotWings.setBackground(Color.yellow);
        bottom.add(hotWings);

        pane.add(bottom, BorderLayout.SOUTH);

        ItemListener changePriceListener = new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent itemEvent) {
                if (small.isSelected()) {
                    sizePrice = 7;
                } else if (medium.isSelected()) {
                    sizePrice = 9;
                } else if (large.isSelected()) {
                    sizePrice = 11;
                } else if (xLarge.isSelected()) {
                    sizePrice = 13;
                } else {
                    sizePrice = 0;
                }
                if (hotWings.isSelected()) {
                    sizePrice += 5;
                }
                toppingsPrice = toppings.getSelectedIndices().length;
                totalPrice = sizePrice + toppingsPrice;
                price.setText(String.format("Price of your pizza is $%.2f", totalPrice));
            }
        };

        small.addItemListener(changePriceListener);
        medium.addItemListener(changePriceListener);
        large.addItemListener(changePriceListener);
        xLarge.addItemListener(changePriceListener);
        hotWings.addItemListener(changePriceListener);

        toppings.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent lse) {
                if (small.isSelected()) {
                    sizePrice = 7;
                } else if (medium.isSelected()) {
                    sizePrice = 9;
                } else if (large.isSelected()) {
                    sizePrice = 11;
                } else if (xLarge.isSelected()) {
                    sizePrice = 13;
                } else {
                    sizePrice = 0;
                }
                if (hotWings.isSelected()) {
                    sizePrice += 5;
                }
                toppingsPrice = toppings.getSelectedIndices().length;
                totalPrice = sizePrice + toppingsPrice;
                price.setText(String.format("Price of your pizza is $%.2f", totalPrice));
            }
        });

    }

}

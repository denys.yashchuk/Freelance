
public class Sequence {
    
    private static int efficiency;
        
    /**
     * Clear the value of efficiency and compute the sequence
     * @param type  the type of computing
     * @param n     the number of the element
     * @return      n-th element of the sequence
     */
    public static int compute(String type, int n){
        efficiency = 0;
        if(type.equals("iterative")) return computeIterative(n);
        else return computeRecursive(n);
    }
    
    /**
     * Compute the sequence iterative and count efficiency
     * @param n     the number of the element
     * @return      n-th element of the sequence
     */
    public static int computeIterative (int n){
        int a = 0;
        int b = 1;
        for(int i = 2; i <= n; i++){
            efficiency++;
            int tmp = a + 2*b;
            a = b;
            b = tmp;
        }
        if(n == 0) return a;
        else return b;
    }
    
    /**
     * Compute the sequence recursive and count efficiency
     * @param n     the number of the element
     * @return      n-th element of the sequence
     */
    public static int computeRecursive (int n){
        efficiency++;
        if(n == 0) return 0;
        else if(n == 1) return 1;
        else return computeRecursive(n-2)+2*computeRecursive(n-1);
    }
    
    /**
     * @return  the value of the efficiency
     */
    public static int getEfficiency (){
        return efficiency;
    }
    
}


import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MyAdapter extends WindowAdapter {

    /**
     * Invoked when a window is in the process of being closed.
     * Compuute the efficiency values of n from 0 to 10 and write them to a file. 
     * @param we    window event 
     */
    @Override
    public void windowClosing(WindowEvent we) {
        File file = new File("efficiency.csv");
        try {
            PrintWriter out = new PrintWriter(file.getAbsoluteFile());
            out.println("n,iter,recur");
            for (int i = 0; i <= 10; i++) {
                String s = i + ",";
                Sequence.compute("iterative", i);
                s += Sequence.getEfficiency() + ",";
                Sequence.compute("recursive", i);
                s += Sequence.getEfficiency();
                out.println(s);
            }
            out.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MyAdapter.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
}

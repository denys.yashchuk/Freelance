
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class MyFrame extends JFrame {

    public static ButtonGroup group;
    public static JRadioButton it;
    public static JRadioButton rec;
    public static JTextField n;
    public static JTextField resOut;
    public static JTextField effOut;

    /**
     * Creates the window
     */
    public MyFrame() {
        setSize(new Dimension(300, 200));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Project 3");
        addComponentsToPane(getContentPane());
        setResizable(false);
        setVisible(true);
        addWindowListener(new MyAdapter());
    }

    /**
     * Adds components to pane of the window
     * @param pane  the pane for adding components 
     */
    private void addComponentsToPane(Container pane) {
        pane.setLayout(new GridLayout(6, 2, 5, 5));

        group = new ButtonGroup();
        it = new JRadioButton("Iterative", true);
        group.add(it);
        rec = new JRadioButton("Recursive", false);
        group.add(rec);
        pane.add(new JLabel());
        pane.add(it);

        pane.add(new JLabel());
        pane.add(rec);

        JLabel l = new JLabel("Enter n:");
        pane.add(l);

        n = new JTextField();
        n.setMinimumSize(new Dimension(20, 40));
        pane.add(n);

        pane.add(new JLabel());

        JButton compute = new JButton("Compute");
        compute.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                if (group.isSelected(it.getModel())) {
                    resOut.setText(Integer.toString(Sequence.compute("iterative", Integer.parseInt(n.getText()))));
                } else {
                    resOut.setText(Integer.toString(Sequence.compute("recursive", Integer.parseInt(n.getText()))));
                }
                effOut.setText(Integer.toString(Sequence.getEfficiency()));
            }
        });

        pane.add(compute);
        JLabel res = new JLabel("Result:");
        pane.add(res);

        resOut = new JTextField();
        pane.add(resOut);

        JLabel eff = new JLabel("Efficiency:");
        pane.add(eff);

        effOut = new JTextField();
        pane.add(effOut);
    }

    public static void main(String[] args) {
        new MyFrame();
    }

}

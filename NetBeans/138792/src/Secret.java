
public class Secret implements Encryptable {

    private final int SHIFT = 7;
    private String open;
    private String encrypted;
    
    public Secret(String open){
        encrypted = "Is open";
        this.open = open;
    }

    @Override
    public void encrypt() {
        encrypted = "";
        int len = open.length();
        for (int x = 0; x < len; x++) {
            char c = (char) (open.charAt(x) - SHIFT);
            if (c < 'a') {
                encrypted += (char) (open.charAt(x) + (26 - SHIFT));
            } else {
                encrypted += (char) (open.charAt(x) - SHIFT);
            }
        }
        open = "Is secret";
    }

    @Override
    public void decrypt() {
        open = "";
        int len = encrypted.length();
        for (int x = 0; x < len; x++) {
            char c = (char) (encrypted.charAt(x) + SHIFT);
            if (c > 'z') {
                open += (char) (encrypted.charAt(x) - (26 - SHIFT));
            } else {
                open += (char) (encrypted.charAt(x) + SHIFT);
            }
        }
        encrypted = "Is open";
    }

    @Override
    public String getEncrypted() {
        return encrypted;
    }

    @Override
    public String getOpen() {
        return open;
    }

}

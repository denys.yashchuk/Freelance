
interface Encryptable {
    
    public void encrypt();
    public void decrypt();
    public String getEncrypted();
    public String getOpen();
    
}

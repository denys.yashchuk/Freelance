
import java.util.ArrayList;


public class Main {
    
    public static void main(String[] args){
        ArrayList<Encryptable> en = new ArrayList<Encryptable>();
        en.add(new Secret("simplesecret"));
        en.add(new Password("P@ssw0rd"));
        for(int i = 0; i < en.size(); i++){
            Encryptable element = en.get(i);
            System.out.println("Element #"+(i+1)+"\n");
            System.out.println("Before encryption:");
            System.out.println("Open Text is: "+element.getOpen());
            System.out.println("Encrypted Text is: "+element.getEncrypted()+"\n");
            element.encrypt();
            System.out.println("After encryption:");
            System.out.println("Open Text is: "+element.getOpen());
            System.out.println("Encrypted Text is: "+element.getEncrypted()+"\n");
            element.decrypt();
            System.out.println("After decryption:");
            System.out.println("Open Text is: "+element.getOpen());
            System.out.println("Encrypted Text is: "+element.getEncrypted()+"\n");
        }
    }
    
}

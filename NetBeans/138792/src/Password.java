
public class Password implements Encryptable{

    private String encrypted;
    private String password;

    public Password(String password) {
        encrypted = "Is open";
        this.password = password;
    }

    public String getEncrypted() {
        return encrypted;
    }

    public String getOpen() {
        return password;
    }
    
    @Override
    public void encrypt() {
        encrypted = "";
        for(int i = 0; i < password.length()/2; i++){
            encrypted += password.charAt(i*2+1)+""+password.charAt(i*2);
        }
        if(password.length()%2==1){
            encrypted += password.charAt(password.length()-1);
        }
        password = "Is secret";
    }

    @Override
    public void decrypt() {
        password = "";
        for(int i = 0; i < encrypted.length()/2; i++){
            password += encrypted.charAt(i*2+1)+""+encrypted.charAt(i*2);
        }
        if(encrypted.length()%2==1){
            password += encrypted.charAt(encrypted.length()-1);
        }
        encrypted = "Is open";
    }
    
}


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PipedInputStream;
import java.net.Socket;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConsumerProcess implements Runnable {

    private int[] array = new int[100];
    private PipedInputStream pis;
    private ProducerProcess producer;
    private SharedMemory sm;
    private int type;

    public ConsumerProcess(PipedInputStream pis) {
        this.pis = pis;
        type = 1;
    }

    public ConsumerProcess(ProducerProcess producer) {
        this.producer = producer;
        type = 2;
    }

    public ConsumerProcess() {
        type = 3;
    }

    public ConsumerProcess(SharedMemory sm) {
        this.sm = sm;
        type = 4;
    }

    @Override
    public void run() {
        switch(type){
            case 1:
                readThroughPipe();
                break;
            case 2:
                readMessage();
                break;
            case 3:
                readThroughSocket();
                break;
            case 4:
                readUsingSharedMemory();
                break;
        }
        FileManager.writeToFile("consumer.txt", Arrays.toString(array));
    }

    private void readThroughPipe() {
        try {
            for (int i = 0; i < array.length; i++) {
                array[i] = pis.read();
            }
            pis.close();
        } catch (IOException ex) {
            Logger.getLogger(ConsumerProcess.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void readMessage() {
        array = producer.readMessage();
    }

    private void readThroughSocket() {
        try {
            Socket socket = new Socket("localhost", 9999);
            InputStream in = socket.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            for (int i = 0; i < 100; i++) {
                array[i] = Integer.parseInt(reader.readLine());
            }
            reader.close();
        } catch (IOException ex) {
            Logger.getLogger(ConsumerProcess.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void readUsingSharedMemory() {
        array = sm.get();
        for (int i = 1; i < 10; i++) {
            array = Arrays.copyOf(array, array.length + 10);
            System.arraycopy(sm.get(), 0, array, array.length - 10, 10);
        }
    }

}


import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.Scanner;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String choice;
        do {
            System.out.println("Choose method: \n"
                    + "1. Pipe;\n"
                    + "2. Message;\n"
                    + "3. Socket;\n"
                    + "4. Shared memory and semaphore;");
            choice = sc.nextLine();
        } while (!(choice.equals("1") || choice.equals("2")
                || choice.equals("3") || choice.equals("4")));
        switch (choice) {
            case "1":
                testPipe();
                break;
            case "2":
                testMessage();
                break;
            case "3":
                testSocket();
                break;
            case "4":
                testSemaphore();
                break;
        }
    }

    private static void testPipe() {
        try {
            PipedOutputStream pos = new PipedOutputStream();
            PipedInputStream pis = new PipedInputStream();
            pos.connect(pis);
            Thread producer = new Thread(new ProducerProcess(pos));
            Thread consumer = new Thread(new ConsumerProcess(pis));
            producer.start();
            consumer.start();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void testMessage() {
        Vector vector = new Vector();
        ProducerProcess pp = new ProducerProcess(vector);
        Thread producer = new Thread(pp);
        Thread consumer = new Thread(new ConsumerProcess(pp));
        producer.start();
        consumer.start();
    }

    private static void testSocket() {
        Thread producer = new Thread(new ProducerProcess());
        Thread consumer = new Thread(new ConsumerProcess());
        producer.start();
        consumer.start();
    }

    private static void testSemaphore() {
        SharedMemory sm = new SharedMemory();
        Thread producer = new Thread(new ProducerProcess(sm));
        Thread consumer = new Thread(new ConsumerProcess(sm));
        producer.start();
        consumer.start();
    }

}

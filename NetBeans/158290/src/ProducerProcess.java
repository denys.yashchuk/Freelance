
import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.channels.Pipe;
import java.util.Arrays;
import java.util.Random;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProducerProcess implements Runnable {

    private int[] array = new int[100];
    private PipedOutputStream pos;
    private Vector vector;
    private SharedMemory sm;
    private int type;

    public ProducerProcess(PipedOutputStream pos) {
        this.pos = pos;
        type = 1;
    }

    public ProducerProcess(Vector vector) {
        this.vector = vector;
        type = 2;
    }

    public ProducerProcess() {
        type = 3;
    }

    public ProducerProcess(SharedMemory sm) {
        this.sm = sm;
        type = 4;
    }

    @Override
    public void run() {
        generateArray();
        FileManager.writeToFile("producer.txt", Arrays.toString(array));
        switch(type){
            case 1:
                sendThroughPipe();
                break;
            case 2:
                sendMessage();
                break;
            case 3:
                sendThroughSocket();
                break;
            case 4:
                sendUsingSharedMemory();
                break;
        }
    }

    private int[] generateArray() {
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(255);
        }
        return array;
    }

    private void sendThroughPipe() {
        try {
            for (int i = 0; i < array.length; i++) {
                pos.write(array[i]);
            }
            pos.close();
        } catch (IOException ex) {
            Logger.getLogger(ProducerProcess.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private synchronized void sendMessage() {
        for (int i = 0; i < array.length; i++) {
            vector.add(array[i]);
        }
        notifyAll();
    }

    public synchronized int[] readMessage() {
        try {
            while (vector.size() != 100) {
                wait();
            }
            int[] output = new int[100];
            for (int i = 0; i < 100; i++) {
                output[i] = (int) vector.get(i);
            }
            vector.removeAllElements();
            return output;
        } catch (InterruptedException ex) {
            Logger.getLogger(ConsumerProcess.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    private void sendThroughSocket() {
        try {
            ServerSocket serverSocket = new ServerSocket(9999);
            Socket clientSocket = serverSocket.accept();
            PrintStream ps = new PrintStream(clientSocket.getOutputStream());
            for (int i = 0; i < 100; i++) {
                ps.println(array[i]);
            }
            ps.close();
            clientSocket.close();
        } catch (IOException ex) {
            Logger.getLogger(ProducerProcess.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void sendUsingSharedMemory() {
        for (int i = 0; i < 10; i++) {
            sm.put(Arrays.copyOfRange(array, i * 10, i * 10 + 10));
        }
    }

}

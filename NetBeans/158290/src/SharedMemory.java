
import java.util.concurrent.Semaphore;

public class SharedMemory {

    private int[] array;

    private static Semaphore semCon = new Semaphore(0);

    private static Semaphore semProd = new Semaphore(1);

    public int[] get() {
        try {
            semCon.acquire();
        } catch (InterruptedException e) {
            System.out.println("InterruptedException caught");
        }
        semProd.release();
        return array;
    }

    public void put(int[] n) {
        try {
            semProd.acquire();
        } catch (InterruptedException e) {
            System.out.println("InterruptedException caught");
        }

        this.array = n;
        semCon.release();
    }
}

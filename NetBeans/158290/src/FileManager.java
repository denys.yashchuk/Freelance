
import java.io.FileNotFoundException;
import java.util.Formatter;

public class FileManager {

    public static void writeToFile(String fileName, String input) {
        try {
            Formatter formatter = new Formatter(fileName);
            formatter.format("%s", input);
            formatter.close();
            System.out.println("done");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}

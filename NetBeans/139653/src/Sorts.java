
public class Sorts {
    
    public static void main(String[] args){
        double a[] = {4.0,3.5,3.7,2.2,2.6,3.49,3.3,1.5,3.6};
        for(double c: a){
            System.out.print(c+" ");
        }
        System.out.println();
        insertionSort(a);
        for(double c: a){
            System.out.print(c+" ");
        }
    }
    
    public static void insertionSort(double[] a){     
        for(int i = 1; i < a.length; i++){
            double tmp = a[i];
            int j;
            for(j = i - 1; j >= 0 && tmp < a[j]; j--){
                a[j+1] = a[j];
            }
            a[j+1] = tmp;
        }
    }
    
}

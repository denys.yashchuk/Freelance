
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Stack;
import java.util.concurrent.ArrayBlockingQueue;

public class Lab5 {

    public static void main(String args[]) {
        System.out.println("5.2.1");
        //ArrayList<Data> array = new ArrayList<Data>();
        LinkedList<Data> array = new LinkedList<Data>();
        array.add(new Data("Fred", 21));
        array.add(new Data("Jo", 43));
        array.add(new Data("Harry", 78));
        array.add(new Data("Zoe", 37));
        PrintCollection(array);

        System.out.println("5.2.2");
        ArrayList<Data> ArrayA = new ArrayList<Data>();
        ArrayList<Data> ArrayB = new ArrayList<Data>();
        ArrayA.add(new Data("Fred", 21));
        ArrayA.add(new Data("Jo", 43));
        ArrayA.add(new Data("Zoe", 37));
        PrintCollection(ArrayA);
        System.out.println();
        ArrayB = ArrayA;
        PrintCollection(ArrayB);
        System.out.println();
        ArrayA.remove(1);
        PrintCollection(ArrayB);

        ArrayList<Data> ArrayC = new ArrayList<Data>();
        ArrayList<Data> ArrayD = new ArrayList<Data>();
        ArrayC.add(new Data("Fred", 21));
        ArrayC.add(new Data("Jo", 43));
        ArrayC.add(new Data("Zoe", 37));
        PrintCollection(ArrayC);
        System.out.println();
        ArrayD = (ArrayList<Data>) ArrayC.clone();
        ArrayC.remove(1);
        PrintCollection(ArrayC);
        System.out.println();
        PrintCollection(ArrayD);
        System.out.println();

        System.out.println("5.3");
        Stack<Data> stack = new Stack<Data>();
        for (Data d : ArrayD) {
            stack.push(d);
        }
        PrintCollection(stack);
        while (stack.isEmpty() == false) {
            stack.pop().Print();
        }
        System.out.println(stack.size());

        System.out.println("5.4");
        ArrayBlockingQueue<Data> q = new ArrayBlockingQueue<Data>(10);
        for (Data d : ArrayD) {
            q.add(d);
        }
        PrintCollection(q);
        while (q.isEmpty() == false) {
            q.poll().Print();
        }
        System.out.println(q.size());
        for (int i = 0; i < 20; ++i) {
            q.offer(new Data("Test:" + String.valueOf(i), i));
        }
        PrintCollection(q);
    }

    public static void PrintCollection(Collection<Data> c) {
        for (Iterator<Data> iter = c.iterator(); iter.hasNext();) {
            Data x = (Data) iter.next();
            x.Print();
        }
        System.out.println();
    }

}

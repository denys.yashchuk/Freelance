
import java.util.Random;

public class RangeOfThree {

    public static void main(String[] args) {
        Random r = new Random();
        int[] randomValues = {r.nextInt(99 - 10 + 1) + 10, r.nextInt(99 - 10 + 1) + 10,
            r.nextInt(99 - 10 + 1) + 10};
        int minimum = Math.min(Math.min(randomValues[0], randomValues[1]), randomValues[2]);
        int maximum = Math.max(Math.max(randomValues[0], randomValues[1]), randomValues[2]);
        System.out.print("Random numbers: " + randomValues[0] + " " + randomValues[1] + " " 
                + randomValues[2] + " Range: " + (maximum - minimum));
    }

}


import java.util.Arrays;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author denys
 */
public class Test {

    public static void main(String[] args) {
        int[] arr1 = {5,2,3,8,7,0,1,9,6,4};
        int[] arr2 = {12,3,4,20,1,10,9,17,16,15,2,6,7,5,11,14,8,13,19,18};
        
        System.out.println("array 1: "+Arrays.toString(arr1));
        System.out.println("array 2: "+Arrays.toString(arr2));
        System.out.println("");
        
        System.out.println("Liner search 0 in array 1: ");
        System.out.println(SearchSortTool.linearSearchUnsorted(arr1, 0));
        System.out.println("Liner search 11 in array 2: ");
        System.out.println(SearchSortTool.linearSearchUnsorted(arr2, 11));
        System.out.println("");
        
        System.out.println("Two way liner search 4 in array 1: ");
        System.out.println(SearchSortTool.linearSearchUnsorted(arr1, 4));
        System.out.println("Two way liner search 1 in array 2: ");
        System.out.println(SearchSortTool.linearSearchUnsorted(arr2, 1));
        System.out.println("");
        
        System.out.println("Insert Sort:");
        System.out.println(Arrays.toString(arr1));
        System.out.println(Arrays.toString(SearchSortTool.insertSort(Arrays.copyOf(arr1, arr1.length))));
        System.out.println(Arrays.toString(arr2)); 
        System.out.println(Arrays.toString(SearchSortTool.insertSort(Arrays.copyOf(arr2, arr2.length))));
        System.out.println("");
        
        System.out.println("Selection Sort:");
        System.out.println(Arrays.toString(arr1));
        System.out.println(Arrays.toString(SearchSortTool.selectionSort(arr1)));
        System.out.println(Arrays.toString(arr2)); 
        System.out.println(Arrays.toString(SearchSortTool.selectionSort(arr2)));
        System.out.println("");
        
        System.out.println("array 1: "+Arrays.toString(arr1));
        System.out.println("array 2: "+Arrays.toString(arr2));
        System.out.println("");
        
        System.out.println("Liner search 0 in array 1: ");
        System.out.println(SearchSortTool.linearSearchSorted(arr1, 0));
        System.out.println("Liner search 11 in array 2: ");
        System.out.println(SearchSortTool.linearSearchSorted(arr2, 11));
        System.out.println("");
        
        System.out.println("Binary search 1 in array 1: ");
        System.out.println(SearchSortTool.binarySearch(arr1, 1));
        System.out.println("Binary search 15 in array 2: ");
        System.out.println(SearchSortTool.binarySearch(arr2, 15));
        System.out.println("");
    }

}

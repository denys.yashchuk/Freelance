/*
 * The SearchSortTool program implements algoritms for searching element in the
 * array and for sorting an array
 */

/**
 *
 * @author student
 */
public class SearchSortTool {

    /**
     * implements linear search in unsorted array
     *
     * @param arr - array for searching
     * @param el - element to search
     * @return index of element or -1 if there is not such element in the array
     */
    public static int linearSearchUnsorted(int[] arr, int el) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == el) {
                return i;
            }
        }
        return -1;
    }

    /**
     * implements linear search, which search element from the start and end of
     * unsorted array in the same time
     *
     * @param arr - array for searching
     * @param el - element to search
     * @return index of element or -1 if there is not such element in the array
     */
    public static int twoWayLinearSearchUnsorted(int[] arr, int el) {
        for (int i = 0; i < arr.length / 2 + 1; i++) {
            if (arr[i] == el) {
                return i;
            } else if (arr[arr.length - 1 - i] == el) {
                return arr.length - 1 - i;
            }
        }
        return -1;
    }

    /**
     * implements linear search in sorted array
     *
     * @param arr - array for searching
     * @param el - element to search
     * @return index of element or -1 if there is not such element in the array
     */
    public static int linearSearchSorted(int[] arr, int el) {
        for (int i = 0; i < arr.length && arr[i] <= el; i++) {
            if (arr[i] == el) {
                return i;
            }
        }
        return -1;
    }

    /**
     * implements binary search in unsorted array
     *
     * @param arr - array for searching
     * @param el - element to search
     * @return index of element or -1 if there is not such element in the array
     */
    public static int binarySearch(int[] arr, int el) {
        int start = 0;
        int end = arr.length;
        while (start <= end) {
            int mid = start + (end - start) / 2;
            if (el < arr[mid]) {
                end = mid - 1;
            } else if (el > arr[mid]) {
                start = mid + 1;
            } else {
                return mid;
            }
        }
        return -1;
    }

    /**
     * implements isert sort of the array
     *
     * @param arr - array for sorting
     * @return sorted array
     */
    public static int[] insertSort(int[] arr) {
        for (int i = 1; i < arr.length; i++) {
            int tmp = arr[i];
            int j = i;
            while (j > 0 && arr[j - 1] >= tmp) {
                arr[j] = arr[j - 1];
                --j;
            }
            arr[j] = tmp;
        }
        return arr;
    }

    /**
     * implements selection sort of the array
     *
     * @param arr - array for sorting
     * @return sorted array
     */
    public static int[] selectionSort(int[] arr) {
        int min;
        for (int i = 0; i < arr.length; i++) {
            min = i;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j] < arr[min]) {
                    min = j;
                }
            }
            int tmp = arr[i];
            arr[i] = arr[min];
            arr[min] = tmp;
        }
        return arr;
    }
}

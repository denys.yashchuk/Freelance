
import java.awt.Container;
import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;

public class Clock extends JFrame {

    public Clock() {
        setMinimumSize(new Dimension(300, 180));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Clock");
        addComponentsToPane(getContentPane());
        setResizable(false);
        setVisible(true);
    }

    private void addComponentsToPane(Container pane) {
        JTabbedPane tp = new JTabbedPane();
        tp.add("Stop Watch", new StopWatchTab());
        tp.add("Alarm", new AlarmTab());
        pane.add(tp);
    }

}

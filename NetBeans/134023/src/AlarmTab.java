
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.Timer;

public class AlarmTab extends JPanel {

    private static final JCheckBox set = new JCheckBox("Set Alarm");

    public AlarmTab() {
        ClockLabel timeLable = new ClockLabel("time");
        this.setLayout(new GridLayout(2, 1));
        this.add(timeLable);
        JPanel setAlarm = new JPanel();
        Integer[] hours = new Integer[24];
        Integer[] minutes = new Integer[60];
        Integer[] seconds = new Integer[60];
        for (int i = 0; i < 24; i++) {
            hours[i] = i;
        }
        for (int i = 0; i < 60; i++) {
            minutes[i] = i;
        }
        for (int i = 0; i < 60; i++) {
            seconds[i] = i;
        }
        JComboBox hour = new JComboBox(hours);
        JComboBox minute = new JComboBox(minutes);
        JComboBox second = new JComboBox(seconds);
        hour.setPreferredSize(new Dimension(40, 30));
        minute.setPreferredSize(new Dimension(40, 30));
        second.setPreferredSize(new Dimension(40, 30));
        setAlarm.add(new JLabel("HH:"));
        setAlarm.add(hour);
        setAlarm.add(new JLabel("MM:"));
        setAlarm.add(minute);
        setAlarm.add(new JLabel("SS:"));
        setAlarm.add(second);

        set.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                if (set.isSelected()) {
                    alarm((int) hour.getSelectedItem(),
                            (int) minute.getSelectedItem(),
                            (int) second.getSelectedItem());
                }
            }
        });
        setAlarm.add(set);
        this.add(setAlarm);
    }

    public static void alarm(int hours, int minutes, int seconds) {
        Runnable r = new Runnable() {
            public void run() {
                while (true) {
                    Date d = new Date();
                    int ah = d.getHours();
                    int am = d.getMinutes();
                    int as = d.getSeconds();
                    if (ah == hours && am == minutes && as == seconds) {
                        JOptionPane.showMessageDialog(new JFrame(), "ALARM!!!", "ALARM",
                                JOptionPane.INFORMATION_MESSAGE);
                        set.setSelected(false);
                        break;
                    }
                }
            }
        };
        new Thread(r).start();
    }
}

class ClockLabel extends JLabel implements ActionListener {

    String type;
    SimpleDateFormat sdf;

    public ClockLabel(String type) {
        this.type = type;
        sdf = new SimpleDateFormat("HH:mm:ss");
        setFont(new Font("Serif", Font.PLAIN, 50));
        setHorizontalAlignment(SwingConstants.CENTER);
        Timer t = new Timer(1000, this);
        t.start();
    }

    public void actionPerformed(ActionEvent ae) {
        Date d = new Date();
        setText(sdf.format(d));
    }
}

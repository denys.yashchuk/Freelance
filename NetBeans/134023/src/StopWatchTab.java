
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

public class StopWatchTab extends JPanel {

    private Timer myTimer1;
    public static final int ONE_SEC = 1000;   //time step in milliseconds
    public static final int TENTH_SEC = 100;

    private Font myClockFont;

    private JButton startBtn, stopBtn, resetBtn;
    private JLabel timeLbl;
    private JPanel topPanel, bottomPanel;

    private int clockTick;  	//number of clock ticks; tick can be 1.0 s or 0.1 s
    private double clockTime;  	//time in seconds
    private String clockTimeString;

    public StopWatchTab() {
        clockTick = 0;  		//initial clock setting in clock ticks
        clockTime = ((double) clockTick) / 10.0;
        clockTimeString = new Double(clockTime).toString();
        myClockFont = new Font("Serif", Font.PLAIN, 50);
        addComponentsToPane();
    }

    private void addComponentsToPane() {

        this.setLayout(new BorderLayout());

        timeLbl = new JLabel();
        timeLbl.setFont(myClockFont);
        timeLbl.setText(clockTimeString);

        startBtn = new JButton("Start");
        stopBtn = new JButton("Stop");
        resetBtn = new JButton("Reset");

        myTimer1 = new Timer(TENTH_SEC, new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                clockTick++;
                clockTime = ((double) clockTick) / 10.0;
                clockTimeString = new Double(clockTime).toString();
                timeLbl.setText(clockTimeString);
                //System.out.println(clockTime);
            }
        });

        startBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                myTimer1.start();
            }
        });

        stopBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                myTimer1.stop();
            }
        });

        resetBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                clockTick = 0;
                clockTime = ((double) clockTick) / 10.0;
                clockTimeString = new Double(clockTime).toString();
                timeLbl.setText(clockTimeString);
            }
        });

        topPanel = new JPanel();
        bottomPanel = new JPanel();
        topPanel.add(timeLbl);
        bottomPanel.add(startBtn);
        bottomPanel.add(stopBtn);
        bottomPanel.add(resetBtn);
        this.add(topPanel, BorderLayout.CENTER);
        this.add(bottomPanel, BorderLayout.SOUTH);
    }

}

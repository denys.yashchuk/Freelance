
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        System.out.println("10 Random numbers from 3 to 12:");
        for (int i = 0; i < 10; ++i) {
            int x = CS2004.UI(3, 12);
            System.out.println(x);
        }

        System.out.println("\nTest ScalesSolution constructor: ");
        System.out.println("Input: 10101");
        ScalesSolution s = new ScalesSolution("10101");
        s.println();
        System.out.println("Input: 10101x");
        s = new ScalesSolution("10101x");
        s.println();

        System.out.println("\nTest with weights 1, 2, 3, 4, 10: ");
        s = new ScalesSolution("10101");
        ArrayList<Double> weights = new ArrayList<Double>();
        weights.add(1.0);
        weights.add(2.0);
        weights.add(3.0);
        weights.add(4.0);
        weights.add(10.0);
        System.out.println(s.ScalesFitness(weights));

        System.out.println("Number (at least 100 each) of random Scales solutions "
                + "for the first 10, 100, 250, 500 and 1000 primes numbers and evaluation"
                + " (and average) their fitness.");
        int numberOfIterations = 100; // how much iterations will be for counting average Scales solutions

        System.out.print("For first 10 primes average Scales solutions: ");
        double average = 0.0;
        weights = CS2004.ReadNumberFile("1000 Primes.txt"); // read 1000 primes from file
        for (int i = 0; i < numberOfIterations; i++) { // for numberOfIterations times
            s = new ScalesSolution(10); // create random scasol of 10 elements
            average += s.ScalesFitness(weights); // get sum of numberOfIterations  Scales solutions
        }
        average = average / numberOfIterations; // get average value
        System.out.println(average); // print average value

        System.out.print("For first 100 primes average Scales solutions: ");
        average = 0.0;
        weights = CS2004.ReadNumberFile("1000 Primes.txt");
        for (int i = 0; i < numberOfIterations; i++) {
            s = new ScalesSolution(100);
            average += s.ScalesFitness(weights);
        }
        average = average / numberOfIterations;
        System.out.println(average);

        System.out.print("For first 250 primes average Scales solutions: ");
        average = 0.0;
        weights = CS2004.ReadNumberFile("1000 Primes.txt");
        for (int i = 0; i < numberOfIterations; i++) {
            s = new ScalesSolution(250);
            average += s.ScalesFitness(weights);
        }
        average = average / numberOfIterations;
        System.out.println(average);

        System.out.print("For first 500 primes average Scales solutions: ");
        average = 0.0;
        weights = CS2004.ReadNumberFile("1000 Primes.txt");
        for (int i = 0; i < numberOfIterations; i++) {
            s = new ScalesSolution(500);
            average += s.ScalesFitness(weights);
        }
        average = average / numberOfIterations;
        System.out.println(average);

        System.out.print("For first 1000 primes average Scales solutions: ");
        average = 0.0;
        weights = CS2004.ReadNumberFile("1000 Primes.txt");
        for (int i = 0; i < numberOfIterations; i++) {
            s = new ScalesSolution(1000);
            average += s.ScalesFitness(weights);
        }
        average = average / numberOfIterations;
        System.out.println(average);
    }

}

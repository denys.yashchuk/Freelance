
public class Edge {

    public Vertex from;
    public Vertex to;
    public int weight;

    public Edge(Vertex from, Vertex to, int weight) {
        this.from = from;
        this.to = to;
        this.weight = weight;
    }

    public int getWeight() {
        return weight;
    }

    public String showWithWeight() {
        return "(" + from + ", " + to + ") w = " + weight;
    }

    @Override
    public boolean equals(Object obj) {
        Edge e = (Edge) obj;
        return e.from.equals(this.from) && e.to.equals(this.to);
    }

    @Override
    public String toString() {
        return "(" + from + ", " + to + ")";
    }

}

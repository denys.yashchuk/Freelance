
import java.util.ArrayList;

public class Vertex {

    public ArrayList<Edge> inEdges;
    public ArrayList<Edge> outEdges;
    public int degree;
    public int distance;
    private char name;
    public boolean wasViseted;
    public Vertex parent;

    public Vertex() {
        inEdges = new ArrayList<Edge>();
        outEdges = new ArrayList<Edge>();
    }

    public Vertex(char name) {
        this.name = name;
        inEdges = new ArrayList<Edge>();
        outEdges = new ArrayList<Edge>();
    }

    public char getName() {
        return name;
    }

    public void setName(char name) {
        this.name = name;
    }

    public Vertex addEdge(Vertex node, boolean isDirect, int weight) {
        if (!isDirect) {
            Edge e = new Edge(this, node, weight);
            outEdges.add(e);
            node.inEdges.add(e);
            e = new Edge(node, this, weight);
            inEdges.add(e);
            node.outEdges.add(e);
            return this;
        } else {
            Edge e = new Edge(this, node, weight);
            outEdges.add(e);
            node.inEdges.add(e);
            return this;
        }
    }

    @Override
    public boolean equals(Object obj) {
        Vertex e = (Vertex) obj;
        return e.name == this.name;
    }

    @Override
    public String toString() {
        return name + "";
    }
}

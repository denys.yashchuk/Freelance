
import java.io.File;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    public static void main(String[] args) {
        File f = new File("assn4in.txt"); // gets file
        try {
            Graph g = new Graph(f); // creates graph from file
            System.out.println(g.topSort()); // do topological sort and shows results
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}


import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.Scanner;

public class Graph {

    boolean[][] adjMatrix; // adjacency matrix of the graph
    String[] names; // names of vertexes
    int numberOfVertex;

    public Graph(boolean[][] adjMatrix, String[] names) {
        this.adjMatrix = adjMatrix;
        this.names = names;
        numberOfVertex = names.length;
    }

    public Graph(File f) throws FileNotFoundException {
        Scanner sc = new Scanner(f);
        this.numberOfVertex = sc.nextInt();// read first line from file with number of vertexes
        sc.nextLine();
        this.adjMatrix = new boolean[numberOfVertex][numberOfVertex]; // creates arrays
        this.names = new String[numberOfVertex];
        String n = sc.nextLine(); // read names of vertexes
        names = n.split("\t"); // set names to array (names are separated by tabs);
        for (int i = 0; i < numberOfVertex; i++) {
            String[] raw = sc.nextLine().split("\t");
            for (int j = 0; j < numberOfVertex; j++) {
                adjMatrix[i][j] = raw[j].equals("1"); // reads adj matrix and sets it to the array;
            }
        }

    }

    public ArrayList<String> findCycles() {

        ArrayList<String> white = new ArrayList<String>();
        ArrayList<String> gray = new ArrayList<String>();
        ArrayList<String> black = new ArrayList<String>();

        for (String n : names) {
            white.add(n); // add all vertexes to white pool
        }

        // wile there is vertexes in white pool
        while (white.size() > 0) {
            String current = white.iterator().next(); // gets vertex from pool
            // do dfs
            if (dfs(current, white, gray, black)) {
              // returns gray pool (pool with the vertexex in cycle
                return gray;
            }
        }
        return null;

    }

    private boolean dfs(String current, ArrayList<String> white,
            ArrayList<String> gray, ArrayList<String> black) {
        //move current to gray set from white set and then explore it.
        moveVertex(current, white, gray);
        int index = getIndexOfCurrent(current);
        for (int i = 0; i < names.length; i++) {
            if (adjMatrix[index][i]) {
                //if in black set means already explored so continue.
                if (black.contains(names[i])) {
                    continue;
                }
                //if in gray set then cycle found.
                if (gray.contains(names[i])) {
                    return true;
                }
                if (dfs(names[i], white, gray, black)) {
                    return true;
                }
            }
        }
        //move vertex from gray set to black set when done exploring.
        moveVertex(current, gray, black);
        return false;
    }

    // moves vertex from one pool to another
    private void moveVertex(String current, ArrayList<String> from,
            ArrayList<String> to) {
        to.add(current);
        from.remove(current);
    }

    // returns index of the vertex with such name
    private int getIndexOfCurrent(String current) {
        for (int i = 0; i < names.length; i++) {
            if (names[i].equals(current)) {
                return i;
            }
        }
        return -1;
    }

    // adds one vertex instead of vertexes, which are in cycle
    public void combineCycle(ArrayList<String> cycle) {
        boolean[] tmp = new boolean[numberOfVertex];
        ArrayList indexes = new ArrayList(); // array with indexes of cycle vertexes
        for (String s : cycle) {
            indexes.add(getIndexOfCurrent(s));
        }
        // combine edges from cycle vertexes to the one raw
        for (int i = 0; i < numberOfVertex; i++) {
            for (int j = 0; j < indexes.size(); j++) {
                tmp[i] = tmp[i] || adjMatrix[(int) indexes.get(j)][i];
            }
        }
        // removes raws of cycle vertexes and adds new raw
        boolean[][] newMatrix = new boolean[numberOfVertex - cycle.size() + 1][numberOfVertex];
        int j = 0;
        for (int i = 0; i < numberOfVertex; i++) {
            if (!indexes.contains(i)) {
                newMatrix[j++] = adjMatrix[i];
            }
        }
        newMatrix[j] = tmp;
        // remove names of cycle vertexes and adds one combine name
        String[] newNames = new String[numberOfVertex - cycle.size() + 1];
        j = 0;
        for (int i = 0; i < numberOfVertex; i++) {
            if (!indexes.contains(i)) {
                newNames[j++] = names[i];
            }
        }
        newNames[j] = cycle.toString();

        // combine edges to cycle vertexes to the one column
        tmp = new boolean[numberOfVertex - cycle.size() + 1];
        for (int i = 0; i <= numberOfVertex - cycle.size(); i++) {
            for (j = 0; j < indexes.size(); j++) {
                tmp[i] = tmp[i] || newMatrix[i][(int) indexes.get(j)];
            }
        }
        adjMatrix = new boolean[numberOfVertex - cycle.size() + 1][numberOfVertex - cycle.size() + 1];
        int k = 0;
        
        // removes coumns of cycle vertexes and adds new column
        for (int i = 0; i <= numberOfVertex - cycle.size(); i++) {
            for (j = 0; j < numberOfVertex; j++) {
                if (!indexes.contains(j)) {
                    adjMatrix[i][k++] = newMatrix[i][j];
                }
            }
            k = 0;
        }
        j = numberOfVertex - cycle.size();
        for (int i = 0; i <= numberOfVertex - cycle.size(); i++) {
            adjMatrix[i][j] = tmp[i];
        }

        // removes loops from adjacency matrix
        for (int i = 0; i < adjMatrix.length; i++) {
            adjMatrix[i][i] = false;
        }

        // updates variables
        names = newNames;
        numberOfVertex = names.length;
    }

    // do topological sort
    public String topSort() {
      // while there are cycles - removes them
        ArrayList<String> cycle = findCycles();
        while (cycle != null) {
            combineCycle(cycle);
            cycle = findCycles();
        }
        // deque with putputing vertexes
        Deque<String> stack = new ArrayDeque<>();
        // array with visited vertexes
        ArrayList<String> visited = new ArrayList<String>();
        for (String current : names) {
          // if vertex is not visited, use topSortUtil
            if (visited.contains(current)) {
                continue;
            }
            topSortUtil(current, stack, visited);
        }
        // generate output string
        String out = "";
        for (String s : stack) {
            out += s + ", ";
        }
        return out.substring(0, out.length() - 2);
    }

    private void topSortUtil(String vertex, Deque<String> stack,
            ArrayList<String> visited) {
        visited.add(vertex); // adds vertex to visited vertex
        int index = getIndexOfCurrent(vertex); // gets index of current vertex
        for (int i = 0; i < names.length; i++) {
            if (adjMatrix[index][i]) {
              // if the child vertex is not visited use topSortUtil for this vertex
                if (visited.contains(names[i])) {
                    continue;
                }
                topSortUtil(names[i], stack, visited);
            }
        }
        stack.offerFirst(vertex); // inserts vertex to stack;
    }

}

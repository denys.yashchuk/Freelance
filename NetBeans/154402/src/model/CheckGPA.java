package model;

import java.io.IOException;
import java.util.Scanner;

public class CheckGPA {

    public static double check(double gpa) {
        if (gpa < 0 || gpa > 4.0) {
            try {
                throw new InvalidGPAException();
            } catch (InvalidGPAException e) {
                Scanner kb = new Scanner(System.in);
                do {
                    System.out.println("Enter a correct GPA: ");
                    return -1;
                    //gpa = kb.nextDouble();
                } while (gpa < 0 || gpa > 4.0);
            }
        }
        return gpa;
    }
}

package model;

import java.io.Serializable;

public class Person implements Serializable {

    private String fName;
    private String lName;
    private String id;
    private String phone;
    private Address address;
    private static int idInt = 0;

    public Person() {
        this.id = "" + idInt++;
        System.out.println(this.id);
    }

    public Person(String fName, String lName, String phone, Address address) {
        this();
        this.fName = fName;
        this.lName = lName;
        this.phone = CheckPhone.check(phone);
        this.address = address;
    }

    public Person(Person person) {
        this(person.fName, person.lName, person.phone, new Address(
                person.address));
    }

    // Getters & setters
    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public Object getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = CheckPhone.check(phone);
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Person [fName=" + fName + ", lName=" + lName + ", id=" + id
                + ", phone=" + phone + ", address=" + address + "]";
    }

}

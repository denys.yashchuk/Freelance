package model;

import java.util.Scanner;

public class CheckPhone {

    public static String check(String phone) {
        Scanner kb = new Scanner(System.in);
        phone = phone.trim();
        phone = phone.replaceAll("[-() ]", "");
        if (phone.length() != 10) {
            try {
                throw new InvalidPhoneException();
            } catch (InvalidPhoneException e) {
                do {
                    System.out.println("Re-enter a 10-digit phone number: ");
                    return null;
                    //phone = kb.nextLine();
                    //phone = phone.trim();
                    //phone = phone.replaceAll("[-()]", "");
                } while (phone.length() != 10);
            }
        }
        return "(" + phone.substring(0, 3) + ") " + phone.substring(3, 6) + "-" + phone.substring(6);
    }
}

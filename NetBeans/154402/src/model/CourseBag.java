package model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.Scanner;

public class CourseBag implements Serializable {

    private Course[] courseBag;
    private int maxSize;
    private int size;

    public CourseBag(int maxSize) {
        this.maxSize = maxSize;
        this.courseBag = new Course[maxSize];
    }

    public void add(Course course) {

        if (size < maxSize) {
            courseBag[size++] = course;
        }

    }

    public Course findBycourseNumber(String courseNumber) {
        Course foundCourse = null;
        for (int i = 0; i < size; i++) {
            if (courseBag[i].getCourseNumber().equals(courseNumber)) {
                foundCourse = courseBag[i];
                break;
            }
        }
        return foundCourse;
    }

    public Course removeBycourseNumber(String courseNumber) {
        Course foundCourse = null;
        for (int i = 0; i < size; i++) {
            if (courseBag[i].getCourseNumber().equals(courseNumber)) {
                foundCourse = courseBag[i];
                System.arraycopy(courseBag, i + 1, courseBag, i, size-- - 1);
                break;
            }
        }
        return foundCourse;
    }

    public void display() {
        for (int i = 0; i < size; i++) {
            System.out.println(courseBag[i]);
        }
    }

    public void importText(String textFileName) {
        try (Scanner sc = OpenFile.open(textFileName)) {
            while (sc.hasNextLine()) {
                String[] tokens = sc.nextLine().split("\t");
                String courserTitle = tokens[0];
                String courseNumber = tokens[1];
                String textbookISBN = tokens[2];
                int numberOfCredits = Integer.parseInt(tokens[3]);
                Course course = new Course(courserTitle, courseNumber, textbookISBN, numberOfCredits);
                courseBag[size++] = course;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

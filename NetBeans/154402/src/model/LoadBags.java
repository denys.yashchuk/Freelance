package model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class LoadBags implements Serializable {

    public static void load(BodyBag bb, CourseBag cb, FacultyBag fb, PersonBag pb, StudentBag sb, TextbookBag tb) {
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream("bb.txt"));
            bb = (BodyBag) in.readObject();
            in = new ObjectInputStream(new FileInputStream("fb.txt"));
            fb = (FacultyBag) in.readObject();
            in = new ObjectInputStream(new FileInputStream("pb.txt"));
            pb = (PersonBag) in.readObject();
            in = new ObjectInputStream(new FileInputStream("sb.txt"));
            sb = (StudentBag) in.readObject();
            in = new ObjectInputStream(new FileInputStream("tb.txt"));
            tb = (TextbookBag) in.readObject();
            in.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}

package model;

import java.io.FileNotFoundException;

public class Demo {

    public static void main(String[] args) throws FileNotFoundException {

        Address address = new Address("21", "Jump", "Street", "NY", "11766");
        Address address1 = new Address("22", "Jump", "Street", "NY", "11766");
        Address address2 = new Address("23", "Jump", "Street", "NY", "11766");
        Address address3 = new Address("24", "Jump", "Street", "NY", "11766");
        Student s1 = new Student("Randy", "Michaels", "631-413-2119", address, "Spanish", null, args, args, 1.5);
        Faculty f1 = new Faculty("Jack", "Johnson", "917-631-2219", address1, "12", 53232.0, "Professor");
        Student s2 = new Student("Jackie", "Parti", "613-213-2144", address2, "English", null, args, args, 4.0);
        Faculty f2 = new Faculty("Nikki", "Carter", "917-531-7551", address3, "18", 50020.0, "Professor");

        BodyBag theBag = new BodyBag(20);

        theBag.importFacultyText("faculties.txt");
        theBag.importStudentText("students.txt");
        theBag.add(f1);
        theBag.add(f2);
        theBag.add(s2);
        theBag.add(s1);

        // theBag.display();
        System.out.println(" The following are unsorted students: ");
        for (Student p : theBag.getStudents()) {
            System.out.println(p);
        }
        System.out.println(" The following are unsorted faculty members: ");
        Faculty[] fy = theBag.getFaculty();
        for (Faculty f : fy) {
            System.out.println(f);
        }
        System.out.println(" The following are students: ");
        for (Person s : theBag.getPersonByType("STUDENT")) {
            System.out.println(s);
        }

        System.out.println("The following are faculty members: ");
        for (Person f : theBag.getPersonByType("FACULTY")) {
            System.out.println(f);
        }

        System.out.println("Sorted Students:");
        Person[] s = theBag.sortStudentsByGPA(theBag.getPersonByType("STUDENT"));

        for (Person stu : s) {
            System.out.println(stu);
        }

        System.out.println("Sorted Faculty: ");

        Person[] f = theBag.sortFacultyBySalary(theBag.getPersonByType("FACULTY"));
        for (Person fac : f) {
            System.out.println(fac);

//		PersonBag personBag = new PersonBag(10);
//		personBag.importText("persons.txt");
//		personBag.display();
//		personBag.findById("1");
//		Person newPerson = new Person("Billy", "Swcheig", "739-234-1358",
//				new Address("7", "truval lane", "Smithtown", "ny", "11194"));
//		personBag.add(newPerson);
//		
//		personBag.removeById("2");
//		personBag.display();
//
//		//this is the student bag
//		StudentBag studentBag = new StudentBag(10);
//		studentBag.importText("students.txt");
//		studentBag.display();
//		System.out.println("________");
//		String[] coursesTook = { "111,101,109" };
//		String[] coursesTaking = { "212, 232 , 234" };
//		String[] coursestoTake = { "912,424,122" };
//		Student newStudent = new Student("John", "Johnson", "917-874-8888",
//				new Address("3", "Hayfield ct", "Mount Sinai", "Ny", "11766"),
//				" Computer", coursesTook, coursesTaking, coursestoTake);
//		studentBag.add(newStudent);
//		studentBag.findById("2");
//		studentBag.removeById("1");
//		studentBag.display();
//
//		//this is the faculty bag
//		FacultyBag facultyBag = new FacultyBag(10);
//		facultyBag.importText("faculties.txt");
//		facultyBag.display();
//		System.out.println("");
//		facultyBag.findById("1");
//		Faculty newFaculty = new Faculty("FirstName", " Last Name", "917-375-9110",
//				new Address(" 4", "johnson Ct", "nesconset", "ny", "11766"),
//				"123 main st", 1234.90, "Professor");
//		facultyBag.add(newFaculty);
//		facultyBag.removeById("4");
//		facultyBag.display();
//
//		
//
//		// CourseBag
//		CourseBag courseBag = new CourseBag(10);
//		courseBag.importText("courses.txt");
//		courseBag.display();
//		System.out.println("---------");
//		courseBag.findByCourseNumber("CST140");
//		
//		
//		Course newCourse = new Course("Physics", "PHY141", "2332-234234", 12);
//		courseBag.add(newCourse);
//		
//		courseBag.removeBycourseNumber("CST141");
//		
//		courseBag.display();
//		
//		// TextbookBag
//		TextbookBag textbookBag = new TextbookBag(10);
//		textbookBag.importText("textbook.txt");
//		textbookBag.display();
//		System.out.println("-------------");
//		textbookBag.findByIsbn("1290-1393");
//		Textbook newTextbook = new Textbook("Physics", "Pearson",
//				"New Brunsik", "2345-12452", 15.312);
//		textbookBag.add(newTextbook);
//		textbookBag.removeByISBN("1290-1393");
//		textbookBag.display();
        }
    }
}

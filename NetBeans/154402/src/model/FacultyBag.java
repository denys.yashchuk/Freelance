package model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.Scanner;

public class FacultyBag implements Serializable {

    private Faculty[] facultyBag;
    private int maxSize;
    private int size;

    public FacultyBag(int maxSize) {
        this.maxSize = maxSize;
        this.facultyBag = new Faculty[maxSize];
    }

    public void add(Faculty faculty) {
        if (size < maxSize) {
            facultyBag[size++] = faculty;
        }
    }

    public Faculty findById(String id) {
        Faculty foundFaculty = null;
        for (int i = 0; i < size; i++) {
            if (facultyBag[i].getId().equals(id)) {
                foundFaculty = facultyBag[i];
                break;
            }
        }
        return foundFaculty;
    }

    public Faculty removeById(String id) {
        Faculty foundFaculty = null;
        for (int i = 0; i < size; i++) {
            if (facultyBag[i].getId().equals(id)) {
                foundFaculty = facultyBag[i];
                System.arraycopy(facultyBag, i + 1, facultyBag, i, size-- - 1);
                break;
            }
        }
        return foundFaculty;
    }

    public void display() {
        for (int i = 0; i < size; i++) {
            System.out.println(facultyBag[i]);
        }
    }

    public void importText(String textFileName) {
        try (Scanner sc = OpenFile.open(textFileName)) {
            while (sc.hasNextLine()) {
                String[] tokens = sc.nextLine().split("\t");
                String fName = tokens[0];
                String lName = tokens[1];
                String phone = tokens[2];
                String stNumber = tokens[3];
                String stName = tokens[4];
                String city = tokens[5];
                String state = tokens[6];
                String zip = tokens[7];
                Address address = new Address(stNumber, stName, city, state, zip);
                String officeAddress = tokens[8];
                double salary = Double.parseDouble(tokens[9]);
                String title = tokens[10];
                Faculty faculty = new Faculty(fName, lName, phone, address, officeAddress, salary, title);
                facultyBag[size++] = faculty;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

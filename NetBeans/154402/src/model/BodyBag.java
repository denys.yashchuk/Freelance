package model;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class BodyBag implements Serializable {

    private Person[] a;
    private int nElems;

    public BodyBag(int maxSize) {
        a = new Person[maxSize];
        nElems = 0;
    }

    public void add(Person person) {
        a[nElems++] = person;
    }

    public void display() {
        System.out.println("asda");
        for (int i = 0; i < nElems; i++) {
            System.out.println(a[i]);
        }
        System.out.println();
    }

    public Person findById(String id) {
        int i = -1;
        for (i = 0; i < nElems; i++) {
            if (a[i].getId().equals(id)) {
                break;
            }
        }
        if (i == nElems) {
            return null;
        } else {
            return a[i];
        }
    }

    public Person removeById(String id) {
        int i = -1;
        for (i = 0; i < nElems; i++) {
            if (a[i].getId().equals(id)) {
                break;
            }
        }
        if (i == nElems) {
            return null;
        } else {
            Person temp = a[i];
            for (int j = i; j < nElems - 1; j++) {
                a[j] = a[j + 1];
            }
            nElems--;
            return temp;
        }
    }

    public void importStudentText(String fileName) throws FileNotFoundException {
        Scanner in = OpenFile.open(fileName);
        while (in.hasNextLine()) {
            String[] data = in.nextLine().split("\t");

            String[] data0 = in.nextLine().split("\t");
            Address adr = new Address(data0[0], data0[1], data0[2], data0[3], data0[4]);

            String[] data1 = in.nextLine().split("\t");

            String[] data2 = in.nextLine().split("\t");

            String[] data3 = in.nextLine().split("\t");
            double gpa = Double.parseDouble(data[4]);

            Person student = new Student(data[0], data[1], data[2], adr, data[3], data1, data2, data3, gpa);
            add(student);

        }
    }

    public void importFacultyText(String fileName) throws FileNotFoundException {
        Scanner in = OpenFile.open(fileName);
        while (in.hasNextLine()) {

            String[] data = in.nextLine().split("\t");

            String[] data0 = in.nextLine().split("\t");
            Address adr = new Address(data0[0], data0[1], data0[2], data0[3], data0[4]);

            double salary = Double.parseDouble(data[4]);
            Person faculty = new Faculty(data[0], data[1], data[2], adr, data[3], salary, data[5]);
            add(faculty);

        }
    }

    public Student[] getStudents() {
        Student[] temp = new Student[a.length];
        int n = 0;
        for (int i = 0; i < nElems; i++) {
            if (a[i] instanceof Student) {
                temp[n++] = (Student) a[i];
            }
        }
        Student[] students = Arrays.copyOf(temp, n);
        return students;
    }

    public Faculty[] getFaculty() {
        Faculty[] temp = new Faculty[a.length];
        int n = 0;
        for (int i = 0; i < nElems; i++) {
            if (a[i] instanceof Faculty) {
                temp[n++] = (Faculty) a[i];
            }
        }
        Faculty[] faculty = Arrays.copyOf(temp, n);
        return faculty;
    }

    public Person[] getPersonByType(String type) {

        if (type.equals("STUDENT")) {
            ArrayList<Student> pList = new ArrayList<>();
            for (int i = 0; i < nElems; i++) {
                if (a[i] instanceof Student) {
                    pList.add((Student) a[i]);
                }
            }
            Person[] p = pList.toArray(new Person[0]);
            return p;

        } else if (type.equals("FACULTY")) {
            ArrayList<Faculty> pList = new ArrayList<>();
            for (int i = 0; i < nElems; i++) {
                if (a[i] instanceof Faculty) {
                    pList.add((Faculty) a[i]);
                }
            }
            Person[] p = pList.toArray(new Person[0]);
            return p;
        } else {
            return null;
        }
    }

    public Person[] sortStudentsByGPA(Person[] persons) {
        for (int out = 0; out < persons.length - 1; out++) {
            int min = out;
            for (int in = out + 1; in < persons.length; in++) {
                if (((Student) (persons[in])).getGpa()
                        < ((Student) persons[min]).getGpa()) {
                    min = in;
                }
            }
            persons = swap(persons, out, min);
        }
        return persons;
    }

    public Person[] sortFacultyBySalary(Person[] persons) {
        for (int out = 0; out < persons.length; out++) {
            int min = out;
            for (int in = out + 1; in < persons.length; in++) {
                if (((Faculty) (persons[in])).getSalary() < ((Faculty) persons[min]).getSalary()) {
                    min = in;
                }
            }
            persons = swap(persons, out, min);
        }
        return persons;
    }

    private Person[] swap(Person[] persons, int index1, int index2) {
        Person temp = persons[index2];
        persons[index2] = persons[index1];
        persons[index1] = temp;
        return persons;
    }

    public void displayStudents() {
        for (Student s : getStudents()) {
            System.out.println(s);
        }
    }

    public void printStudents(String filename) throws FileNotFoundException {
        PrintWriter pw = new PrintWriter(filename);
        for (Student s : getStudents()) {
            pw.println(s);
            pw.close();
        }
    }

    public void displaySortedStudents() {
        for (Person s : sortStudentsByGPA(getStudents())) {
            System.out.println(s);
        }
    }

    public void printSortedStudents(String filename) throws FileNotFoundException {
        PrintWriter pw = new PrintWriter(filename);
        for (Person s : sortStudentsByGPA(getStudents())) {
            pw.println(s);
            pw.close();
        }
    }
}

package model;

import java.util.Scanner;

public class CheckZIP {

    public static String check(String zip) {
        Scanner sc = new Scanner(System.in);
        zip = zip.trim();
        if (zip.length() != 5) {
            try {
                throw new InvalidZIPException();
            } catch (InvalidZIPException e) {
                do {
                    System.out.println("Re-enter 5-digit zip code: ");
                    return null;
                    //zip = sc.nextLine();
                    //zip = zip.trim();
                } while (zip.length() != 5);
            }
        }
        if (!checkInteger(zip)) {
            do {
                System.out.println("Re-enter 5-digit zip code: ");
                return null;
                //zip = sc.nextLine();
                //zip = zip.trim();
            } while (checkInteger(zip));
        }
        return zip;
    }

    private static boolean checkInteger(String s) {
        try {
            Integer.parseInt(s);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}

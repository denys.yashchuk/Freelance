package model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.Scanner;

public class PersonBag implements Serializable {

    private Person[] personBag;
    private int maxSize;
    private int size;

    public PersonBag(int maxSize) {
        this.maxSize = maxSize;
        this.personBag = new Person[maxSize];
    }

    public void add(Person person) {
        if (size < maxSize) {
            personBag[size++] = person;
        }
    }

    public Person findById(String id) {
        Person foundPerson = null;
        for (int i = 0; i < size; i++) {
            if (personBag[i].getId().equals(id)) {
                foundPerson = personBag[i];
                break;
            }
        }
        return foundPerson;
    }

    public Person removeById(String id) {
        Person foundPerson = null;
        for (int i = 0; i < size; i++) {
            if (personBag[i].getId().equals(id)) {
                foundPerson = personBag[i];
                System.arraycopy(personBag, i + 1, personBag, i, size-- - 1);
                break;
            }
        }
        return foundPerson;
    }

    public void display() {
        for (int i = 0; i < size; i++) {
            System.out.println(personBag[i]);
        }
    }

    public void importText(String textFileName) {
        try (Scanner sc = OpenFile.open(textFileName)) {
            while (sc.hasNextLine()) {
                String[] tokens = sc.nextLine().split("\t");
                String fName = tokens[0];
                String lName = tokens[1];
                String phone = tokens[2];
                String stNumber = tokens[3];
                String stName = tokens[4];
                String city = tokens[5];
                String state = tokens[6];
                String zip = tokens[7];
                Address address = new Address(stNumber, stName, city, state, zip);
                Person person = new Person(fName, lName, phone, address);
                personBag[size++] = person;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

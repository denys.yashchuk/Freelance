package model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.Scanner;

public class OpenFile implements Serializable {

    public static Scanner open(String fileName) {
        File f = new File(fileName);
        Scanner input = null;
        Scanner kb = new Scanner(System.in);
        do {
            try {
                input = new Scanner(f);
            } catch (FileNotFoundException e) {
                try {
                    throw new InvalidFileNameException();
                } catch (InvalidFileNameException e1) {
                    System.out.println("Enter a new file name: ");
                    String name = kb.nextLine();
                    f = new File(name);
                }
            }
        } while (input == null);
        return input;
    }
}

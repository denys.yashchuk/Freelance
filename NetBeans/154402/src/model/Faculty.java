package model;

import java.io.Serializable;

public class Faculty extends Person implements Serializable {

    private String officeAddress;
    private double salary;
    private String title;

    public Faculty() {
        super();
    }

    public Faculty(String fName, String lName, String phone, Address address,
            String officeAddress, double salary, String title) {
        super();
        setfName(fName);
        setlName(lName);
        setPhone(phone);
        setAddress(address);
        this.officeAddress = officeAddress;
        this.salary = salary;
        this.title = title;
    }

    public Faculty(Faculty faculty) {
        this(faculty.getfName(), faculty.getlName(), faculty.getPhone(), new Address(
                faculty.getAddress()), faculty.officeAddress, faculty.salary,
                faculty.title);
    }

    // Getters & setters
    public String getOfficeAddress() {
        return officeAddress;
    }

    public void setOfficeAddress(String officeAddress) {
        this.officeAddress = officeAddress;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Faculty [officeAddress=" + officeAddress + ", salary=" + salary
                + ", title=" + title + ", fName=" + getfName() + ", lName=" + getlName()
                + ", id=" + getId() + ", phone=" + getPhone() + ", address=" + getAddress()
                + "]";
    }
}

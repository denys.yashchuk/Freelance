package model;

import java.io.Serializable;

public class Textbook implements Serializable {

    private String title;
    private String author;
    private String publisher;
    private String isbn;
    private double price;

    public Textbook() {

    }

    public Textbook(String title, String author, String publisher, String isbn, double price) {
        this.title = title;
        this.author = author;
        this.publisher = publisher;
        this.isbn = isbn;
        this.price = price;
    }

    public Textbook(Textbook textbook) {
        this(textbook.title, textbook.author, textbook.publisher, textbook.isbn, textbook.price);
    }

    // Getters & setters
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Textbook [title=" + title + ", author=" + author
                + ", publisher=" + publisher + ", isbn=" + isbn + ", price="
                + price + "]";
    }

}

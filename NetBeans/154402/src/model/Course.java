package model;

import java.io.Serializable;

public class Course implements Serializable {

    private String courseTitle;
    private String courseNumber;
    private String textbookISBN;
    private int numberOfCredits;

    // fixed error
    public Course() {

    }

    public Course(String courserTitle, String courseNumber,
            String textbookISBN, int numberOfCredits) {
        this.courseTitle = courserTitle;
        this.courseNumber = courseNumber;
        this.textbookISBN = textbookISBN;
        this.numberOfCredits = numberOfCredits;
    }

    public Course(Course course) {
        this(course.courseTitle, course.courseNumber, course.textbookISBN,
                course.numberOfCredits);
    }

    @Override
    public String toString() {
        return "Course [courserTitle=" + courseTitle + ", courseNumber="
                + courseNumber + ", textbookISBN=" + textbookISBN
                + ", numberOfCredits=" + numberOfCredits + "]";
    }

    // Getters & setters
    public String getCourserTitle() {
        return courseTitle;
    }

    public void setCourserTitle(String courserTitle) {
        this.courseTitle = courserTitle;
    }

    public String getCourseNumber() {
        return courseNumber;
    }

    public void setCourseNumber(String courseNumber) {
        this.courseNumber = courseNumber;
    }

    public String getTextbookISBN() {
        return textbookISBN;
    }

    public void setTextbookISBN(String textbookISBN) {
        this.textbookISBN = textbookISBN;
    }

    public int getNumberOfCredits() {
        return numberOfCredits;
    }

    public void setNumberOfCredits(int numberOfCredits) {
        this.numberOfCredits = numberOfCredits;
    }

}

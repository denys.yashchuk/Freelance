package model;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class SaveBags {

    public static void save(BodyBag bb, CourseBag cb, FacultyBag fb, PersonBag pb, StudentBag sb, TextbookBag tb) {
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("bb.txt"));
            out.writeObject(bb);
            out = new ObjectOutputStream(new FileOutputStream("fb.txt"));
            out.writeObject(fb);
            out = new ObjectOutputStream(new FileOutputStream("pb.txt"));
            out.writeObject(pb);
            out = new ObjectOutputStream(new FileOutputStream("sb.txt"));
            out.writeObject(sb);
            out = new ObjectOutputStream(new FileOutputStream("tb.txt"));
            out.writeObject(tb);
            out.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}

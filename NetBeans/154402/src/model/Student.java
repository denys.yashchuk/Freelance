package model;

import java.io.Serializable;
import java.util.Arrays;

public class Student extends Person implements Serializable {

    private String major;
    private String[] coursesTook;
    private String[] coursesTaking;
    private String[] coursesToTake;
    private double gpa;

    public Student() {
        super();
    }

    public Student(String fName, String lName, String phone, Address address,
            String major, String[] coursesTook, String[] coursesTaking,
            String[] coursesToTake, double gpa) {
        super();
        setfName(fName);
        setlName(lName);
        setPhone(phone);
        setAddress(address);
        this.major = major;
        this.setCoursesTook(coursesTook);
        this.setCoursesTaking(coursesTaking);
        this.setCoursesToTake(coursesToTake);
        this.gpa = CheckGPA.check(gpa);
    }

    @Override
    public String toString() {
        return "Student [fName=" + getfName() + ", lName=" + getlName() + ", id=" + getId()
                + ", phone=" + getPhone() + ", address=" + getAddress() + ", major="
                + major + ", coursesTook=" + Arrays.toString(coursesTook)
                + ", coursesTaking=" + Arrays.toString(coursesTaking)
                + ", coursesToTake=" + Arrays.toString(coursesToTake)
                + ", gpa=" + gpa + "]";
    }

    public Student(Student student) {
        this(student.getfName(), student.getlName(), student.getPhone(), new Address(
                student.getAddress()), student.major, null, null, null, student.gpa);
    }

    public double getGpa() {
        return gpa;
    }

    public void setGpa(double gpa) {
        this.gpa = CheckGPA.check(gpa);
    }

    public String[] getCoursesTook() {
        return coursesTook;
    }

    public void setCoursesTook(String[] coursesTook) {
        this.coursesTook = coursesTook;
    }

    public String[] getCoursesTaking() {
        return coursesTaking;
    }

    public void setCoursesTaking(String[] coursesTaking) {
        this.coursesTaking = coursesTaking;
    }

    public String[] getCoursesToTake() {
        return coursesToTake;
    }

    public void setCoursesToTake(String[] coursesToTake) {
        this.coursesToTake = coursesToTake;
    }
}

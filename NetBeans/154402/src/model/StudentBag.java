package model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.Scanner;

public class StudentBag implements Serializable {

    private Student[] studentBag;
    private int maxSize;
    private int size;

    /**
     * Constructor
     *
     * @param maxSize
     */
    public StudentBag(int maxSize) {
        this.maxSize = maxSize;
        this.studentBag = new Student[maxSize];
    }

    /**
     * Add a Student object to bag
     *
     * @param student
     */
    public void add(Student student) {
        if (size < maxSize) {
            studentBag[size++] = student;
        }
    }

    public Student findById(String id) {
        Student foundStudent = null;
        for (int i = 0; i < size; i++) {
            if (studentBag[i].getId().equals(id)) {
                foundStudent = studentBag[i];
                break;
            }
        }
        return foundStudent;
    }

    public Student removeById(String id) {
        Student foundStudent = null;
        for (int i = 0; i < size; i++) {
            if (studentBag[i].getId().equals(id)) {
                foundStudent = studentBag[i];

                System.arraycopy(studentBag, i + 1, studentBag, i, size-- - 1);
                break;
            }
        }
        return foundStudent;
    }

    public void display() {
        for (int i = 0; i < size; i++) {
            System.out.println(studentBag[i]);
        }
    }

    public void importText(String textFileName) {
        try (Scanner sc = OpenFile.open(textFileName)) {
            while (sc.hasNextLine()) {
                String[] tokens = sc.nextLine().split("\t");
                String fName = tokens[0];
                String lName = tokens[1];
                String phone = tokens[2];
                String stNumber = tokens[3];
                String stName = tokens[4];
                String city = tokens[5];
                String state = tokens[6];
                String zip = tokens[7];
                Address address = new Address(stNumber, stName, city, state, zip);
                String major = tokens[8];
                String[] coursesTook = sc.nextLine().split("\t");
                String[] coursesTaking = sc.nextLine().split("\t");
                String[] coursesToTake = sc.nextLine().split("\t");
                //Student student = new Student(fName, lName, phone, address, major, coursesTook, coursesTaking, coursesToTake);
                //studentBag[size++] = student;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

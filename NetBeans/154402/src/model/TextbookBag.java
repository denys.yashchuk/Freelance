package model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.Scanner;

public class TextbookBag implements Serializable {

    private Textbook[] textbookBag;
    private int maxSize;
    private int size;

    public TextbookBag(int maxSize) {
        this.maxSize = maxSize;
        this.textbookBag = new Textbook[maxSize];
    }

    public void add(Textbook textbook) {
        if (size < maxSize) {
            textbookBag[size++] = textbook;
        }
    }

    public Textbook findByIsbn(String isbn) {
        Textbook foundTextbook = null;
        for (int i = 0; i < size; i++) {
            if (textbookBag[i].getIsbn().equals(isbn)) {
                foundTextbook = textbookBag[i];
                break;
            }
        }
        return foundTextbook;
    }

    public Textbook removeByIsbn(String isbn) {
        Textbook foundTextbook = null;
        for (int i = 0; i < size; i++) {
            if (textbookBag[i].getIsbn().equals(isbn)) {
                foundTextbook = textbookBag[i];
                System.arraycopy(textbookBag, i + 1, textbookBag, i, size-- - 1);
                break;
            }
        }
        return foundTextbook;
    }

    public void display() {
        for (int i = 0; i < size; i++) {
            System.out.println(textbookBag[i]);
        }
    }

    public void importText(String textFileName) {
        try (Scanner sc = OpenFile.open(textFileName)) {
            while (sc.hasNextLine()) {
                String[] tokens = sc.nextLine().split("\t");
                String title = tokens[0];
                String author = tokens[1];
                String publisher = tokens[2];
                String isbn = tokens[3];
                double price = Double.parseDouble(tokens[4]);
                Textbook textbook = new Textbook(title, author, publisher, isbn, price);
                textbookBag[size++] = textbook;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

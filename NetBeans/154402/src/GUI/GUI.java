package GUI;

import javafx.application.*;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import model.Address;
import model.BodyBag;
import model.CheckGPA;
import model.CheckPhone;
import model.CheckZIP;
import model.Faculty;
import model.Person;
import model.Student;

public class GUI extends Application {

    BodyBag theBag = new BodyBag(20);

    // Labels and TextFields for input
    Label firstNameL = new Label("First name:");
    TextField firstNameT = new TextField();
    Label secondNameL = new Label("Second name:");
    TextField secondNameT = new TextField();
    Label phoneL = new Label("Phone:");
    TextField phoneT = new TextField();
    Label majorL = new Label("Major:");
    TextField majorT = new TextField();
    Label stNumberL = new Label("Street Number:");
    TextField stNumberT = new TextField();
    Label stNameL = new Label("Street Name:");
    TextField stNameT = new TextField();
    Label cityL = new Label("City:");
    TextField cityT = new TextField();
    Label stateL = new Label("State:");
    TextField stateT = new TextField();
    Label zipL = new Label("ZIP:");
    TextField zipT = new TextField();
    Label gpaL = new Label("GPA:");
    TextField gpaT = new TextField();
    Label memberIdL = new Label("Member ID:");
    TextField memberIdT = new TextField();
    Label officeAddressL = new Label("Office Address:");
    TextField officeAddressT = new TextField();
    Label salaryL = new Label("Salary:");
    TextField salaryT = new TextField();
    Label titleL = new Label("Title:");
    TextField titleT = new TextField();

    // HBoxes, which include Labels and TextFields for input
    HBox fName = new HBox(10);
    HBox sName = new HBox(10);
    HBox phone = new HBox(10);
    HBox stNumber = new HBox(10);
    HBox stName = new HBox(10);
    HBox city = new HBox(10);
    HBox state = new HBox(10);
    HBox zip = new HBox(10);
    HBox major = new HBox(10);
    HBox gpa = new HBox(10);
    HBox officeAddress = new HBox(10);
    HBox salary = new HBox(10);
    HBox title = new HBox(10);
    HBox memberId = new HBox(10);

    public static void main(String[] args) {
        launch(args); // launch GUI
    }

    //The application initialization method.
    @Override
    public void init() throws Exception {
        // TODO Auto-generated method stub
        super.init();
        createHBoxes();
    }

    //The main entry point for all JavaFX applications.
    @Override
    public void start(Stage myStage) throws Exception {
        // TODO Auto-generated method stub

        myStage.setTitle("System"); // sets title

        BorderPane rootNode = new BorderPane(); // creates border pane
        rootNode.setPadding(new Insets(20, 20, 20, 20)); // adds padding to the
        // pane
        VBox left = new VBox(10); // creates left part (vertical column with objects and spacing 10 px)
        left.setAlignment(Pos.CENTER); // sets center alignment to the
        // elements in the left part
        Label menu = new Label("Menu"); // Simple label with the text

        Button addStudent = new Button("Add Student"); // button

        // on button clicked add addStudent interface to the right part of window
        addStudent.setOnAction((event) -> {
            rootNode.setRight(addStudentVBox());
        });

        Button addFaculty = new Button("Add Faculty");

        // on button clicked add addFaculty interface to the right part of window
        addFaculty.setOnAction((event) -> {
            rootNode.setRight(addFacultyVBox());
        });

        Button remove = new Button("Remove Member");

        // on button clicked add removeMember interface to the right part of window
        remove.setOnAction((event) -> {
            rootNode.setRight(removeVBox());
        });

        Button search = new Button("Search Member");

        // on button clicked add searchMember interface to the right part of window
        search.setOnAction((event) -> {
            rootNode.setRight(searchVBox());
        });

        left.getChildren().addAll(menu, addStudent, addFaculty, remove, search); // adds label and buttons to the left part
        rootNode.setLeft(left); // adds left part to the root node
        rootNode.setRight(addStudentVBox()); // add addStudent interface to the right part
        Scene myScene = new Scene(rootNode, 500, 500); // creates scene with the
        // root node and size 500*500

        myStage.setScene(myScene); // sets scene to the stage
        myStage.show(); // shows stage
    }

    // adds Labels and TextFields to the HBoxes
    private void createHBoxes() {
        fName.setAlignment(Pos.CENTER_RIGHT);
        fName.getChildren().addAll(firstNameL, firstNameT);

        sName.setAlignment(Pos.CENTER_RIGHT);
        sName.getChildren().addAll(secondNameL, secondNameT);

        phone.setAlignment(Pos.CENTER_RIGHT);
        phone.getChildren().addAll(phoneL, phoneT);

        stNumber.setAlignment(Pos.CENTER_RIGHT);
        stNumber.getChildren().addAll(stNumberL, stNumberT);

        stName.setAlignment(Pos.CENTER_RIGHT);
        stName.getChildren().addAll(stNameL, stNameT);

        city.setAlignment(Pos.CENTER_RIGHT);
        city.getChildren().addAll(cityL, cityT);

        state.setAlignment(Pos.CENTER_RIGHT);
        state.getChildren().addAll(stateL, stateT);

        zip.setAlignment(Pos.CENTER_RIGHT);
        zip.getChildren().addAll(zipL, zipT);

        major.setAlignment(Pos.CENTER_RIGHT);
        major.getChildren().addAll(majorL, majorT);

        gpa.setAlignment(Pos.CENTER_RIGHT);
        gpa.getChildren().addAll(gpaL, gpaT);

        officeAddress.setAlignment(Pos.CENTER_RIGHT);
        officeAddress.getChildren().addAll(officeAddressL, officeAddressT);

        salary.setAlignment(Pos.CENTER_RIGHT);
        salary.getChildren().addAll(salaryL, salaryT);

        title.setAlignment(Pos.CENTER_RIGHT);
        title.getChildren().addAll(titleL, titleT);

        memberId.setAlignment(Pos.CENTER_RIGHT);
        memberId.getChildren().addAll(memberIdL, memberIdT);
    }

    // creates addStudent Interface
    private VBox addStudentVBox() {
        VBox addStudent = new VBox(10); // creates VBox
        addStudent.setAlignment(Pos.CENTER); // set center alignment in the VBox

        Label boxName = new Label("Add Student"); // creates label

        Button add = new Button("Add Student"); // creates button
        add.setOnAction((event) -> {
            String ph = phoneT.getText(); // get text from TextField to String
            String zipS = zipT.getText();
            String gpaS = gpaT.getText();
            double gpaD;
            try {
                gpaD = Double.parseDouble(gpaS); // parse double value from String
                if (CheckPhone.check(ph) == null) {
                    // shows ERROR window
                    JOptionPane.showMessageDialog(new JFrame(),
                            "Re-enter a 10-digit phone number", "ERROR",
                            JOptionPane.ERROR_MESSAGE);
                } else if (CheckZIP.check(zipS) == null) {
                    // shows ERROR window
                    JOptionPane.showMessageDialog(new JFrame(),
                            "Re-enter 5-digit zip code", "ERROR",
                            JOptionPane.ERROR_MESSAGE);
                } else if (CheckGPA.check(gpaD) == -1) {
                    // shows ERROR window
                    JOptionPane.showMessageDialog(new JFrame(),
                            "Enter a correct GPA", "ERROR",
                            JOptionPane.ERROR_MESSAGE);
                } else {
                    // creates Address and Student objects and adds it to the Bag
                    Address a = new Address(stNumberT.getText(), stNameT.getText(), cityT.getText(),
                            stateT.getText(), zipS);
                    Student s = new Student(firstNameT.getText(), secondNameT.getText(),
                            ph, a, majorT.getText(), null, null, null, gpaD);
                    theBag.add(s);
                }
            } catch (Exception e) {
                // shows ERROR window
                JOptionPane.showMessageDialog(new JFrame(),
                        "Enter a correct GPA", "ERROR",
                        JOptionPane.ERROR_MESSAGE);
            }
            // clear all TextFields of this interface
            firstNameT.clear();
            secondNameT.clear();
            phoneT.clear();
            stNumberT.clear();
            stNameT.clear();
            cityT.clear();
            stateT.clear();
            zipT.clear();
            majorT.clear();
            gpaT.clear();
        });

        // adds elements to the VBox
        addStudent.getChildren().addAll(boxName, fName, sName, phone, stNumber,
                stName, city, state, zip, major, gpa, add);
        return addStudent;
    }

    private VBox addFacultyVBox() {
        VBox addFaculty = new VBox(10);
        addFaculty.setAlignment(Pos.CENTER);

        Label boxName = new Label("Add Faculty");

        Button add = new Button("Add Faculty");
        add.setOnAction((event) -> {
            String ph = phoneT.getText();
            String zipS = zipT.getText();
            String gpaS = gpaT.getText();
            double salaryD;
            try {
                salaryD = Double.parseDouble(salaryT.getText());
                if (CheckPhone.check(ph) == null) {
                    JOptionPane.showMessageDialog(new JFrame(),
                            "Re-enter a 10-digit phone number", "ERROR",
                            JOptionPane.ERROR_MESSAGE);
                } else if (CheckZIP.check(zipS) == null) {
                    JOptionPane.showMessageDialog(new JFrame(),
                            "Re-enter 5-digit zip code", "ERROR",
                            JOptionPane.ERROR_MESSAGE);
                } else {
                    Address a = new Address(stNumberT.getText(), stNameT.getText(), cityT.getText(),
                            stateT.getText(), zipT.getText());
                    Faculty f = new Faculty(firstNameT.getText(), secondNameT.getText(),
                            phoneT.getText(), a, officeAddressT.getText(),
                            salaryD, titleT.getText());
                    theBag.add(f);
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(new JFrame(),
                        "Enter a correct salary", "ERROR",
                        JOptionPane.ERROR_MESSAGE);
            }
            firstNameT.clear();
            secondNameT.clear();
            phoneT.clear();
            stNumberT.clear();
            stNameT.clear();
            cityT.clear();
            stateT.clear();
            zipT.clear();
            officeAddressT.clear();
            salaryT.clear();
            titleT.clear();
        });

        addFaculty.getChildren().addAll(boxName, fName, sName, phone, stNumber,
                stName, city, state, zip, officeAddress, salary, title, add);
        return addFaculty;
    }

    private VBox removeVBox() {
        VBox removeMember = new VBox(10);
        removeMember.setAlignment(Pos.CENTER);

        Label boxName = new Label("Remove Member");

        Button remove = new Button("Remove member");
        remove.setOnAction((event) -> {
            // removes member from the Bag and shows removed member or message if there is no member with such id;
            Person p = theBag.removeById(memberIdT.getText());
            JOptionPane.showMessageDialog(new JFrame(),
                    (p == null ? "There is no member with such id" : p.toString() + " removed"));
            memberIdT.clear();
        });

        removeMember.getChildren().addAll(boxName, memberId, remove);
        return removeMember;

    }

    private VBox searchVBox() {
        VBox searchMember = new VBox(10);
        searchMember.setAlignment(Pos.CENTER);

        Label boxName = new Label("Search Member");

        Button search = new Button("Search member");
        search.setOnAction((event) -> {
            // shows member with such id or message if there is no member with such id;
            Person p = theBag.findById(memberIdT.getText());
            JOptionPane.showMessageDialog(new JFrame(),
                    (p == null ? "There is no member with such id" : p.toString()));
            memberIdT.clear();
        });

        searchMember.getChildren().addAll(boxName, memberId, search);
        return searchMember;

    }

}


import java.util.Scanner;

public class HighArrayApp {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);// uses for reading from console;
        String input;
        do {
            System.out.print("Write the number of elements in the array: ");
            input = sc.nextLine(); // read line from the console
        } while (!input.matches("\\d+")); // while input is not numeric String

        HighArray arr = new HighArray(Integer.parseInt(input)); // creates HighArray

        boolean working = true; // program repeating while this variable is true;
        long num;
        
        while (working) {
            System.out.println("1)Insert a data\n"
                    + "2)Remove a data\n"
                    + "3)Search a data\n"
                    + "4)Display all data\n"
                    + "5)Quit "); // shows menu
            switch (sc.nextLine()) {
                case "1":
                    do {
                        System.out.print("Write the number to insert: ");
                        input = sc.nextLine();
                    } while (!input.matches("\\d+"));
                    num = Long.parseLong(input); // read element from console
                    if(arr.insert(num)){
                        System.out.println(num + " was inserted successfully."); // if element was inserted
                    } else {
                        System.out.println("Array is full."); // if element was not inserted
                    }
                    System.out.println();
                    break;
                case "2":
                    do {
                        System.out.print("Write the number to remove: ");
                        input = sc.nextLine();
                    } while (!input.matches("\\d+"));
                    num = Long.parseLong(input); // read element from console
                    if(arr.delete(num)){
                        System.out.println(num + " was removed successfully."); // if element was removed
                    } else {
                        System.out.println("There is not "+num+" in the array."); // if element was not removed
                    }
                    System.out.println();
                    break;
                case "3":
                    do {
                        System.out.print("Write the number to search: ");
                        input = sc.nextLine();
                    } while (!input.matches("\\d+"));
                    num = Long.parseLong(input); // read element from console
                    if (arr.find(num)) {
                        System.out.println(num + " was found");  // if element was find
                    } else {
                        System.out.println(num + " was not found");  // if element was not find
                    }
                    break;
                case "4":
                    System.out.println(arr); // shows all elements in array
                    break;
                case "5":
                    working = false; // stop the program
                    break;
                default:
                    // if wrong input was entered;
                    System.out.println("Enter only 1 number (1,2,3,4 or 5)");
                    break;
            }
        }
    }

}

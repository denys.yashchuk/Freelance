// highArray.java
// demonstrates array class with high-level interface
// to run this program: C>java HighArrayApp
////////////////////////////////////////////////////////////////

class HighArray {

    private long[] a; // ref to array a
    private int nElems; // number of data items
//-----------------------------------------------------------

    /**
     * Constructor. Creates array, sets the number of elements to 0
     * @param max - the size of array
     */
    public HighArray(int max) // constructor
    {
        a = new long[max]; // create the array
        nElems = 0; // no items yet
    }
//-----------------------------------------------------------

    /**
     * Method for finding the element in the array
     * @param searchKey - element for finding
     * @return true - there is such element in array; 
     *         false - there is not such element in array;
     */
    public boolean find(long searchKey) { // find specified value
        int j;
        for (j = 0; j < nElems; j++) {
            if (a[j] == searchKey) {
                break;
            }
        }
        if (j == nElems) // gone to end?
        {
            return false; // yes, can’t find it
        } else {
            return true; // no, found it
        }
    } // end find()
//-----------------------------------------------------------

    /**
     * Inserts element to the array
     * @param value - element for inserting
     * @return true - element was inserted;
     *         false - array is full;
     */
    public boolean insert(long value) // put element into array
    {
        if (nElems == a.length) {
            return false;
        } else {
            a[nElems] = value; // insert it
            nElems++; // increment size
            return true;
        }
    }
//-----------------------------------------------------------

    /**
     * Delets element from the array
     * @param value - element for deleting
     * @return true - element was deleted;
     *         false - there is not such element in array;
     */
    public boolean delete(long value) {
        int j;
        for (j = 0; j < nElems; j++) {
            if (value == a[j]) {
                break;
            }
        }
        if (j == nElems) // can’t find it
        {
            return false;
        } else // found it
        {
            for (int k = j; k < nElems - 1; k++) {
                a[k] = a[k + 1];
            }
            nElems--; // decrement size
            return true;
        }
    } // end delete()
//-----------------------------------------------------------

    /**
     * @return elements in the array as a String
     */
    @Override
    public String toString() {
        String out = "";
        for (int j = 0; j < nElems; j++) {
            out += a[j] + " ";
        }
        return out;
    }

}
//-----------------------------------------------------------
// end class HighArray
////////////////////////////////////////////////////////////////

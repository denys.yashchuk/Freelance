
import java.text.DecimalFormat;
import java.util.Scanner;

public class Duration {
    
    public static void main(String[] args){
        DecimalFormat df = new DecimalFormat("00");
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter number of seconds: ");
        int seconds = sc.nextInt();
        sc.close();
        int hours = seconds/3600;
        seconds = seconds%3600;
        int minutes = seconds/60;
        seconds = seconds%60;
        System.out.println("Duration: "+df.format(hours)+":"+df.format(minutes)+":"+df.format(seconds));
    }
    
}

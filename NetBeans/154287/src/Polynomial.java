
import java.util.LinkedList;
import java.util.ListIterator;

public class Polynomial {
    // this is a nested static class, it defines a type
    // all the instance varaibles can be access directly inside Polynomial class even though they have
    // private modifier
    private static class Term {

        private double coefficient;
        private int exponent;

        public Term(int exp, double coeff) {
            coefficient = coeff;
            exponent = exp;
        }
    }
    // instance variables of Polynomial
    // first is the term of the Polynomial with the highest degree
    private LinkedList<Term> termList;

    /**
     * Postcondition:</b> Creates a polynomial which is 0.
    * *
     */
    public Polynomial() {
        termList = new LinkedList<Term>();
        termList.add(new Term(0, 0));
    }

    /**
     * Postcondition:</b> Creates a polynomial which has a single term a0*x^0
     *
     * @param a0 The value to be set as the coefficient of the constant (x^0)
     * term.
    * *
     */
    public Polynomial(double a0) {
        termList = new LinkedList<Term>();
        termList.add(new Term(0, a0));
    }

    /** <b>Postcondition:</b> Creates a copy of Polynomial p
     *
     * @param p the Polynomial which is to be copied.
  * *
     */
    public Polynomial(Polynomial p) {
        this.termList = new LinkedList<Term>();
        for(Term t : p.termList){
            this.termList.add(new Term(t.exponent,t.coefficient));
        }
    }

    /** <b>Postcondition:</b> Adds the given amount to the coefficient of the
     * specified exponent.
     *
     * @param amount The amount to be added to the coefficient.
     * @param exponent The degree of the term whose coefficient is to be
     * modified. (1) Note that the exponent can be arbitrary (2) If you want,
     * you can assume the amount is not 0, however, it is possible that after
     * you add the amount, the coefficient becomes 0, in which case, you should
     * delete the TermNode
  * *
     */
    public void add_to_coef(double amount, int exponent) {
        
        for(Term t : termList){
            if(t.exponent == exponent){
                t.coefficient += amount;
                return;
            }
        }
        int i = termList.size();
        while(i<exponent){
            termList.add(new Term(i,0));
            i++;
        }
        termList.add(new Term(exponent, amount));
    }

    /** <b>Postcondition:</b> Sets the coefficient of a specified term to a
     * specified value.
     *
     * @param coefficient The new value of the coefficient.
     * @param exponent The degree of the term whose coefficient is to be
     * modified. (1) Note that the exponent can be arbitrary (2) The coefficient
     * may be 0
  * *
     */
    public void assign_coef(double coefficient, int exponent) {
        for(Term t : termList){
            if(t.exponent == exponent){
                t.coefficient = coefficient;
                return;
            }
        }
        int i = termList.size();
        while(i<exponent){
            termList.add(new Term(i,0));
            i++;
        }
        termList.add(new Term(exponent, coefficient));
    }

    /** <b>Postcondition:</b> Returns coefficient at specified exponent of this
     * polynomial.
     *
     * @param exponent The exponent of the term whose coefficient is sought.
     * @return The coefficient of the term.
     * @throws Exception if the degree of the activating polynomial is less than
     * that of the requested term.
  * *
     */
    public double coefficient(int exponent) {
        for(Term t : termList){
            if(t.exponent == exponent){
                return t.coefficient;
            }
        }
        
        return 0;
    }

    /**
     * @return The value of this Polynomial with the given value for the
     * variable x.
     * @param x The value at which the Polynomial is to be evaluated. using
     * Horner's method to evaluation see the link here
     * https://en.wikipedia.org/wiki/Horner%27s_method
     *
     **
     */
    public double eval(double x) {
        double result = 0;
        for(int i = termList.size() - 1; i >= 0; i--){
            result = termList.get(i).coefficient + (x * result);
        }
        return result;
    }

    /**
     * @return True if p and this polynomial is same
     * @param p The polynomial to be tested for equality.
    *
     */
    public boolean equals(Object obj) {
        if (obj instanceof Polynomial) {
            Polynomial p = (Polynomial) obj;
            if(p.termList.size()!=this.termList.size())
                return false;
            for(int i = 0; i < p.termList.size(); i++){
                if(p.termList.get(i).exponent != this.termList.get(i).exponent
                        || p.termList.get(i).coefficient != this.termList.get(i).coefficient)
                    return false;
            }
        }
        return true;
    }

    /**
     * @return Returns a string representing the polynomial expression with
     * coefficients displayed to the tenths place, omitting any coefficients
     * that are zero. If all coefficients are 0, then the zero function is
     * reported.
     *
     *
     */
    public String toString() {
        String out = "";
        for(int i = termList.size() - 1; i >= 0; i--){
            if(termList.get(i).coefficient != 0){
                out += (termList.get(i).coefficient>0?"+":"-") + 
                        (Math.abs(termList.get(i).coefficient)==1 && termList.get(i).exponent!=0?"":termList.get(i).coefficient) +
                        (termList.get(i).exponent==0?"":"x^"+ termList.get(i).exponent);
            }
        }
        if(out.equals("")){
            return "0";
        }
        return out;
    }

    /**
     * @return Returns a Polynomial that is the sum of p and this Polynomial.
     * @param p The Polynomial to be added to the activating Polynomial.
  * *
     */
    public Polynomial add(Polynomial p) {
        Polynomial out = new Polynomial();
        int i = 0;
        int j = 0;
        while(i < p.termList.size() && j < this.termList.size()){
            if(p.termList.get(i).exponent == this.termList.get(j).exponent){
                out.termList.add(new Term(p.termList.get(i).exponent,this.termList.get(j).coefficient + p.termList.get(i).coefficient));
                i++;
                j++;
            } else if (p.termList.get(i).exponent < this.termList.get(j).exponent){
                out.termList.add(p.termList.get(i));
                i++;
            } else if (p.termList.get(i).exponent > this.termList.get(j).exponent){
                out.termList.add(this.termList.get(j));
                j++;
            }
        }
        while(i < p.termList.size()){
            out.termList.add(p.termList.get(i));
            i++;
        }
        while(j < this.termList.size()){
            out.termList.add(this.termList.get(j));
            j++;
        }
        return out;
    }

    /** <b>Postcondition:</b> Returns a new polynomial obtained by multiplying
     * this term and p. For example, if this polynomial is 2x^2 + 3x + 4 and p
     * is 5x^2 - 1x + 7, then at the end of this function, it will return the
     * polynomial 10x^4 + 13x^3 + 31x^2 + 17x + 28.
     *
     * @param p The polynomial to be multiplied.
     * @return The product of the activating Polynomial and p.
     *
     */
    public Polynomial multiply(Polynomial p) {
        Polynomial out = new Polynomial();
        for(Term t : this.termList){
            for(Term t2 : p.termList){
                out.add_to_coef(t.coefficient*t2.coefficient, t.exponent+t2.exponent);
            }
        }
        return out;
    }
}

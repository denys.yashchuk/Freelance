
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GUI extends JFrame {

    static final int PERCENT = 20;

    JLabel priceAdultLabel;
    JLabel numberAdultLabel;
    JLabel priceChildLabel;
    JLabel numberChildLabel;

    JTextField priceAdultTF;
    JTextField numberAdultTF;
    JTextField priceChildTF;
    JTextField numberChildTF;

    public GUI() {
        setMinimumSize(new Dimension(500, 300)); // set size of the window
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // stop programm
                                            //when the window is closed
        setTitle("Theater Revenue"); // sets Title to the window
        addComponentsToPane(getContentPane()); // adds elements to the 
                                            //window 
        setResizable(false); // make window unresizable
        setVisible(true); // shows window
    }

    private void addComponentsToPane(Container pane) {
        pane.setLayout(new GridBagLayout()); // set Layout to the window
        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets(5, 5, 5, 5); // sets insets

        // creates lables
        priceAdultLabel = new JLabel("Price per adult ticket:");
        numberAdultLabel = new JLabel("Number of adult tickets sold:");
        priceChildLabel = new JLabel("Price per child ticket:");
        numberChildLabel = new JLabel("Number of child tickets sold:");

        // creates TextFields
        priceAdultTF = new JTextField();
        numberAdultTF = new JTextField();
        priceChildTF = new JTextField();
        numberChildTF = new JTextField();

        // sets size to the text fields
        priceAdultTF.setPreferredSize(new Dimension(200, 20));
        numberAdultTF.setPreferredSize(new Dimension(200, 20));
        priceChildTF.setPreferredSize(new Dimension(200, 20));
        numberChildTF.setPreferredSize(new Dimension(200, 20));

        // creates button
        JButton confirm = new JButton("Confirm");

        // adds elements to the window
        c.gridx = 0;
        c.gridy = 0;
        pane.add(priceAdultLabel, c);
        c.gridx = 1;
        c.gridy = 0;
        pane.add(priceAdultTF, c);
        c.gridx = 0;
        c.gridy = 1;
        pane.add(numberAdultLabel, c);
        c.gridx = 1;
        c.gridy = 1;
        pane.add(numberAdultTF, c);
        c.gridx = 0;
        c.gridy = 2;
        pane.add(priceChildLabel, c);
        c.gridx = 1;
        c.gridy = 2;
        pane.add(priceChildTF, c);
        c.gridx = 0;
        c.gridy = 3;
        pane.add(numberChildLabel, c);
        c.gridx = 1;
        c.gridy = 3;
        pane.add(numberChildTF, c);
        c.gridwidth = 2;
        c.gridx = 0;
        c.gridy = 4;
        pane.add(confirm, c);

        // actions, when button is pressed
        confirm.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    JFrame results = new JFrame(); // creates new window
                    results.setMinimumSize(new Dimension(500, 300));
                    results.setTitle("Theater Revenue");
                    Container pane = results.getContentPane();
                    pane.setLayout(new GridLayout(6, 1));

                    // compute values and creates labels with results
                    double grossAdult = 
                        Double.parseDouble(priceAdultTF.getText())
                        * Double.parseDouble(numberAdultTF.getText());
                    JLabel grossAdultLabel = new JLabel("Gross revenue "
                            + "for adult sold: " + grossAdult);
                    double netAdult =
                        grossAdult - grossAdult * (20. / 100);
                    JLabel netAdultLabel = new JLabel("Net revenue"
                            + " for adult tickets sold: " + netAdult);
                    double grossChild = 
                        Double.parseDouble(priceChildTF.getText())
                        * Double.parseDouble(numberChildTF.getText());
                    JLabel grossChildLabel = new JLabel("Gross revenue "
                            + "for child sold: " + grossChild);
                    double netChild = 
                        grossChild - grossChild * (20. / 100);
                    JLabel netChildLabel = new JLabel("Net revenue for"
                            + " child tickets sold: " + netChild);
                    double totalGross = grossAdult + grossChild;
                    JLabel totalGrossLabel = new JLabel("Total gross "
                            + "revenue: " + totalGross);
                    double totalNet = netAdult + netChild;
                    JLabel totalNetLabel = new JLabel("Total net revenue: " 
                            + totalNet);

                    // adds lables to the window
                    pane.add(grossAdultLabel);
                    pane.add(netAdultLabel);
                    pane.add(grossChildLabel);
                    pane.add(netChildLabel);
                    pane.add(totalGrossLabel);
                    pane.add(totalNetLabel);

                    results.setResizable(true);
                    results.setVisible(true);
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(new JFrame(), 
                            "Wrong value(s)", "Warning", 
                            JOptionPane.WARNING_MESSAGE);
                }
            }
        });
    }

    public static void main(String[] args) {
        new GUI(); // starts programm
    }
}

package recipe;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.filechooser.FileNameExtensionFilter;

public class RecipeGUI extends JFrame {

    Recipe recipe;
    File picture;

    public RecipeGUI() {
        setMinimumSize(new Dimension(300, 480));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Recipe");
        addComponentsToPane(getContentPane());
        setResizable(false);
        setVisible(true);
    }

    private void addComponentsToPane(Container pane) {
        pane.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets(5, 5, 5, 5);
        
        JLabel info1 = new JLabel();
        info1.setText("Fill all the fields for adding the recipe.");
        JLabel info2 = new JLabel();
        info2.setText("Enter name for showing or deleting recipe.");
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 2;
        pane.add(info1, c);
        c.gridx = 0;
        c.gridy = 1;
        pane.add(info2, c);

        JLabel name = new JLabel("Name:");
        name.setPreferredSize(new Dimension(80, 20));
        c.gridx = 0;
        c.gridy = 2;
        c.gridwidth = 1;
        pane.add(name, c);

        JTextField nameTF = new JTextField();
        nameTF.setPreferredSize(new Dimension(160, 20));
        c.gridx = 1;
        c.gridy = 2;
        pane.add(nameTF, c);

        JLabel type = new JLabel("Type:");
        type.setPreferredSize(new Dimension(80, 20));
        c.gridx = 0;
        c.gridy = 3;
        pane.add(type, c);

        JTextField typeTF = new JTextField();
        typeTF.setPreferredSize(new Dimension(160, 20));
        c.gridx = 1;
        c.gridy = 3;
        pane.add(typeTF, c);

        JLabel ingredients = new JLabel("Ingredients:");
        ingredients.setPreferredSize(new Dimension(80, 20));
        c.gridx = 0;
        c.gridy = 4;
        pane.add(ingredients, c);

        JTextArea ingredientsTA = new JTextArea();
        ingredientsTA.setLineWrap(true);
        JScrollPane ingredientsSP = new JScrollPane(ingredientsTA);
        ingredientsSP.setPreferredSize(new Dimension(160, 60));
        c.gridx = 1;
        c.gridy = 4;
        pane.add(ingredientsSP, c);

        JLabel instructions = new JLabel("Instructions:");
        instructions.setPreferredSize(new Dimension(80, 20));
        c.gridx = 0;
        c.gridy = 5;
        pane.add(instructions, c);

        JTextArea instructionsTA = new JTextArea();
        instructionsTA.setLineWrap(true);
        JScrollPane instructionsSP = new JScrollPane(instructionsTA);
        instructionsSP.setPreferredSize(new Dimension(160, 100));
        c.gridx = 1;
        c.gridy = 5;
        pane.add(instructionsSP, c);

        JButton chooseImage = new JButton("Choose Image");
        chooseImage.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser chooser = new JFileChooser();
                FileNameExtensionFilter filter = new FileNameExtensionFilter(
                        "JPG & GIF Images", "jpg", "gif");
                chooser.setFileFilter(filter);
                int returnVal = chooser.showOpenDialog(new Component() {
                });
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    picture = chooser.getSelectedFile();
                }
            }
        });
        c.gridx = 0;
        c.gridy = 6;
        c.gridwidth = 2;
        pane.add(chooseImage, c);

        JButton addRecipe = new JButton("Add Recipe");
        addRecipe.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String name = nameTF.getText().toLowerCase();
                String type = typeTF.getText();
                String ingredients = ingredientsTA.getText();
                String instructions = instructionsTA.getText();
                System.out.println(picture.getName());
                if (name.isEmpty() || type.isEmpty() || ingredients.isEmpty() || instructions.isEmpty()
                        || picture.equals(null) || (!picture.getName().endsWith(".jpg")
                        && !picture.getName().endsWith(".gif"))) {
                    JOptionPane.showMessageDialog(new JFrame(),
                            "Please, fill all the fields and choose JPEG or GIF picture",
                            "Error", JOptionPane.ERROR_MESSAGE);
                } else {
                    recipe = new Recipe(name, type, ingredients, instructions,
                            picture);
                    if (recipe.saveToFile()) {
                        showRecipe();
                    }
                }
            }
        });
        c.gridx = 0;
        c.gridy = 7;
        c.gridwidth = 1;
        pane.add(addRecipe, c);

        JButton showRecipe = new JButton("Show Recipe");
        showRecipe.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String name = nameTF.getText().toLowerCase();
                    File file = new File("recipes\\" + name + ".txt");
                    if (file.exists()) {
                        recipe = new Recipe(name);
                        showRecipe();
                    } else {
                        JOptionPane.showMessageDialog(new JFrame(),
                                "There is no recipe with such name.",
                                "Error", JOptionPane.ERROR_MESSAGE);
                    }
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(RecipeGUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        c.gridx = 1;
        c.gridy = 7;
        c.gridwidth = 1;
        pane.add(showRecipe, c);

        JButton delRecipe = new JButton("Delete Recipe");
        delRecipe.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String name = nameTF.getText().toLowerCase();
                File file = new File("recipes\\" + name + ".jpg");
                if (file.exists()) {
                    file.setWritable(true);
                    file.delete();
                }
                File file1 = new File("recipes\\" + name + ".gif");
                if (file1.exists()) {
                    file.setWritable(true);
                    file1.delete();
                }
                File file2 = new File("recipes\\" + name + ".txt");
                if (file2.exists()) {
                    file.setWritable(true);
                    file2.delete();
                }
                
            }
        });
        c.gridx = 0;
        c.gridy = 8;
        c.gridwidth = 2;
        pane.add(delRecipe, c);
    }

    public void showRecipe() {

        JFrame frame = new JFrame();
        frame.setMinimumSize(new Dimension(300, 400));
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setTitle("Your Recipe");
        frame.setVisible(true);
        frame.setLayout(new GridLayout(1, 2));

        JTextArea r = new JTextArea();
        r.setLineWrap(true);
        r.setEnabled(false);
        r.setText(recipe.toString());
        JScrollPane rSP = new JScrollPane(r);
        rSP.setPreferredSize(new Dimension(300, 400));

        frame.getContentPane().add(rSP);

        ImageIcon pictureIcon = new ImageIcon(recipe.getPicture().getPath());
        JLabel pictureLabel = new JLabel(pictureIcon);

        frame.getContentPane().add(pictureLabel);
    }

}

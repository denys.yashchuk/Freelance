package recipe;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.Scanner;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Recipe {

    String name;
    String type;
    String ingredients;
    String instructions;
    File picture;

    public Recipe(String name, String type, String ingredients,
            String instructions, File picture) {
        this.name = name;
        this.type = type;
        this.ingredients = ingredients;
        this.instructions = instructions;
        this.picture = picture;
    }

    public Recipe(String name) throws FileNotFoundException {
        File file = new File("recipes\\" + name + ".txt");
        Scanner in = new Scanner(file);
        String[] recipeParts = in.nextLine().split("\u00A3");
        this.type = recipeParts[0];
        this.name = recipeParts[1];
        this.ingredients = recipeParts[2];
        this.instructions = recipeParts[3];
        picture = new File("recipes\\" + name + ".jpg");
        if (!picture.exists()) {
            picture = new File("recipes\\" + name + ".gif");
        }
        in.close();
    }

    public File getPicture() {
        return picture;
    }

    @Override
    public String toString() {
        return type.toUpperCase() + " " + name.toUpperCase() + "\n\nIngredients:\n" + ingredients
                + "\n\nInstructions:\n" + instructions;
    }

    public String stringToFile() {
        return type.toUpperCase() + "\u00A3" + name.toUpperCase() + "\u00A3"
                + ingredients + "\u00A3" + instructions;
    }

    public boolean saveToFile() {

        File file = new File("recipes\\" + name + ".txt");

        try {
            if (file.exists()) {
                JOptionPane.showMessageDialog(new JFrame(), "You already have recipe with such name",
                        "Attention", JOptionPane.INFORMATION_MESSAGE);
                return false;
            } else if (!file.exists()) {
                file.createNewFile();
                PrintWriter out = new PrintWriter(file.getAbsoluteFile());
                try {
                    out.print(this.stringToFile());
                    out.close();
                } finally {
                    out.close();
                }
            }
            File copyPiture = new File("recipes\\" + name + picture.getName().substring(picture.getName().length() - 4));
            Files.copy(picture.toPath(), copyPiture.toPath());
        } catch (Exception e) {
            return false;
        }
        return true;
    }

}

package yamotd;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static yamotd.Code.*;
import yamotd.msg.MsgReader;
import yamotd.msg.MsgWriter;

public class ClientThread extends Thread {

    private InputStream in = null;
    private PrintStream os = null;
    private Socket clientSocket = null;
    private ServerSocket serverSocket;
    private boolean close = false;
    private volatile ArrayList<String> msgHistory;
    private List<User> userList;
    private int userIndex = -1;
    private int sendMsg = 0;
    private ArrayList<ClientThread> threads;
    private String sendToUserId;

    public ClientThread(ServerSocket serverSocket, Socket clientSocket, ArrayList<ClientThread> threads, List<User> userList) {
        this.userList = userList;
        msgHistory = new ArrayList<>(MsgReader.readMsg());
        sendMsg = 0;
        this.serverSocket = serverSocket;
        this.clientSocket = clientSocket;
        this.threads = threads;
        try {
            os = new PrintStream(clientSocket.getOutputStream());
        } catch (IOException ex) {
            Logger.getLogger(ClientThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //	Manages connection from Client program to recieve data.
    public void run() {
        while (!clientSocket.isClosed()) {
            try {
                in = clientSocket.getInputStream();

                if (in.available() > 0) {
                    getData(in);
                }
            } catch (IOException ex) {
                System.err.println("Lost I/O stream!");
                close();
            }
            if (clientSocket.isClosed()) {
                break;
            }
            waitT();
        }
    }

    //	Closes all server connections for server shutdown.
    public void exit() {
        synchronized (this) {
            for (ClientThread ct : threads) {
                ct.send(ERROR_210);
            }
            this.send(ERROR_210);
        }
        try {
            serverSocket.close();
        } catch (IOException ex) {
            print(ERROR_300);
        }
        close = true;

    }

//	Identifies the number of messages inside of history.msg (message of the day file).
    public int getSizeMsg() {
        return msgHistory.size();
    }
//	Reads the messages of the day into memory.

    public String getMsg(int index) {
        if (index < 0 || index >= msgHistory.size()) {
            return null;
        }
        return msgHistory.get(index);
    }
//	Writes Message of the day.

    public void add(String msg) {
        msgHistory.add(msg);
        MsgWriter.saveMsg(msg);
    }

    //	Check user login and return success or failure.
    public boolean login(String userID, String password) {
        int i = 0;
        for (User user : userList) {
            if (user.getPassword().equals(password)
                    && user.getUserID().equals(userID)) {
                userIndex = i;
                return true;
            }
            i++;
        }
        userIndex = -1;
        return false;
    }

//	Closes Client connections to server.
    public void close() {
        try {
            clientSocket.close();
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

//	Identify type of command recieved from the client & corresponding message to send.
//	Conducts error checking along with executing aditional functions in line with Client message.
    public void getData(InputStream in) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        try {
            int code = reader.read();
            switch (code) {
                case MSGGET:
                    print(MSGGET);
                    if (sendMsg < getSizeMsg()) {
                        send(MSG, getMsg(sendMsg));
                        sendMsg++;
                    } else {
                        sendMsg = 0;
                        send(MSG, getMsg(sendMsg));
                        sendMsg++;
                    }
                    break;
                case MSGSTORE:
                    print(MSGSTORE);
                    if (userIndex != -1) {
                        send(OK);
                    } else {
                        send(ERROR_401);
                    }
                    break;
                case SHUTDOWN:
                    print(SHUTDOWN);
                    if (userIndex == 0) {
                        send(OK);
                        exit();
                    } else {
                        send(ERROR_402);
                    }
                    System.exit(0);
                    break;
                case LOGIN:
                    print(LOGIN);
                    String user = reader.readLine();
                    String[] username = user.trim().split("\\s+");
                    if (username.length <= 1) {
                        send(ERROR_410);
                    } else {
                        String userId = username[0];
                        String userPassword = username[1];
                        if (login(userId, userPassword)) {
                            send(OK);
                        } else {
                            send(ERROR_410);
                        }
                    }
                    break;
                case LOGOUT:
                    print(LOGOUT);
                    send(OK);
                    userIndex = -1;
                    break;
                case QUIT:
                    print(QUIT);
                    send(OK);
                    close();
                    break;
                case WHO:
                    print(WHO);
                    send(MESSAGE, getUsers());
                    break;
                case SEND:
                    print(SEND);
                    sendToUserId = reader.readLine().trim();
                    send(OK);
                    break;
                case MESSAGE:
                    print(MESSAGE);
                    String msg = reader.readLine();
                    sendMsg(msg);
                    send(OK);
                    break;
                case TEXT:
                    String message = reader.readLine();
                    add(message);
                    send(OK);
                    break;

            }
        } catch (IOException e) {
            System.err.println("Lost I/O stream!");
            close();
        }
    }

    public void sendMsg(String msg) {

        synchronized (this) {
            for (ClientThread ct : threads) {
                if (ct.userIndex == userList.indexOf(new User(sendToUserId, ""))) {
                    ct.send(MESSAGE, userList.get(userIndex).getUserID() + ": " + msg + "\n");
                    break;
                }
            }
        }

    }

    public void send(int code) {
        send(code, null);
    }

//	Sends the message to the Client. 
    public synchronized void send(int code, String msg) {
        OutputStream out = null;
        try {
            out = clientSocket.getOutputStream();
        } catch (IOException e) {
            System.err.println("Error sending message, message not sent to server!");
            close();
        }
        DataOutputStream writer = new DataOutputStream(out);
        try {
            switch (code) {
                case MSG:
                    writer.write(code);
                    if (msg != null) {
                        writer.writeBytes(msg + "\n");
                    }
                    break;
                case MESSAGE:
                    writer.write(code);
                    if (msg != null) {
                        writer.writeBytes(msg + "\n");
                    }
                    break;
                default:
                    writer.write(code);
            }
        } catch (IOException e) {
            System.err.println("Lost I/O stream!");
            close();
        }
    }

    //	Timeout on thread.
    private void waitT() {
        try {
            Thread.sleep(500);
        } catch (InterruptedException ex) {/*NOP*/
        }
    }

    // Gets users name and ip
    private String getUsers() {
        String out = "";
        synchronized (this) {
            System.out.println(threads.size());
            for (ClientThread ct : threads) {
                out += ct.getUser() + "\n";
            }
        }
        return out;
    }

    // Gets user name and ip
    public String getUser() {
        if (userIndex != -1) {
            return userList.get(userIndex).getUserID() + "\t" + clientSocket.getInetAddress().toString() + "\n";
        } else {
            return "";
        }
    }

    public void setThreads(ArrayList<ClientThread> threads) {
        this.threads = threads;
    }

}

/*
 * Client.java
 */
package yamotd;

import java.io.*;
import java.net.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import static yamotd.Code.*;

public class Client {

    private Socket socket;
    private String HOST;
    private int PORT;
    private volatile boolean exit = false;
    private volatile boolean connect = false;
    private Scanner in;
    private Thread receivingThread;

//	Check for the correct Command Line Parameters to start Client. 
// 	Print out error message that displays the correct program usage/parameters.
//	If parameters are correct launches routine to connect to server.
    public static void main(String a[]) {
        /* if (a.length < 1) {
            System.out.println("Please use the following syntax to start the program: yamotd.Client <Server IP Address>");
            System.exit(1);
        } else {
            new Client(a[0], SERVER_PORT).run();
        }*/
        new Client("localhost", SERVER_PORT).run();
    }
//	Paramaters used to establish connection to server.

    public Client(String nameServer, int port) {
        HOST = nameServer;
        PORT = port;
        connectToServer();
    }

// 	After the Client has connected to the server read input from user.
    public void run() {
        if (socket != null && socket.isConnected()) {
            receivingThread = new Thread() {
                @Override
                public void run() {
                    while (true) {
                        getData();
                    }
                }
            };
            receivingThread.start();
            in = new Scanner(System.in);
            while (!isExit()) {
                send(in.nextLine());
            }
        }
    }
//	Closes the socket ending the connection to server.

    public void close() {
        exit = true;
        try {
            socket.close();
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.exit(0);
    }

    public boolean isExit() {
        return exit;
    }

    //	Establish a connection to the server.
    public void connectToServer() {
        if (socket != null) {
            return;
        }
        try {
            socket = new Socket(HOST, PORT);
            connect = true;
            System.err.println("You have successfully connected to the server!");
            return;
        } catch (UnknownHostException e) {
            System.err.println("Unknown host " + HOST);
            close();
        } catch (IOException e) {
            System.err.println("Error socket was terminated");
        }
    }

//	Identify type of command to be sent to server based off of user input.
    public void send(String msg) {
        msg = msg.trim();
        if (get(MSGGET).equalsIgnoreCase(msg)) {
            send(MSGGET);
        } else if (get(QUIT).equalsIgnoreCase(msg)) {
            send(QUIT);
        } else if (get(LOGOUT).equalsIgnoreCase(msg)) {
            send(LOGOUT);
        } else if (get(SHUTDOWN).equalsIgnoreCase(msg)) {
            send(SHUTDOWN);
        } else if (msg.length() > 4 && get(LOGIN).equalsIgnoreCase(msg.substring(0, 5))) {
            send(LOGIN, msg);
        } else if (get(MSGSTORE).equalsIgnoreCase(msg)) {
            send(MSGSTORE);
            String message = in.nextLine();
            send(TEXT, message);

        } else if (get(WHO).equalsIgnoreCase(msg)) {
            send(WHO);
        } else if (get(SEND).equalsIgnoreCase(msg.substring(0, 4))) {
            send(SEND, msg);
            String message = in.nextLine();
            send(MESSAGE, message);
        }
    }

    public synchronized void send(int code) {
        send(code, null);
    }

//	Sends the command to the server.
    public synchronized void send(int code, String msg) {
        OutputStream out = null;
        try {
            out = socket.getOutputStream();
        } catch (IOException e) {
            System.err.println("Error sending message, message not sent to server!");
            close();
            return;
        }
        DataOutputStream writer = new DataOutputStream(out);
        try {
            switch (code) {
                case MSGGET:
                    writer.write(code);
                    break;
                case MSGSTORE:
                    writer.write(code);
                    break;
                case SHUTDOWN:
                    writer.write(code);
                    System.exit(0);
                    break;
                case LOGIN:
                    writer.write(code);
                    writer.writeBytes(msg.substring(msg.indexOf(" ") + 1) + "\n");
                    break;
                case LOGOUT:
                    writer.write(code);
                    break;
                case QUIT:
                    writer.write(code);
                    close();
                    break;
                case WHO:
                    writer.write(code);
                    break;
                case SEND:
                    writer.write(code);
                    writer.writeBytes(msg.substring(msg.indexOf(" ") + 1) + "\n");
                    break;
                case MESSAGE:
                    writer.write(MESSAGE);
                    writer.writeBytes(msg + "\n");
                    break;
                case TEXT:
                    writer.write(TEXT);
                    writer.writeBytes(msg + "\n");
                    break;
            }
        } catch (IOException e) {
            System.err.println("Lost I/O stream");
            close();
        }
    }

//	Prints messages recieved from the server.
    public boolean getData() {
        InputStream in = null;
        try {
            in = socket.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            try {
                int code = reader.read();
                switch (code) {
                    case OK:
                        print(OK);
                        return true;
                    case ERROR_401:
                        print(ERROR_401);
                        return false;
                    case ERROR_402:
                        print(ERROR_402);
                        return false;
                    case ERROR_410:
                        print(ERROR_410);
                        return false;
                    case ERROR_300:
                        print(ERROR_300);
                        return false;
                    case MSG:
                        print(MSG);
                        System.out.println(reader.readLine());
                        return true;
                    case MESSAGE:
                        while (reader.ready()) {
                            System.out.println(reader.readLine());
                        }
                        return true;
                    case ERROR_210:
                        print(ERROR_210);
                        close();
                        return false;
                }
            } catch (IOException e) {
                System.err.println("Lost I/O stream");
                close();
            }
        } catch (IOException ex) {
            System.err.println("Lost I/O stream");
            close();
        }
        return false;
    }
}

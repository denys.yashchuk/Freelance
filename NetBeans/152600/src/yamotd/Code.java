/*
 * Code.java
 */
package yamotd;

public class Code {

    public static final int MSGGET = 0;
    public static final int MSGSTORE = 1;
    public static final int SHUTDOWN = 2;
    public static final int LOGIN = 3;
    public static final int LOGOUT = 4;
    public static final int QUIT = 5;
    public static final int OK = 6;
    public static final int MSG = 7;
    public static final int ERROR_401 = 8;
    public static final int ERROR_402 = 9;
    public static final int ERROR_410 = 10;
    public static final int ERROR_300 = 11;
    public static final int WHO = 12;
    public static final int SEND = 13;
    public static final int MESSAGE = 14;
    public static final int ERROR_210 = 15;
    public static final int TEXT = 16;

    private static final String[] code = {"MSGGET", "MSGSTORE", "SHUTDOWN", "LOGIN",
        "LOGOUT", "QUIT", "200 OK", "200 OK", "401 You are not currently logged in, login first.",
        "402 User not allowed to execute this command.", "410 Wrong UserID or Password",
        "300 message format error", "WHO", "SEND", "MESSAGE", "210 the server is about to shutdown...", "TEXT"};

    public static final int SERVER_PORT = 7514;

//	Gets the stored message based off code value.
    public static String get(int code) {
        return Code.code[code];
    }

//	Prints the stored message based off of code value.
    public static void print(int code) {
        System.out.println(get(code));
    }
}

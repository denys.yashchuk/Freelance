/*
 * Server.java
 */
package yamotd;

import yamotd.msg.MsgReader;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

public class Server implements Runnable {

    private ServerSocket serverSocket;
    private final int SERVER_PORT;
    private Socket clientSocket;
    private List<User> userList;
    public static final int maxClientsCount = 10;
    public static ArrayList<ClientThread> threads = new ArrayList<ClientThread>();

//	Starts new thread for server
    public static void main(String[] args) {
        Server server = new Server(Code.SERVER_PORT);
        new Thread(server).start();
    }
// 	Initiliazes the server information (Server Port, MOD Msg reader, & User creds)

    public Server(int port) {
        SERVER_PORT = port;
        userList = new ArrayList<>();
        userList.add(new User("root", "root01"));
        userList.add(new User("john", "john01"));
        userList.add(new User("david", "david01"));
        userList.add(new User("mary", "mary01"));
    }

    @Override
//	initializes & handles server socket.
    public void run() {
        create();
        System.err.println("The server is running");
        /*
     * Create a client socket for each connection and pass it to a new client
     * thread.
         */
        while (true) {
            try {
                clientSocket = serverSocket.accept();
                ClientThread ct = new ClientThread(serverSocket, clientSocket, threads, userList);
                threads.add(ct);
                ct.start();
                for(ClientThread c : threads){
                    c.setThreads(threads);
                }
                if (threads.size() == maxClientsCount) {
                    PrintStream os = new PrintStream(clientSocket.getOutputStream());
                    os.println("Server too busy. Try later.");
                    os.close();
                    clientSocket.close();
                }
            } catch (IOException e) {
                //System.out.println(e);
            }
        }
    }

//	Creates the server socket and establishes a socket timeout & error condition for in use port.
    public void create() {
        if (serverSocket == null) {
            try {
                serverSocket = new ServerSocket(SERVER_PORT);
            } catch (IOException e) {
                System.err.println("Port is already in use:" + SERVER_PORT);
            }
        }
        if (serverSocket != null) {
            try {
                serverSocket.setSoTimeout(1000);
            } catch (SocketException ex) {/*NOP*/
            }
        }
    }

//	Timeout on thread.
    private void waitT() {
        try {
            Thread.sleep(500);
        } catch (InterruptedException ex) {/*NOP*/
        }
    }
}

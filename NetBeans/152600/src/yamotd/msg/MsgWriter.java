/*
 *	MsgWriter.java
 */
package yamotd.msg;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class MsgWriter {

//	Used to write message of the day to history.msg.
    public static void saveMsg(String msg) {
        try {
            File file = new File("history.msg");
            FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(msg + "\n");
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

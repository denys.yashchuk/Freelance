/*
 *	MsgReader.java
 */
package yamotd.msg;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MsgReader {

//	Used to read message of the day from the history.msg file.
//  Returns a list of messages of the day into the history arraylist.
    public static List<String> readMsg() {
        ArrayList<String> history = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader("history.msg"))) {
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                history.add(sCurrentLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return history;
    }
}

/*
 * User.java
 */
package yamotd;

import java.util.Objects;

public class User {

    private String password;
    private String userID;

//	Used to get and store usernames and passwords from Servers userList for the Severs login function.
    public User(String userID, String password) {
        this.password = password;
        this.userID = userID;
    }
//	Used by the Servers login function to compare Server saved password value against user input.

    public String getPassword() {
        return password;
    }
//	Used by the Servers login function to compare Server saved User name value against user input.

    public String getUserID() {
        return userID;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (!Objects.equals(this.userID, other.userID)) {
            return false;
        }
        return true;
    }

}


public class SimpleQueue<T> {

    private T[] values;
    private final int INITIAL_CAPACITY = 10;
    private int size, first, next;

    public SimpleQueue() {
        values = (T[]) new Object[INITIAL_CAPACITY];
    }

    private void resize(int capacity) {
        T[] tmp = (T[]) new Object[capacity];

        for (int i = 0; i < size; i++) {
            tmp[i] = values[(first + i) % values.length];
        }

        values = tmp;
        first = 0;
        next = size;
    }

    public SimpleQueue<T> enqueue(T ele) {
        if (values.length == size) {
            resize(values.length * 2);
        }
        values[next++] = ele;
        if (next == values.length) {
            next = 0;
        }
        size++;
        return this;
    }

    public T dequeue() {
        if (size == 0) {
            throw new java.util.NoSuchElementException();
        }
        T ele = values[first];
        values[first] = null;
        if (++first == values.length) {
            first = 0;
        }
        size--;
        return ele;
    }

    public T front() {
        if (size == 0) {
            throw new java.util.NoSuchElementException();
        }
        return values[first];
    }

    public int getSize() {
        return size;
    }

    public int getCapacity() {
        return values.length;
    }

    public static void main(String[] args) {
        SimpleQueue<String> myqueue = new SimpleQueue<String>();
        System.out.println("Capacity of the queue is: " + myqueue.getCapacity());
        for (int i = 0; i < 20; i++) {
            System.out.println("Add \"" + i + "\" to the queue");
            myqueue.enqueue(i + "");
        }
        System.out.println("Now capacity of thr queue is: " + myqueue.getCapacity());
        int numOfElementsinQueue = myqueue.getSize();
        for (int i = 0; i < numOfElementsinQueue; i++) {
            System.out.println("Dequeue: " + myqueue.dequeue());
        }
        System.out.println("Number of elements in queue: " + myqueue.getSize());
    }

}

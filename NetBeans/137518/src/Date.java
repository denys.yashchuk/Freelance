
import javax.swing.JFrame;
import javax.swing.JOptionPane;


public class Date {

    int month;
    int day;
    int year;

    public Date(int month, int day, int year) {
        if (month > 0 && month <= 12 && day > 0 && day <= 31) {
            this.month = month;
            this.day = day;
            this.year = year;
        } else {
            this.month = 1;
            this.day = 1;
            this.year = 2016;
            JOptionPane.showMessageDialog(new JFrame(),
                            "Bad value of the date! Sets 01\\01\\2016",
                            "",
                            JOptionPane.INFORMATION_MESSAGE);
        }
    }

    public Date(String s) {
        String[] parts = s.split("\\");
        this.month = Integer.parseInt(parts[0]);
        this.day = Integer.parseInt(parts[1]);
        this.year = Integer.parseInt(parts[2]);
    }
    
    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }

    public int getYear() {
        return year;
    }
    
    public boolean isEqual(Date otherDate){
        return this.getMonth() == otherDate.getMonth()
                && this.getDay() == otherDate.getDay()
                && this.getDay() == otherDate.getDay();
    }

    @Override
    public String toString() {
        return month + "\\" + day + "\\" + year;
    }
}

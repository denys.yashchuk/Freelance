package Member;

import Borrowable.*;
import java.util.ArrayList;

public class Member {

    int id;
    String name;
    ArrayList<Book> books;
    ArrayList<Periodical> periodicals;

    private final int STUDENT_MAX_BOOKS = 2;
    private final int STUDENT_MAX_PERIODICALS = 1;
    private final int FACULTY_MAX_BOOKS = 7;
    private final int FACULTY_MAX_PERIODICALS = 3;
    private final int EMPLOYEE_MAX_BOOKS = 3;
    private final int EMPLOYEE_MAX_PERIODICALS = 0;

    public String getName() {
        return name;
    }

    public Member(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public static Member newMember(String s) {
        String type = s.split("#")[0];
        if (type.equals("student")) {
            return new Student(s.split("#")[1]);
        } else if (type.equals("faculty")) {
            return new Faculty(s.split("#")[1]);
        } else if (type.equals("employee")) {
            return new Employee(s.split("#")[1]);
        } else {
            return null;
        }
    }

    public void addBook(Book book) {
        books.add(book);
    }

    public void addPeriodical(Periodical periodical) {
        periodicals.add(periodical);
    }

    public void removeBook(Book book) {
        books.remove(book);
    }

    public void removePeriodical(Periodical periodical) {
        periodicals.remove(periodical);
    }

    public int getId() {
        return id;
    }

    public ArrayList<Book> getBooks() {
        return books;
    }

    public ArrayList<Periodical> getPeriodicals() {
        return periodicals;
    }

    public void setBooks(ArrayList<Book> books) {
        this.books = books;
    }

    public void setPeriodicals(ArrayList<Periodical> periodicals) {
        this.periodicals = periodicals;
    }

    public int getNumberOfBooks() {
        return books.size();
    }

    public int getNumberOfPeriodical() {
        return periodicals.size();
    }

    public boolean isAbleToBorrowBook() {
        if (this instanceof Student) {
            return this.getNumberOfBooks() < STUDENT_MAX_BOOKS;
        } else if (this instanceof Faculty) {
            return this.getNumberOfBooks() < FACULTY_MAX_BOOKS;
        } else if (this instanceof Employee) {
            return this.getNumberOfBooks() < EMPLOYEE_MAX_BOOKS;
        } else {
            return false;
        }
    }

    public boolean isAbleToBorrowPeriodical() {
        if (this instanceof Student) {
            return this.getNumberOfPeriodical() < STUDENT_MAX_PERIODICALS;
        } else if (this instanceof Faculty) {
            return this.getNumberOfPeriodical() < FACULTY_MAX_PERIODICALS;
        } else if (this instanceof Employee) {
            return this.getNumberOfPeriodical() < EMPLOYEE_MAX_PERIODICALS;
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        if (this instanceof Student) {
            Student s = (Student) this;
            return s.toString();
        } else if (this instanceof Faculty) {
            Faculty f = (Faculty) this;
            return f.toString();
        } else {
            Employee e = (Employee) this;
            return e.toString();
        }
    }
}

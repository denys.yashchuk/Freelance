package Member;

import Borrowable.Book;
import Borrowable.Periodical;
import java.util.ArrayList;

public class Student extends Member {

    String major;

    public Student(int id, String name, String major) {
        super(id, name);
        this.major = major;
        super.books = new ArrayList<Book>();
        super.periodicals = new ArrayList<Periodical>();
    }

    public Student(String s) {
        super(Integer.parseInt(s.split("#")[2]), s.split("#")[3]);
        String[] parts = s.split("#");
        String[] books = parts[0].split("@");
        String[] periodicals = parts[1].split("@");
        ArrayList<Book> b = new ArrayList<Book>();
        for (String book : books) {
            b.add(new Book(book));
        }
        ArrayList<Periodical> p = new ArrayList<Periodical>();
        for (String periodical : periodicals) {
            p.add(new Periodical(periodical));
        }
        setBooks(b);
        setPeriodicals(p);
        this.major = parts[4];
    }

    @Override
    public String toString() {
        String out = "student#";
        if (!books.isEmpty()) {
            for (int i = 0; i < books.size(); i++) {
                out += books.get(i).toString();
                if (i != books.size() - 1) {
                    out += "@";
                }
            }
        } else {
            out += "null";
        }
        if (!periodicals.isEmpty()) {
            out += "#";
            for (int i = 0; i < periodicals.size(); i++) {
                out += periodicals.get(i).toString();
                if (i != periodicals.size() - 1) {
                    out += "@";
                }
            }
        } else {
            out += "null";
        }
        out += "#" + id + "#" + name + "#" + major;
        return out;
    }

}

package Borrowable;

import Member.Member;

public class Periodical extends Borrowable {

    String issn;
    String title;
    int issue;
    int year;

    public Periodical(String callNumber, String issn, String title, int issue, int year) {
        super(callNumber);
        this.issn = issn;
        this.title = title;
        this.issue = issue;
        this.year = year;
    }

    public Periodical(String s) {
        super(s.split(":")[4], Boolean.parseBoolean(s.split(":")[5]), Member.newMember(s.split(":")[6]));
        String[] parts = s.split(":");
        this.issn = parts[0];
        this.title = parts[1];
        this.issue = Integer.parseInt(parts[2]);
        this.year = Integer.parseInt(parts[3]);
    }

    public String getTitle() {
        return title;
    }

    public String getIssn() {
        return issn;
    }

    @Override
    public String toString() {
        return "periodical%" + issn + ":" + title + ":" + issue + ":" + year
                + ":" + callNumber + ":" + status + ":" + ((owner != null) ? owner.toString() : "null");
    }

}

package Borrowable;

import Member.Member;
import java.util.ArrayList;

public class Book extends Borrowable {

    String isbn;
    String title;
    ArrayList<String> author;
    int edition;

    public Book(String callNumber, String isbn, String title, ArrayList<String> author, int edition) {
        super(callNumber);
        this.isbn = isbn;
        this.title = title;
        this.author = author;
        this.edition = edition;
    }

    public Book(String s) {
        super(s.split(":")[4], Boolean.parseBoolean(s.split(":")[5]), Member.newMember(s.split(":")[6]));
        String[] parts = s.split(":");
        String[] authors = parts[0].split("$");
        this.author = new ArrayList<String>();
        for (String auth : authors) {
            author.add(auth);
        }
        this.isbn = parts[1];
        this.title = parts[2];
        this.edition = Integer.parseInt(parts[3]);
    }

    public String getIsbn() {
        return isbn;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        String out = "book%";
        for (String s : author) {
            out += s + "$";
        }
        out += ":" + isbn + ":" + title + ":" + edition;
        out += ":" + callNumber + ":" + status + ":" + ((owner != null) ? owner.toString() : "null");
        return out;
    }

}

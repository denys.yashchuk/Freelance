package Borrowable;

import Member.*;

public class Borrowable {

    String callNumber;
    boolean status;
    Member owner;

    public Borrowable(String callNumber) {
        this.callNumber = callNumber;
        status = true;
    }

    public Borrowable(String callNumber, boolean status, Member owner) {
        this.callNumber = callNumber;
        this.status = status;
        this.owner = owner;
    }

    public static Borrowable newBorrowable(String s) {
        if (!s.equals("")) {
            String[] parts = s.split("%");
            if (parts[0].equals("book")) {
                return new Book(parts[1]);
            } else {
                return new Periodical(parts[1]);
            }
        } else {
            return null;
        }
    }

    public Member getOwner() {
        return owner;
    }

    public void setOwner(Member owner) {
        this.owner = owner;
        status = false;
    }

    public void removeOwner() {
        this.owner = null;
        status = true;
    }

    public boolean getStatus() {
        return status;
    }

    public String getBorrowableTitle() {
        if (this instanceof Book) {
            Book book = (Book) this;
            return book.getTitle();
        } else {
            Periodical p = (Periodical) this;
            return p.getTitle();
        }
    }

    @Override
    public String toString() {
        String out = "";
        if (this instanceof Book) {
            Book b = (Book) this;
            out += b.toString();
        } else if (this instanceof Periodical) {
            Periodical p = (Periodical) this;
            out += p.toString();
        }
        return out;
    }

}

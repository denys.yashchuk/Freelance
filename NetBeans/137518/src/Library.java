
import java.util.ArrayList;
import Borrowable.*;
import Member.*;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;
import javax.swing.*;

public class Library extends JFrame {

    static ArrayList<Member> members;
    static ArrayList<Borrowable> borrowable;
    static ArrayList<Operation> operations;
    JFrame frame = new JFrame();
    private JTextField month;
    private JTextField day;
    private JTextField year;

    public Library() {
        members = new ArrayList<Member>();
        borrowable = new ArrayList<Borrowable>();
        operations = new ArrayList<Operation>();
        setSize(new Dimension(650, 350));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Library");
        addComponentsToPane(getContentPane());
        setResizable(false);
        setVisible(true);
    }

    private void addComponentsToPane(Container pane) {
        pane.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets(5, 5, 5, 5);

        JButton addMember = new JButton("Add Member");
        addMember.addActionListener(addMember());
        JButton addItem = new JButton("Add Item");
        addItem.addActionListener(addItem());
        JButton borrowItem = new JButton("Borrow an item");
        borrowItem.addActionListener(borowItem());
        JButton returnItem = new JButton("Return an item");
        returnItem.addActionListener(returnItem());
        JButton search = new JButton("Search for item by title");
        search.addActionListener(searchItemByTitle());
        JButton showBorrowedItems = new JButton("Items are currently borrowed by a member");
        showBorrowedItems.addActionListener(currentlyBorrowed());
        JButton showBorrowedByDate = new JButton("Show borrowed by a specific date");
        showBorrowedByDate.addActionListener(showByDate());
        JButton save = new JButton("Save data to the disk");
        save.addActionListener(save());
        JLabel date = new JLabel("Set current date:");
        month = new JTextField("MM");
        day = new JTextField("DD");
        year = new JTextField("YYYY");

        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 2;
        pane.add(addMember, c);

        c.gridx = 0;
        c.gridy = 1;
        c.gridwidth = 2;
        pane.add(addItem, c);

        c.gridx = 2;
        c.gridy = 0;
        c.gridwidth = 2;
        pane.add(borrowItem, c);

        c.gridx = 2;
        c.gridy = 1;
        c.gridwidth = 2;
        pane.add(returnItem, c);

        c.gridx = 0;
        c.gridy = 2;
        c.gridwidth = 4;
        pane.add(search, c);

        c.gridx = 0;
        c.gridy = 3;
        c.gridwidth = 4;
        pane.add(showBorrowedItems, c);

        c.gridx = 0;
        c.gridy = 4;
        c.gridwidth = 4;
        pane.add(showBorrowedByDate, c);

        c.gridx = 0;
        c.gridy = 5;
        c.gridwidth = 4;
        pane.add(save, c);

        c.gridx = 0;
        c.gridy = 6;
        c.gridwidth = 1;
        pane.add(date, c);

        c.gridx = 1;
        c.gridy = 6;
        c.gridwidth = 1;
        pane.add(month, c);

        c.gridx = 2;
        c.gridy = 6;
        c.gridwidth = 1;
        pane.add(day, c);

        c.gridx = 3;
        c.gridy = 6;
        c.gridwidth = 1;
        pane.add(year, c);
    }

    public static void main(String[] args) throws FileNotFoundException {
        Library l = new Library();
        restore();
    }

    private ActionListener addMember() {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object[] options = {"Student",
                    "Employee",
                    "Faculty"};
                String id;
                do {
                    id = JOptionPane.showInputDialog(frame, "Enter member's id:");
                } while (!checkId(id));
                String name = JOptionPane.showInputDialog(frame, "Enter member's name:");
                int choice = JOptionPane.showOptionDialog(frame,
                        "The member is: ", "Add Member",
                        JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,
                        null, options, options[2]);
                switch (choice) {
                    case 0:
                        String major = JOptionPane.showInputDialog(frame, "Enter student's major:");
                        members.add(new Student(Integer.parseInt(id), name, major));
                        break;
                    case 1:
                        String officePhone;
                        do {
                            officePhone = JOptionPane.showInputDialog(frame, "Enter employee's office phone (XXX-XX-XX):");
                        } while (!checkOfficePhone(officePhone));
                        members.add(new Employee(Integer.parseInt(id), name, officePhone));
                        break;
                    case 2:
                        do {
                            officePhone = JOptionPane.showInputDialog(frame, "Enter faculty's office phone (XXX-XX-XX):");
                        } while (!checkOfficePhone(officePhone));
                        String dept = JOptionPane.showInputDialog(frame, "Enter faculty's department:");
                        members.add(new Faculty(Integer.parseInt(id), name, officePhone, dept));
                        break;
                }
            }
        };
    }

    private boolean checkId(String id) {
        boolean checkId = id.matches("\\d+");
        if (!members.isEmpty()) {
            for (Member m : members) {
                if (m.getId() == Integer.parseInt(id)) {
                    checkId = false;
                }
            }
        }
        if (!checkId) {
            JOptionPane.showMessageDialog(frame,
                    "Please, enter only numbers. The id must be unique!",
                    "",
                    JOptionPane.ERROR_MESSAGE);
        }
        return checkId;
    }

    private boolean checkOfficePhone(String officePhone) {
        boolean checkOfficePhone = officePhone.matches("\\d\\d\\d-\\d\\d-\\d\\d");
        if (!checkOfficePhone) {
            JOptionPane.showMessageDialog(frame,
                    "Please, enter office phone in format \"XXX-XX-XX\"!",
                    "",
                    JOptionPane.ERROR_MESSAGE);
        }
        return checkOfficePhone;
    }

    private ActionListener addItem() {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object[] options = {"Book",
                    "Periodical"};
                int choice = JOptionPane.showOptionDialog(frame,
                        "New item is: ", "Add Item",
                        JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,
                        null, options, options[1]);
                switch (choice) {
                    case 0:
                        String callNumber = JOptionPane.showInputDialog(frame,
                                "Enter call number of the book:");
                        String isbn = JOptionPane.showInputDialog(frame,
                                "Enter ISBN of the book:");
                        String title = JOptionPane.showInputDialog(frame,
                                "Enter title of the book:");
                        ArrayList<String> authors = new ArrayList<String>();
                        String author = JOptionPane.showInputDialog(frame,
                                "Enter author of the book (enter quit for stopping):");
                        while (!author.toLowerCase().equals("quit")) {
                            authors.add(author);
                            author = JOptionPane.showInputDialog(frame,
                                    "Enter author of the book (enter quit for stopping):");
                        }
                        String edition;
                        do {
                            edition = JOptionPane.showInputDialog(frame,
                                    "Enter edition of the book:");
                            if (!edition.matches("\\d+")) {
                                JOptionPane.showMessageDialog(frame,
                                        "Please, enter right value of edition!",
                                        "",
                                        JOptionPane.ERROR_MESSAGE);
                            }
                        } while (!edition.matches("\\d+"));
                        borrowable.add(new Book(callNumber, isbn, title, authors, Integer.parseInt(edition)));
                        break;
                    case 1:
                        callNumber = JOptionPane.showInputDialog(frame,
                                "Enter call number of the periodical:");
                        String issn = JOptionPane.showInputDialog(frame,
                                "Enter ISSN of the periodical:");
                        title = JOptionPane.showInputDialog(frame,
                                "Enter title of the periodical:");
                        String issue;
                        do {
                            issue = JOptionPane.showInputDialog(frame,
                                    "Enter issue of the periodical:");
                            if (!issue.matches("\\d+")) {
                                JOptionPane.showMessageDialog(frame,
                                        "Please, enter right value of issue!",
                                        "",
                                        JOptionPane.ERROR_MESSAGE);
                            }
                        } while (!issue.matches("\\d+"));
                        String year;
                        do {
                            year = JOptionPane.showInputDialog(frame,
                                    "Enter issue of the periodical:");
                            if (!year.matches("\\d+")) {
                                JOptionPane.showMessageDialog(frame,
                                        "Please, enter right value of year!",
                                        "",
                                        JOptionPane.ERROR_MESSAGE);
                            }
                        } while (!year.matches("\\d+"));
                        borrowable.add(new Periodical(callNumber, issn, title,
                                Integer.parseInt(issue), Integer.parseInt(year)));
                        break;
                }
            }
        };

    }

    private ActionListener borowItem() {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                String id;
                do {
                    id = JOptionPane.showInputDialog(frame, "Enter member id:");
                    if (!id.matches("\\d+")) {
                        JOptionPane.showMessageDialog(frame,
                                "Please, enter only numbers!",
                                "",
                                JOptionPane.ERROR_MESSAGE);
                    }
                } while (!id.matches("\\d+"));
                int memberIndex = findMemberById(Integer.parseInt(id));
                if (memberIndex == -1) {
                    JOptionPane.showMessageDialog(frame,
                            "There is not member with such id!",
                            "",
                            JOptionPane.ERROR_MESSAGE);
                } else {
                    Member member = members.get(memberIndex);
                    Object[] options = {"Book",
                        "Periodical"};
                    int choice = JOptionPane.showOptionDialog(frame,
                            "What does " + member.getName() + " want to borrow?", "Add Member",
                            JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,
                            null, options, options[1]);
                    switch (choice) {
                        case 0:
                            String isbn = JOptionPane.showInputDialog(frame,
                                    "Enter ISBN of the book:");
                            int bookIndex = findBookByISBN(isbn);
                            if (bookIndex == -1) {
                                JOptionPane.showMessageDialog(frame,
                                        "There is not book with such isbn or this book is borrowed!",
                                        "",
                                        JOptionPane.ERROR_MESSAGE);
                            } else {
                                Book book = (Book) borrowable.get(bookIndex);
                                int confirm = JOptionPane.showConfirmDialog(frame, "Confirm?", "", 2);
                                if (confirm == 0) {
                                    if (member.isAbleToBorrowBook()) {
                                        book.setOwner(member);
                                        member.addBook(book);
                                        operations.add(new Operation(member, book, getCurrentDate(), "Borrow"));
                                    } else {
                                        JOptionPane.showMessageDialog(frame,
                                                "You can't borrow any more books!",
                                                "",
                                                JOptionPane.ERROR_MESSAGE);
                                    }
                                    break;
                                }
                            }
                            break;
                        case 1:
                            String issn = JOptionPane.showInputDialog(frame,
                                    "Enter ISSN of the periodical:");
                            int periodicalIndex = findPeriodicalByISSN(issn);
                            if (periodicalIndex == -1) {
                                JOptionPane.showMessageDialog(frame,
                                        "There is not periodical with such issn or this periodical is borrowed!",
                                        "",
                                        JOptionPane.ERROR_MESSAGE);
                            } else {
                                Periodical periodical = (Periodical) borrowable.get(periodicalIndex);
                                int confirm = JOptionPane.showConfirmDialog(frame, "Confirm?", "", 2);
                                if (confirm == 0) {
                                    if (member.isAbleToBorrowPeriodical()) {
                                        periodical.setOwner(member);
                                        member.addPeriodical(periodical);
                                        operations.add(new Operation(member, periodical, getCurrentDate(), "Borrow"));
                                    } else {
                                        JOptionPane.showMessageDialog(frame,
                                                "You can't borrow any more periodical!",
                                                "",
                                                JOptionPane.ERROR_MESSAGE);
                                    }
                                    break;
                                }
                            }
                            break;
                    }
                }
            }
        };
    }

    private Date getCurrentDate() {
        String month = this.month.getText().trim();
        String day = this.day.getText().trim();
        String year = this.year.getText().trim();
        if (month.matches("\\d+") && day.matches("\\d+") && year.matches("\\d+")) {
            return new Date(Integer.parseInt(month), Integer.parseInt(day), Integer.parseInt(year));
        } else {
            return new Date(-1, -1, -1);
        }

    }

    private int findMemberById(int id) {
        for (int i = 0; i < members.size(); i++) {
            if (members.get(i).getId() == id) {
                return i;
            }
        }
        return -1;
    }

    private int findBookByISBN(String isbn) {
        for (int i = 0; i < borrowable.size(); i++) {
            if (borrowable.get(i) instanceof Book) {
                Book b = (Book) borrowable.get(i);
                if (b.getIsbn().equals(isbn) && b.getStatus()) {
                    return i;
                }
            }
        }
        return -1;
    }

    private int findPeriodicalByISSN(String issn) {
        for (int i = 0; i < borrowable.size(); i++) {
            if (borrowable.get(i) instanceof Periodical) {
                Periodical b = (Periodical) borrowable.get(i);
                if (b.getIssn().equals(issn) && b.getStatus()) {
                    return i;
                }
            }
        }
        return -1;
    }

    private ActionListener returnItem() {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                Object[] options = {"Book",
                    "Periodical"};
                int choice = JOptionPane.showOptionDialog(frame,
                        "What do you want return: ", "Return Item",
                        JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,
                        null, options, options[1]);
                switch (choice) {
                    case 0:
                        String isbn = JOptionPane.showInputDialog(frame,
                                "Enter ISBN of the book:");
                        int bookIndex = findBookByISBN(isbn);
                        if (bookIndex != -1) {
                            Book book = (Book) borrowable.get(bookIndex);
                            Object[] options2 = {"Return",
                                "Cancel"};
                            if (!book.getStatus() && JOptionPane.showOptionDialog(frame,
                                    book.getTitle() + " is borrowed by " + book.getOwner().getName(), "Return Item",
                                    JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,
                                    null, options, options[1]) == 0) {
                                Member m = book.getOwner();
                                m.removeBook(book);
                                book.removeOwner();
                                operations.add(new Operation(m, book, getCurrentDate(), "Return"));
                            }
                        } else {
                            JOptionPane.showMessageDialog(frame,
                                    "There is no book with such ISBN",
                                    "",
                                    JOptionPane.ERROR_MESSAGE);
                        }
                        break;
                    case 1:
                        String issn = JOptionPane.showInputDialog(frame,
                                "Enter ISBN of the book:");
                        int periodicalIndex = findPeriodicalByISSN(issn);
                        if (periodicalIndex != -1) {
                            Periodical p = (Periodical) borrowable.get(periodicalIndex);
                            Object[] options2 = {"Return",
                                "Cancel"};
                            if (!p.getStatus() && JOptionPane.showOptionDialog(frame,
                                    p.getTitle() + " is borrowed by " + p.getOwner().getName(), "Return Item",
                                    JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,
                                    null, options, options[1]) == 0) {
                                Member m = p.getOwner();
                                m.removePeriodical(p);
                                p.removeOwner();
                                operations.add(new Operation(m, p, getCurrentDate(), "Return"));
                            }
                        } else {
                            JOptionPane.showMessageDialog(frame,
                                    "There is no periodical with such ISSN",
                                    "",
                                    JOptionPane.ERROR_MESSAGE);
                        }
                        break;
                }
            }
        };
    }

    private ActionListener searchItemByTitle() {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                String title = JOptionPane.showInputDialog(frame,
                        "Enter title:");
                Borrowable find = null;
                for (Borrowable b : borrowable) {
                    if (b.getBorrowableTitle().equals(title)) {
                        find = b;
                    }
                }
                if (find != null) {
                    if (find instanceof Book) {
                        Book book = (Book) find;
                        JOptionPane.showMessageDialog(frame,
                                "It is a book with ISBN: " + book.getIsbn(),
                                "",
                                JOptionPane.INFORMATION_MESSAGE);
                    } else if (find instanceof Periodical) {
                        Periodical p = (Periodical) find;
                        JOptionPane.showMessageDialog(frame,
                                "You it is a periodical with ISSN: " + p.getIssn(),
                                "",
                                JOptionPane.INFORMATION_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(frame,
                            "There is no item with such title",
                            "",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        };
    }

    private ActionListener currentlyBorrowed() {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                String id;
                do {
                    id = JOptionPane.showInputDialog(frame, "Enter member id:");
                    if (!id.matches("\\d+")) {
                        JOptionPane.showMessageDialog(frame,
                                "Please, enter only numbers!",
                                "",
                                JOptionPane.ERROR_MESSAGE);
                    }
                } while (!id.matches("\\d+"));
                int memberIndex = findMemberById(Integer.parseInt(id));
                if (memberIndex == -1) {
                    JOptionPane.showMessageDialog(frame,
                            "There is not member with such id!",
                            "",
                            JOptionPane.ERROR_MESSAGE);
                } else {
                    Member m = members.get(memberIndex);
                    ArrayList<Book> membersBooks = m.getBooks();
                    ArrayList<Periodical> membersPeriodicals = m.getPeriodicals();
                    String titles = "";
                    for (Book b : membersBooks) {
                        titles += "\"" + b.getTitle() + "\"" + "\n";
                    }
                    for (Periodical p : membersPeriodicals) {
                        titles += "\"" + p.getTitle() + "\"" + "\n";
                    }
                    JOptionPane.showMessageDialog(frame,
                            titles,
                            "",
                            JOptionPane.INFORMATION_MESSAGE);
                }
            }
        };
    }

    private ActionListener showByDate() {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                String date = JOptionPane.showInputDialog(frame, "Enter date in format \"MM-DD-YYYY\": ");
                String month = date.split("-")[0];
                String day = date.split("-")[1];
                String year = date.split("-")[2];
                Date dateOfBorrow;
                if (month.matches("\\d+") && day.matches("\\d+") && year.matches("\\d+")) {
                    dateOfBorrow = new Date(Integer.parseInt(month), Integer.parseInt(day), Integer.parseInt(year));
                } else {
                    dateOfBorrow = new Date(-1, -1, -1);
                }
                String out = "In that date was borrowed:\n";
                for (Operation o : operations) {
                    if (o.getType().equals("Borrow") && o.getBorrowedDate().isEqual(dateOfBorrow)) {
                        out += "\"" + o.getItem().getBorrowableTitle() + "\" is a "
                                + ((o.getItem() instanceof Book) ? "book" : "periodical")
                                + " borrowed by " + o.getMember().getName() + ";\n";
                    }
                }
                JOptionPane.showMessageDialog(frame,
                        out,
                        "",
                        JOptionPane.INFORMATION_MESSAGE);
            }
        };
    }

    private ActionListener save() {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                File membersFile = new File("members.txt");
                File itemsFile = new File("items.txt");
                File operationsFile = new File("operation.txt");
                String mem = "";
                if (!members.isEmpty()) {
                    for (Member member : members) {
                        mem += member.toString() + "\n";
                    }
                }
                String itm = "";
                if (!borrowable.isEmpty()) {
                    for (Borrowable borrow : borrowable) {
                        itm += borrow.toString() + "\n";
                    }
                }
                String oper = "";
                if (!operations.isEmpty()) {
                    for (Operation operation : operations) {
                        oper += operation.toString() + "\n";
                    }
                }
                try {
                    if (membersFile.exists()) {
                        PrintWriter out = new PrintWriter(membersFile.getAbsoluteFile());
                        try {
                            out.print(mem);
                            out.close();
                        } finally {
                            out.close();
                        }
                    } else if (!membersFile.exists()) {
                        membersFile.createNewFile();
                        PrintWriter out = new PrintWriter(membersFile.getAbsoluteFile());
                        try {
                            out.print(mem);
                            out.close();
                        } finally {
                            out.close();
                        }
                    }
                } catch (Exception ex) {

                }
                try {
                    if (itemsFile.exists()) {
                        PrintWriter out = new PrintWriter(itemsFile.getAbsoluteFile());
                        try {
                            out.print(itm);
                            out.close();
                        } finally {
                            out.close();
                        }
                    } else if (!itemsFile.exists()) {
                        itemsFile.createNewFile();
                        PrintWriter out = new PrintWriter(itemsFile.getAbsoluteFile());
                        try {
                            out.print(itm);
                            out.close();
                        } finally {
                            out.close();
                        }
                    }
                } catch (Exception ex) {

                }
                try {
                    if (operationsFile.exists()) {
                        PrintWriter out = new PrintWriter(operationsFile.getAbsoluteFile());
                        try {
                            out.print(oper);
                            out.close();
                        } finally {
                            out.close();
                        }
                    } else if (!operationsFile.exists()) {
                        operationsFile.createNewFile();
                        PrintWriter out = new PrintWriter(operationsFile.getAbsoluteFile());
                        try {
                            out.print(oper);
                            out.close();
                        } finally {
                            out.close();
                        }
                    }
                } catch (Exception ex) {

                }
            }
        };
    }

    private static void restore() throws FileNotFoundException {
        File membersFile = new File("members.txt");
        File itemsFile = new File("items.txt");
        File operationsFile = new File("operation.txt");
        Scanner sc;
        String s;
        if (membersFile.exists()) {
            sc = new Scanner(membersFile);
            while (sc.hasNext()) {
                s = sc.nextLine();
                if (!s.equals("")) {
                    members.add(Member.newMember(s));
                }
            }
        }
        if (itemsFile.exists()) {
            sc = new Scanner(itemsFile);
            while (sc.hasNext()) {
                s = sc.nextLine();
                if (!s.equals("")) {
                    borrowable.add(Borrowable.newBorrowable(s));
                }
            }
        }
        if (operationsFile.exists()) {
            sc = new Scanner(operationsFile);
            while (sc.hasNext()) {
                s = sc.nextLine();
                if (!s.equals("")) {
                    operations.add(new Operation(s));
                }
            }
        }
    }
}

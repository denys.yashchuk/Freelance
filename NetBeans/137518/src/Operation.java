
import Borrowable.Borrowable;
import Member.Member;

public class Operation {

    Member member;
    Borrowable item;
    Date borrowedDate;
    String type;

    public Operation(Member member, Borrowable item, Date borrowedDate, String type) {
        this.member = member;
        this.item = item;
        this.borrowedDate = borrowedDate;
        this.type = type;
    }

    public Operation(String s) {
        String[] parts = s.split("**");
        this.member = Member.newMember(parts[0]);
        this.item = Borrowable.newBorrowable(parts[1]);
        this.borrowedDate = new Date(parts[2]);
        this.type = parts[3];
    }

    public Member getMember() {
        return member;
    }

    public Borrowable getItem() {
        return item;
    }

    public Date getBorrowedDate() {
        return borrowedDate;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return member.toString() + "**" + item.toString() + "**" + borrowedDate.toString() + "**" + type;
    }

}

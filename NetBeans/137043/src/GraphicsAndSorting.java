
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

// ****************************************************************************
// Name: (Your Name) Folder Name: (name of folder in dropbox)
// C8162 Spring 2016 Lab #: 6
// Class time: (Tues/Thurs 10:00 am))
// Date: (date done - ready to turn in)
// Program Name: Animation
// ****************************************************************************
public class GraphicsAndSorting extends JPanel {

    private static final int DELAY = 1000;
    private int[] randArray = new int[25];
    int index, min;
    Timer timer = new Timer(DELAY, new BarListener());

    public GraphicsAndSorting() {
        randArray = new int[25];
        Random r = new Random();
        for (int i = 0; i < randArray.length; i++) {
            randArray[i] = r.nextInt(90) + 10;
        }
        setPreferredSize(new Dimension(1000, 600));
        index = 0;
        timer.start();
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        drawBars(g);
    }

    public void drawBars(Graphics g) {
        for (int i = 0; i < randArray.length; i++) {
            if (i == index - 1 || i == min) {
                g.setColor(Color.red);
            } else {
                g.setColor(Color.blue);
            }
            int[] x = {i * 40, i * 40, i * 40 + 38, i * 40 + 38};
            int[] y = {510-randArray[i] * 5, 510, 510, 510-randArray[i] * 5};
            g.fillPolygon(x, y, x.length);
            g.drawString(Integer.toString(randArray[i]), i*40, 520);
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.add(new GraphicsAndSorting());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    } // end main

    private class BarListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            int temp;
            min = index;
            for (int scan = index + 1; scan < randArray.length; scan++) {
                if (randArray[scan] < randArray[min]) {
                    min = scan;
                }
            }
            temp = randArray[min];
            randArray[min] = randArray[index];
            randArray[index] = temp;
            index++;
            if (index == randArray.length) {
                timer.stop();
            }
            repaint();
        }

    }
} // end class

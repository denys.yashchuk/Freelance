
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Polygon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

// ****************************************************************************
// Name: (Your Name) Folder Name: (name of folder in dropbox)
// C8162 Spring 2016 Lab #: 6
// Class time: (Tues/Thurs 10:00 am))
// Date: (date done - ready to turn in)
// Program Name: Animation
// ****************************************************************************
public class Animation extends JPanel {

    private static final int D_W = 800;
    private static final int D_H = 600;
    Car car;

    public Animation() {
        car = new Car(0, 50);

        Timer timer = new Timer(50, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                car.move();
                repaint();
            }
        });
        timer.start();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        car.drawCar(g);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(D_W, D_H);
    }

    public class Car {

        private static final int INCREMENT = 5;
        int x, y;
        boolean moveRight;
        int[] xCarBody = {450, 450, 75, 75};
        int[] yCarBody = {425, 275, 275, 425};
        int xLeftWheel = 150;
        int xRightWheel = 350;
        int xHeadlight = 445;
        int[] xWindshield = {440,440,400,400};
        int[] yWindshield = {320, 280, 280, 320};

        public Car(int x, int y) {
            this.x = x;
            this.y = y;
            moveRight = true;
        }

        public void drawCar(Graphics g) {
            g.setColor(Color.red);
            Polygon carBody = new Polygon();

            // add points to the polygon
            g.fillPolygon(xCarBody, yCarBody, xCarBody.length);
            g.setColor(Color.black);
            g.drawPolyline(xCarBody, yCarBody, xCarBody.length);

            // draw the tires
            g.setColor(Color.black);   // color of tires
            g.fillOval(xLeftWheel, 375, 75, 75);
            g.fillOval(xRightWheel, 375, 75, 75);

            // draw headlights
            g.setColor(Color.yellow);   // color of headlights
            g.fillOval(xHeadlight, 360, 10, 40);

            // draw front windshield
            g.setColor(Color.blue);
            g.fillPolygon(xWindshield, yWindshield, xWindshield.length);
            
            g.setColor(Color.black);
            g.drawLine(0, 450, D_W, 450);
        }

        public void move() {
            if (moveRight) {
                if (xCarBody[0] == D_W) {
                    moveRight = false;
                    xLeftWheel -= 50;
                    xRightWheel -= 50;
                    xHeadlight -= 375;
                    for(int i =0;i< xWindshield.length; i++){
                        xWindshield[i] -= 315;
                    }
                } else {
                    for (int i = 0; i < xCarBody.length; i++) {
                        xCarBody[i] += INCREMENT;
                    }
                    for (int i = 0; i < xWindshield.length; i++) {
                        xWindshield[i] += INCREMENT;
                    }
                    xLeftWheel += INCREMENT;
                    xRightWheel += INCREMENT;
                    xHeadlight += INCREMENT;
                }
            } else{
                if (xCarBody[3] == 0) {
                    moveRight = true;
                    xLeftWheel += 50;
                    xRightWheel += 50;
                    xHeadlight += 375;
                    for(int i =0;i< xWindshield.length; i++){
                        xWindshield[i] += 315;
                    }
                } else {
                    for (int i = 0; i < xCarBody.length; i++) {
                        xCarBody[i] -= INCREMENT;
                    }
                    for (int i = 0; i < xWindshield.length; i++) {
                        xWindshield[i] -= INCREMENT;
                    }
                    xLeftWheel -= INCREMENT;
                    xRightWheel -= INCREMENT;
                    xHeadlight -= INCREMENT;
                }
            }
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                JFrame frame = new JFrame();
                frame.add(new Animation());
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.pack();
                frame.setVisible(true);
            }
        });
    }
} // end class

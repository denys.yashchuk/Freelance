
import java.util.Random;

public class Main {

    static final int SIZE_OF_ARRAY = 10;
    static final int MAX_VALUE = 100;

    public static void main(String[] args) {
        // TODO code application logic here
        Random rand = new Random();
        int[] array = new int[SIZE_OF_ARRAY];
        for (int i = 0; i < SIZE_OF_ARRAY; i++) {
            array[i] = rand.nextInt(MAX_VALUE);
        }
        for (int el : array) {
            System.out.print(el + " ");
        }
        System.out.println();
        quickSort(array, 0, SIZE_OF_ARRAY - 1);
        for (int el : array) {
            System.out.print(el + " ");
        }
    }

    public static void quickSort(int[] array, int l, int r) {
        int i = l;
        int j = r;
        int x = array[(l + r) / 2];
        while (i <= j) {
            while (array[i] < x) {
                i++;
            }
            while (array[j] > x) {
                j--;
            }
            if (i <= j) {
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
                i++;
                j--;
            }
        }
        if (l < j) {
            quickSort(array, l, j);
        }
        if (i < r) {
            quickSort(array, i, r);
        }
    }

}

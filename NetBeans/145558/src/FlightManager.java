
public class FlightManager {

    private Flight[] flights;
    private int numberOfFlights;

    public FlightManager() {
        flights = new Flight[5];
        numberOfFlights = 0;
    }

    public void addFlight(Flight flight) {
        if (numberOfFlights == flights.length - 1) {
            increaseSize();
        }
        flights[++numberOfFlights] = flight;
    }

    private void increaseSize() {
        Flight[] newArray = new Flight[flights.length * 2];
        System.arraycopy(flights, 0, newArray, 0, flights.length);
        flights = newArray;
    }

    public Itinerary[] findItineraries(Airport sAirport, Airport dAirport,
            Time deTime) {
        Itinerary[] itineraries = new Itinerary[100];
        int index = 0;
        for (int i = 0; i < numberOfFlights; i++) {
            Flight f = flights[i];
            if (f.getDeparture().isLaterThan(deTime)
                    && f.getSource().equals(sAirport)) {
                if (f.getDestination().equals(dAirport)) {
                    itineraries[index] = new Itinerary(f);
                    index++;
                } else {
                    for (int j = 0; j < numberOfFlights; j++) {
                        if (flights[j].getDestination().equals(dAirport)
                                && f.getDestination().equals(flights[j].getSource())
                                && f.getArrival().isEarlierThan(flights[j].getDeparture())) {
                            itineraries[index] = new Itinerary(f, flights[j]);
                            index++;
                        }
                    }
                }
            }
        }
        return shrinkItineraries(itineraries);
    }

    private Itinerary[] shrinkItineraries(Itinerary[] itineraries) {
        int size = 0;
        while (itineraries[size] != null) {
            size++;
        }
        Itinerary[] part = new Itinerary[++size];
        System.arraycopy(itineraries, 0, part, 0, size);
        return part;
    }

}

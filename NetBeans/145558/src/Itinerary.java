
public class Itinerary {

    private Flight first;
    private Flight second;

    public Itinerary(Flight f1) {
        first = f1;
        second = null;
    }

    public Itinerary(Flight f1, Flight f2) {
        first = f1;
        second = f2;
    }

    public Flight getFirst() {
        return first;
    }

    public Flight getSecond() {
        return second;
    }

    public boolean hasConnection() {
        return second != null;
    }

    public double getTotalCost() {
        double totalCost = first.getCost();
        if (hasConnection()) {
            totalCost += second.getCost();
        }
        return totalCost;
    }

    public Time getDeparture() {
        return first.getDeparture();
    }

    public Time getArrival() {
        if (hasConnection()) {
            return second.getArrival();
        }
        return first.getArrival();
    }

    @Override
    public String toString() {
        String s = first.toDetailedString();
        if (hasConnection()) {
            s += "\n" + second.toDetailedString();
        }
        return s;
    }
}


public class Flight {
    Plane plane;
    String number;
    double cost;
    Time departure;
    int duration;
    Airport source;
    Airport destination;

    public Flight(Plane plane, String number, double cost, Time departure,
                  int duration, Airport source, Airport destination) {
        this.plane = plane;
        this.number = number;
        this.cost = cost;
        this.departure = departure;
        this.duration = duration;
        this.source = source;
        this.destination = destination;
    }

    public Plane getPlane() {
        return plane;
    }

    public String getNumber() {
        return number;
    }

    public double getCost() {
        return cost;
    }

    public Airport getDestination() {
        return destination;
    }

    public Time getDeparture() {
        return departure;
    }

    public Time getArrival() {
        Time arrival = departure.getCopy();
        arrival.addMinutes(duration);
        return arrival;
    }

    public Airport getSource() {
        return source;
    }

    public String toOverviewString() {
        String str = "";
        int h = 0, m = duration;
        while (m > 59) {
            h++;
            m -= 60;
        }
        str += "$" + cost + "0 \n" + departure + " - " + getArrival() +
                "       " + h + "h:" + m + "m \n" +
                plane.getAirline() + "                " + source + "-" + destination;


        return str;
    }

    public String toDetailedString() {
       return departure + " - " + getArrival() + "\n" + Airport.getAirportCity(source) +
                " (" + source + ") - " + Airport.getAirportCity(destination) +
                " (" + destination + ") \n" + plane.getAirline() + " " + number + " * " + plane.getModel();
    }

}

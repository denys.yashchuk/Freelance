
import java.util.Scanner;

public class FlightSearchEngine {
    
    FlightManager fm;
    Scanner sc;
    
    public FlightSearchEngine() {
        fm = new FlightManager();
        sc = new Scanner(System.in);
    }
    
    public static void main(String[] args) {
        int[][] arr = new int[3][3];
        for(int i = 0; i < arr.length; i++){
            for(int j = 0; j < arr[i].length; j++){
                System.out.print(arr[i][j]+"\t");
            }
            System.out.println("");
        }
    }
    
    private void loadFlights() {

    }
    
    public void runSearch() {
        System.out.println("Choose the source Airport");
        Airport sAirport = readAirport();
        System.out.println("Choose the destination Airport");
        Airport dAirport = readAirport();
        System.out.println("Enter departure hours:");
        int h = sc.nextInt();
        System.out.println("Enter departure minutes:");
        int m = sc.nextInt();
        Itinerary[] it = fm.findItineraries(sAirport, dAirport, new Time(h, m));
        if (it.length == 0) {
            System.out.println("No itineraries found.");
        } else {
            displayItineraries(it);
            System.out.println("Minimum" + findCheapestItinerary(it));
        }
    }
    
    private Airport readAirport() {
        System.out.println("Enter the number of the Airport:\n"
                + "1. Phoenix\n"
                + "2. Los Angeles\n"
                + "3. San Francisco\n"
                + "4. Tokyo");
        int number = sc.nextInt();
        switch (number) {
            case 1:
                return Airport.PHX;
            case 2:
                return Airport.LAX;
            case 3:
                return Airport.SFO;
            case 4:
                return Airport.NRT;
            default:
                System.out.println("Make right choice!!!");
                return readAirport();
        }
    }
    
    public Itinerary findCheapestItinerary(Itinerary[] itineraries) {
        Itinerary min = itineraries[0];
        for (int i = 1; i < itineraries.length; i++) {
            if (min.getTotalCost() > itineraries[i].getTotalCost()) {
                min = itineraries[i];
            }
        }
        return min;
    }
    
    public void displayItineraries(Itinerary[] itineraries) {
        for (int i = 0; i < itineraries.length; i++) {
            System.out.println((i + 1) + ". " + itineraries[i] + "\n\n");
        }
    }
}

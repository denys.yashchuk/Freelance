

public class Plane {
    private Airline airline;
    private String model;

    public Plane(Airline airline, String model) {
        this.airline = airline;
        this.model = model;
    }

    public Airline getAirline() {
        return airline;
    }

    public String getModel() {
        return model;
    }

    public String toString() {
        return airline + " * " + model;
    }
}

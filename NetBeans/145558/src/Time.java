
public class Time {

    private int hour;
    private int minute;

    public Time() {
        hour = 12;
        minute = 0;
    }

    public Time(int hour, int minute) {
        this.hour = hour;
        this.minute = minute;
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public void addHours(int extraHours) {
        hour += extraHours;
        if (hour > 23) {
            hour -= 24;
        }
    }

    public void addMinutes(int extraMinutes) {
        minute += extraMinutes;
        while (minute > 59) {
            minute -= 60;
            this.addHours(1);
        }
    }

    public void addTime(Time time) {
        this.addHours(time.getHour());
        this.addMinutes(time.getMinute());
    }

    public Time getCopy() {
        return new Time(this.getHour(), this.getMinute());
    }

    public boolean isEarlierThan(Time other) {
        if (this.getHour() < other.getHour()) {
            return true;
        } else {
            if (this.getMinute() < other.getMinute()
                    && this.getHour() == other.getHour()) {
                return true;
            } else {
                return false;
            }
        }
    }

    public boolean isSameTime(Time other) {
        return (this.getHour() == other.getHour()
                && this.getMinute() == other.getMinute());
    }

    public boolean isLaterThan(Time other) {
        return !isEarlierThan(other) && !isSameTime(other);
    }

    public String toString() {
        String str = "";
        String tmp;
        int currHour = hour;

        if (currHour < 12) {
            tmp = "AM";
        } else if (currHour == 12 && minute == 0) {
            tmp = "AM";
        } else {
            tmp = "PM";
            currHour -= 12;
        }

        if (currHour == 0) {
            currHour += 12;
        }

        str += currHour;

        str += ":";

        if (minute < 10) {
            str += "0" + minute;
        } else {
            str += minute;
        }

        str += tmp;

        return str;
    }
}


public enum Airport {
    PHX, LAX, SFO, NRT, SIN;

    public static String getAirportCity(Airport airport) {
        switch (airport) {
            case PHX:
                return "Phoenix";
            case LAX:
                return "Los Angeles";
            case SFO:
                return "San Francisco";
            case NRT:
                return "Tokyo";
            default:
                return "Unknown city";
        }
    }
}


public class TimeDateApp {

    Time time;
    Date date;
    int timeZone;

    public TimeDateApp() {
        this.time = new Time(0, 0, 0);
        this.date = new Date(1, 1, 0);
    }

    public TimeDateApp(Time time, Date date, int timeZone) {
        this.time = time;
        this.date = date;
        this.timeZone = timeZone;
    }

    public TimeDateApp(int day, int month, int year, int hour, int minute, int second, int timeZone) {
        this.time = new Time(hour, minute, second);
        this.date = new Date(day, month, year);
        this.timeZone = timeZone;
    }

    public void setHour(int hour) {
        this.time.setHour(hour);
    }

    public void setMinute(int minute) {
        this.time.setMinute(minute);
    }

    public void setSecond(int second) {
        this.time.setSecond(second);
    }

    public void setDay(int day) {
        this.date.setDay(day);
    }

    public void setMonth(int month) {
        this.date.setMonth(month);
    }

    public void setYear(int year) {
        this.date.setYear(year);
    }
    
    public int getHour() {
        return this.time.getHour();
    }

    public int getMinute() {
        return this.time.getMinute();
    }

    public int getSecond() {
        return this.time.getSecond();
    }
    
        public int getDay() {
        return this.date.getDay();
    }

    public int getMonth() {
        return this.date.getMonth();
    }

    public int getYear() {
        return this.date.getYear();
    }
    
    public String toUniversalTime(){
        return getYear()+"-"+getMonth()+"-"+getDay()+" "+getHour()+":"+getMinute()+":"+getSecond()+";";
    }
    
    public String toStandardTime(){
        return getDay()+"."+getMonth()+"."+getYear()+" "+(getHour()+timeZone)+":"+getMinute()+":"+getSecond()
                +" (GMT"+(timeZone>0?"+":"-")+timeZone+")";
    }

    @Override
    public String toString() {
        return "TimeDateApp{" + "time=" + time + ", date=" + date + '}';
    }
}

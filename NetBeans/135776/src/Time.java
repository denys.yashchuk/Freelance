
public class Time {

    private int hour;
    private int minute;
    private int second;

    public Time(int hour, int minute, int second) {
        setHour(hour);
        setMinute(minute);
        setSecond(second);
    }

    public void setHour(int hour) {
        if (hour >= 0 && hour <= 23) {
            this.hour = hour;
        } else {
            System.out.println("Wrong hour value. Seting 0.");
            this.hour = 0;
        }
    }

    public void setMinute(int minute) {
        if (minute >= 0 && minute <= 60) {
            this.minute = minute;
        } else {
            System.out.println("Wrong minute value. Seting 0.");
            this.minute = 0;
        }
    }

    public void setSecond(int second) {
        if (second >= 0 && second <= 60) {
            this.second = second;
        } else {
            System.out.println("Wrong second value. Seting 0.");
            this.second = 0;
        }
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public int getSecond() {
        return second;
    }

    @Override
    public String toString() {
        return "Time{" + "hour=" + hour + ", minute=" + minute + ", second=" + second + '}';
    }

}

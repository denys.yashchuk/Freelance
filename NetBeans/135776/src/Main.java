
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter day: (1-31)");
        int day = sc.nextInt();
        System.out.println("Enter month: (1-12)");
        int month = sc.nextInt();
        System.out.println("Enter year: (>0)");
        int year = sc.nextInt();
        System.out.println("Enter hour: (0-23)");
        int hour = sc.nextInt();
        System.out.println("Enter minute: (0-59)");
        int minute = sc.nextInt();
        System.out.println("Enter second: (0-59)");
        int second = sc.nextInt();
        Date date = new Date(day, month, year);
        Time time = new Time(hour, minute, second);
        TimeDateApp tda = new TimeDateApp(time, date, 3);
        System.out.println("Universal Time: "+tda.toUniversalTime());
        System.out.println("Standart Time: "+tda.toStandardTime());

    }

}

import java.io.PrintStream;
import java.io.IOException;
import java.net.Socket;
import java.net.ServerSocket;

// the Server class
public class Server {
   // The server socket.
  private static ServerSocket serverSocket = null;
  // The client socket.
  private static Socket clientSocket = null;

  // This chat server can accept up to maxClientsCount clients' connections.
  private static final int MAX_CLIENTS = 10;
  private static final clientThread[] THREADS = new clientThread[MAX_CLIENTS];

  public static void main(String args[]) {
    // The default port number.
    int portNumber = 2222;
    System.out.println("Server starts on port: " + portNumber);

    /*
     * Open a server socket on the portNumber.
     */
    try {
      serverSocket = new ServerSocket(portNumber);
    } catch (IOException e) {
      System.out.println(e);
    }

    /*
     * Create a client socket for each connection and pass it to a new client
     * thread.
     */
    while (true) {
      try {
        clientSocket = serverSocket.accept();
        int i = 0;
        for (i = 0; i < MAX_CLIENTS; i++) {
          if (THREADS[i] == null) {
            (THREADS[i] = new clientThread(clientSocket, THREADS)).start();
            break;
          }
        }
        if (i == MAX_CLIENTS) {
          PrintStream os = new PrintStream(clientSocket.getOutputStream());
          os.println("Server too busy. Try later.");
          os.close();
          clientSocket.close();
        }
      } catch (IOException e) {
        System.out.println(e);
      }
    }
  }  
}

import javax.swing.*;
import java.awt.*;

/**
 *
 */
public class PanelFrame extends JFrame {//could you please check page 37-39 in chap document and use the same method to add panel 

    public static final int DEFAULT_WIDTH = 400;
    public static final int DEFAULT_HEIGHT = 400;

    private CalculationPanel calculationPanel;
    private ColorPanel colorPanel;
    private MessageBuilderPanel messagePanel;

    public static void main(String[] args) {
        JFrame frame = new PanelFrame();
    }

    public PanelFrame(){

        setTitle("3 Panels");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new FlowLayout(FlowLayout.LEFT,0,0));
        
        
        calculationPanel = new CalculationPanel();
        colorPanel = new ColorPanel();
        messagePanel = new MessageBuilderPanel();
        
        add(calculationPanel);
        add(colorPanel);
        add(messagePanel);
        setSize(402,430); // size of window (with header and border)
        setPreferredSize(new Dimension(DEFAULT_WIDTH, DEFAULT_HEIGHT));
        getContentPane().setBackground(Color.black);
        
        setResizable(true);

        setVisible(true);
    }
}

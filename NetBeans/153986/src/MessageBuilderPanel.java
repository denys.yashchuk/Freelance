import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**
 *
 */
public class MessageBuilderPanel extends JPanel{

    private JLabel label1;
    private JLabel label2;
    private JCheckBox one;
    private JCheckBox two;
    private JCheckBox three;
    private JCheckBox four;
    private JCheckBox five;
    private JCheckBox six;

    public MessageBuilderPanel(){

        label1 = new JLabel();
        label2 = new JLabel();

        label1.setHorizontalAlignment(JLabel.CENTER);
        label2.setHorizontalAlignment(JLabel.CENTER);

        label1.setForeground(Color.RED);
        label2.setForeground(Color.RED);
        // we can't use one Label, because two string are divided by check boxes
        
        JPanel checkPanel1 = new JPanel();
        JPanel checkPanel2 = new JPanel();

        one   = new JCheckBox("One");
        two   = new JCheckBox("Two");
        three = new JCheckBox("Three");
        four  = new JCheckBox("Four");
        five  = new JCheckBox("Five");
        six   = new JCheckBox("Six");

        ItemListener listener = new MessageBuilderListener();

        one .addItemListener(listener);
        two.addItemListener(listener);
        three.addItemListener(listener);
        four.addItemListener(listener);
        five.addItemListener(listener);
        six.addItemListener(listener);

        checkPanel1.setLayout(new FlowLayout());
        checkPanel1.add(one);
        checkPanel1.add(two);
        checkPanel1.add(three);

        checkPanel2.setLayout(new FlowLayout());
        checkPanel2.add(four);
        checkPanel2.add(five);
        checkPanel2.add(six);

        setLayout(new GridLayout(4, 0));//this panel has grig layout and nested 
        //panels have flow layout (Contains nested panels (grid layout and flow 
        //layout) with a title border

        add(checkPanel1);
        add(label1);

        add(checkPanel2);
        add(label2);

        setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Message Builder")); // add border to panel

        setPreferredSize(new Dimension(PanelFrame.DEFAULT_WIDTH/2, PanelFrame.DEFAULT_HEIGHT/2));
    }

    private class MessageBuilderListener implements ItemListener{

        @Override
        public void itemStateChanged(ItemEvent e) {
            String outForLabel1 = "";
            String outForLabel2 = "";

            if(one.isSelected())
                outForLabel1 += "One.";

            if(two.isSelected())
                outForLabel1 += "Two.";

            if(three.isSelected())
                outForLabel1 +="Three.";

            if(four.isSelected())
                outForLabel2 += "Four.";

            if(five.isSelected())
                outForLabel2 += "Five.";

            if(six.isSelected())
                outForLabel2 += "Six.";

            label1.setText(outForLabel1);
            label2.setText(outForLabel2);
        }
    }

}

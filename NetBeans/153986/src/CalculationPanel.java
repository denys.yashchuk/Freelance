
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;//can't use this
import java.awt.event.FocusEvent;//didn't have this
import javax.swing.border.Border;

/**
 *
 */
public class CalculationPanel extends JPanel {

    private JTextField number1Field;
    private JTextField number2Field;
    private ButtonGroup operation;
    private JPanel radioPanel;
    private JButton calculateButton;
    private JRadioButton add;
    private JRadioButton subtract;
    private JRadioButton multiply;
    private JRadioButton divide;

    public CalculationPanel()
    {

        JLabel label1 = new JLabel("Number 1");
        JLabel label2 = new JLabel("Number 2");

        number1Field = new JTextField();
        number2Field = new JTextField();

        operation = new ButtonGroup();

        calculateButton = new JButton("Calculate");
        calculateButton.addActionListener(new CalculationListener());

        setPreferredSize(new Dimension(PanelFrame.DEFAULT_WIDTH/2, PanelFrame.DEFAULT_HEIGHT/2));
        FlowLayout layout = new FlowLayout(FlowLayout.CENTER);
        // 5 pixel black border
        setBorder(BorderFactory.createLineBorder(Color.black, 5));
        setLayout(layout);

        add(getPanelWithField(label1, number1Field));
        add(getPanelWithField(label2, number2Field));
        add      = addToButtonGroup("add");
        subtract = addToButtonGroup("subtract");
        multiply = addToButtonGroup("multiply");
        divide   = addToButtonGroup("divide");
        add(calculateButton);

    }

    private JPanel getPanelWithField(JLabel label, JTextField textField){

        JPanel panel = new JPanel();

        textField.setPreferredSize(new Dimension(100, 20));
        textField.setHorizontalAlignment(JLabel.CENTER);
    
        // This works in my computer.
        textField.addFocusListener(new FocusAdapter()
         {
            @Override
            public void focusGained(FocusEvent e) {
                textField.selectAll(); // selects text, when coursors is put to the text field
            }

            @Override
            public void focusLost(FocusEvent e) {
                textField.select(0, 0); // deselects text, when coursors is taken from the text field
            }
            
        });

        panel.add(label, BorderLayout.WEST);
        panel.add(textField, BorderLayout.CENTER);

        return panel;
    }

    private JRadioButton addToButtonGroup(String name){
        JRadioButton rb = new JRadioButton(name);
        operation.add(rb);
        this.add(rb);
        return rb;
    }

    private class CalculationListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent event) {

            double num1 = 0;
            double num2 = 0;

            if(number1Field.getText().trim().matches("([0-9]*)\\.?([0-9]*)?")){
                num1 = Double.parseDouble(number1Field.getText().trim()); 
            } else{                            
                number1Field.setText("Incorrect input");
                return;
            }
            
            if(number2Field.getText().trim().matches("([0-9]*)\\.?([0-9]*)?")){
                num2 = Double.parseDouble(number2Field.getText().trim()); 
            } else{                            
                number2Field.setText("Incorrect input");
                return;
            }

            JOptionPane.showMessageDialog(null, getResult(num1, num2));
        }
    }

    private String getResult(double num1, double num2){

        if (add.isSelected())
            return "Result of add = " + (num1 + num2);
        else if (subtract.isSelected())
            return "Result of subtract = " + (num1 - num2);
        else if (multiply.isSelected())
            return "Result of multiply = " + (num1*num2);
        else if (divide.isSelected())
            if (num2 == 0)
                return "Error. Division by 0";
            else
                return "Result of divide = " + (num1/num2);
        else return "No radio button selected";

    }
}

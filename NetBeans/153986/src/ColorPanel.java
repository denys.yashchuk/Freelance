import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 */
public class ColorPanel extends JPanel {

    private JButton colorButton;
    private Color color1 = Color.WHITE;
    private Color color2 = Color.RED;
    private JPanel panel;

    public ColorPanel(){

        // Calculate size of panel(panel is less than frame in 4 times and -10px for spacing between panels)
        setPreferredSize(new Dimension(PanelFrame.DEFAULT_WIDTH/2, PanelFrame.DEFAULT_HEIGHT/2));
        setBackground(color1);

        setLayout(new FlowLayout(FlowLayout.CENTER, 15, 10));

        panel = new JPanel();
        panel.setBackground(color2);

        // Calculate size of panel(panel is less than frame in 4 times and -10px for spacing between panels)
        panel.setPreferredSize(new Dimension(PanelFrame.DEFAULT_WIDTH/2 - 10, PanelFrame.DEFAULT_HEIGHT/2 - 20));// makes white border.

        colorButton = new JButton("Change");
        colorButton.addActionListener(new SwapColorsListener());

        panel.add(colorButton, BorderLayout.NORTH);
        add(panel);
    }

    private class SwapColorsListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            swapColors();
        }
    }

    private void swapColors() {
        Color buf = color2;
        color2 = color1;
        color1 = buf;

        this.setBackground(color1);
        panel.setBackground(color2);
    }
}

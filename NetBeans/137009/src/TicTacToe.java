
import java.util.Scanner;

public class TicTacToe {

    public static Scanner sc = new Scanner(System.in);
    public static String userName;
    public static int currentPlayer;
    public static char usym = 'X';
    public static char csym = 'O';

    public static void main(String[] args) {
        char[] board = new char[9];
        resetGame(board);
        splashScreen();
        System.out.println("Enter Y to continue or any other to quit: ");
        String answer = sc.nextLine();
        askForUserNames();
        do {
            currentPlayer = 2;
            resetGame(board);
            displayGrid(board);
            while (!checkTie(board) && !checkWin(board)) {
                switchPlayer();
                play(board);
                if(currentPlayer == 2)displayGrid(board);
            }
            System.out.println("game over!, play again? y/n ");
            answer = sc.nextLine();
        } while (answer.trim().toUpperCase().equals("Y"));
    }

    public static void play(char[] brd) {
        if (currentPlayer == 1) {
            playerMakeMove(brd);
        } else if (currentPlayer == 2) {
            compPlay(brd);
        }
    }

    public static void splashScreen() {
        System.out.println("##############################");
        System.out.println("##############################");
        System.out.println("##############################");
        System.out.println("####### Tic Tac Toe ##########");
        System.out.println("#### Welcome to the game #####");
        System.out.println("##############################");
        System.out.println("##############################");
        System.out.println("##############################");
    }

    public static void askForUserNames() {
        String name;
        do {
            System.out.println("Enter your username: ");
            name = sc.nextLine();
        } while (!validateUserName(name));
        userName = name;
    }

    public static boolean validateUserName(String name) {
        char[] chars = name.toCharArray();
        for (char c : chars) {
            if (!Character.isLetter(c)) {
                return false;
            }
        }
        return true;
    }

    public static void switchPlayer() {
        if (currentPlayer == 1) {
            currentPlayer = 2;
        } else if (currentPlayer == 2) {
            currentPlayer = 1;
        }
    }

    public static void resetGame(char[] brd) {
        for (int i = 0; i < brd.length; i++) {
            brd[i] = Character.forDigit(i, 10);
        }
    }

    public static void displayGrid(char[] brd) {
        System.out.println();
        System.out.println("\n\tTIC-TAC-TOE\n");
        System.out.print("\t:-----:\n");
        for (int i = 0; i < 3; i++) {
            System.out.print("\t:");
            for (int j = 0; j < 3; j++) {
                System.out.print(brd[i * 3 + j] + ":");
            }
            System.out.println();
        }
        System.out.print("\t:-----:");
    }

    public static void playerMakeMove(char[] brd) {
        String move;
        do {
            System.out.print("\nit is the human's turn\nGive me your bestmove! ");
            move = sc.nextLine();
        } while (!validatePlayersMove(brd, move));
        brd[Integer.parseInt(move.trim())] = usym;
    }

    public static boolean validatePlayersMove(char[] brd, String move) {
        if (move.trim().matches("[0-9]+")) {
            int num = Integer.parseInt(move.trim());
            if (num >= 0 && num < 9) {
                return checkPositionAvailability(brd, num);
            }
        }
        return false;
    }

    public static boolean checkPositionAvailability(char[] brd, int move) {
        if (brd[move] == Character.forDigit(move, 10)) {
            return true;
        } else {
            System.out.print("\nposition not available\nmake a different choice");
            return false;
        }
    }

    public static void compPlay(char[] brd) {

        int firstCol = 0;
        int secondCol = 0;
        int thirdCol = 0;
        int firstRow = 0;
        int secondRow = 0;
        int thirdRow = 0;
        int firstDiagonal = 0;
        int secondDiagonal = 0;
        for (int i = 0; i < 3; i++) {
            if (brd[i] == csym) {
                firstRow++;
            }
        }
        for (int i = 0; i < 3; i++) {
            if (brd[i + 3] == csym) {
                secondRow++;
            }
        }
        for (int i = 0; i < 3; i++) {
            if (brd[i + 6] == csym) {
                thirdRow++;
            }
        }
        for (int i = 0; i < 3; i++) {
            if (brd[i * 3] == csym) {
                firstCol++;
            }
        }
        for (int i = 0; i < 3; i++) {
            if (brd[1 + i * 3] == csym) {
                secondCol++;
            }
        }
        for (int i = 0; i < 3; i++) {
            if (brd[2 + i * 3] == csym) {
                thirdCol++;
            }
        }
        for (int i = 0; i < 3; i++) {
            if (brd[i * 4] == csym) {
                firstDiagonal++;
            }
        }
        for (int i = 0; i < 3; i++) {
            if (brd[2 + i * 2] == csym) {
                secondDiagonal++;
            }
        }

        if (firstRow % 3 == 2) {
            for (int i = 0; i < 3; i++) {
                if (brd[i] != csym && brd[i] != usym) {
                    brd[i] = csym;
                    return;
                }
            }
        } else if (secondRow % 3 == 2) {
            for (int i = 0; i < 3; i++) {
                if (brd[i + 3] != csym && brd[i + 3] != usym) {
                    brd[i + 3] = csym;
                    return;
                }
            }
        } else if (thirdRow % 3 == 2) {
            for (int i = 0; i < 3; i++) {
                if (brd[i + 6] != csym && brd[i + 6] != usym) {
                    brd[i + 6] = csym;
                    return;
                }
            }
        } else if (firstCol % 3 == 2) {
            for (int i = 0; i < 3; i++) {
                if (brd[i * 3] != csym && brd[i * 3] != usym) {
                    brd[i * 3] = csym;
                    return;
                }
            }
        } else if (secondCol % 3 == 2) {
            for (int i = 0; i < 3; i++) {
                if (brd[i * 3 + 1] != csym && brd[i * 3 + 1] != usym) {
                    brd[i * 3 + 1] = csym;
                    return;
                }
            }
        } else if (thirdCol % 3 == 2) {
            for (int i = 0; i < 3; i++) {
                if (brd[i * 3 + 2] != csym && brd[i * 3 + 2] != usym) {
                    brd[i * 3 + 2] = csym;
                    return;
                }
            }
        } else if (firstDiagonal % 3 == 2) {
            for (int i = 0; i < 3; i++) {
                if (brd[i * 4] != csym && brd[i * 4] != usym) {
                    brd[i * 4] = csym;
                    return;
                }
            }
        } else if (secondDiagonal % 3 == 2) {
            for (int i = 0; i < 3; i++) {
                if (brd[2 + i * 2] != csym && brd[2 + i * 2] != usym) {
                    brd[2 + i * 2] = csym;
                    return;
                }
            }
        }
        firstCol = 0;
        secondCol = 0;
        thirdCol = 0;
        firstRow = 0;
        secondRow = 0;
        thirdRow = 0;
        firstDiagonal = 0;
        secondDiagonal = 0;

        for (int i = 0; i < 3; i++) {
            if (brd[i] == usym) {
                firstRow++;
            }
        }
        for (int i = 0; i < 3; i++) {
            if (brd[i + 3] == usym) {
                secondRow++;
            }
        }
        for (int i = 0; i < 3; i++) {
            if (brd[i + 6] == usym) {
                thirdRow++;
            }
        }
        for (int i = 0; i < 3; i++) {
            if (brd[i * 3] == usym) {
                firstCol++;
            }
        }
        for (int i = 0; i < 3; i++) {
            if (brd[1 + i * 3] == usym) {
                secondCol++;
            }
        }
        for (int i = 0; i < 3; i++) {
            if (brd[2 + i * 3] == usym) {
                thirdCol++;
            }
        }
        for (int i = 0; i < 3; i++) {
            if (brd[i * 4] == usym) {
                firstDiagonal++;
            }
        }
        for (int i = 0; i < 3; i++) {
            if (brd[2 + i * 2] == usym) {
                secondDiagonal++;
            }
        }
        if (firstRow % 3 == 2) {
            for (int i = 0; i < 3; i++) {
                if (brd[i] != csym && brd[i] != usym) {
                    brd[i] = csym;
                    return;
                }
            }
        } else if (secondRow % 3 == 2) {
            for (int i = 0; i < 3; i++) {
                if (brd[i + 3] != csym && brd[i + 3] != usym) {
                    brd[i + 3] = csym;
                    return;
                }
            }
        } else if (thirdRow % 3 == 2) {
            for (int i = 0; i < 3; i++) {
                if (brd[i + 6] != csym && brd[i + 6] != usym) {
                    brd[i + 6] = csym;
                    return;
                }
            }
        } else if (firstCol % 3 == 2) {
            for (int i = 0; i < 3; i++) {
                if (brd[i * 3] != csym && brd[i * 3] != usym) {
                    brd[i * 3] = csym;
                    return;
                }
            }
        } else if (secondCol % 3 == 2) {
            for (int i = 0; i < 3; i++) {
                if (brd[i * 3 + 1] != csym && brd[i * 3 + 1] != usym) {
                    brd[i * 3 + 1] = csym;
                    return;
                }
            }
        } else if (thirdCol % 3 == 2) {
            for (int i = 0; i < 3; i++) {
                if (brd[i * 3 + 2] != csym && brd[i * 3 + 2] != usym) {
                    brd[i * 3 + 2] = csym;
                    return;
                }
            }
        } else if (firstDiagonal % 3 == 2) {
            for (int i = 0; i < 3; i++) {
                if (brd[i * 4] != csym && brd[i * 4] != usym) {
                    brd[i * 4] = csym;
                    return;
                }
            }
        } else if (secondDiagonal % 3 == 2) {
            for (int i = 0; i < 3; i++) {
                if (brd[2 + i * 2] != csym && brd[2 + i * 2] != usym) {
                    brd[2 + i * 2] = csym;
                    return;
                }
            }
        }
        for (int i = 0; i < brd.length; i++) {
            if (brd[i] != csym && brd[i] != usym) {
                brd[i] = csym;
                return;
            }
        }

    }

    public static boolean checkWin(char[] brd) {
        char sym;
        if (currentPlayer == 1) {
            sym = usym;
        } else {
            sym = csym;
        }

        // Check win by a row
        if (brd[0] == sym && brd[1] == sym && brd[2] == sym) {
            System.out.println("\n" + sym + " won!");
            return true;
        } else if (brd[3] == sym && brd[4] == sym && brd[5] == sym) {
            System.out.println("\n" + sym + " won!");
            return true;
        } else if (brd[6] == sym && brd[7] == sym && brd[8] == sym) {
            System.out.println("\n" + sym + " won!");
            return true;
        } // Check win by a column
        else if (brd[0] == sym && brd[3] == sym && brd[6] == sym) {
            System.out.println("\n" + sym + " won!");
            return true;
        } else if (brd[1] == sym && brd[4] == sym && brd[7] == sym) {
            System.out.println("\n" + sym + " won!");
            return true;
        } else if (brd[2] == sym && brd[5] == sym && brd[8] == sym) {
            System.out.println("\n" + sym + " won!");
            return true;
        } // Check win by a diagonal
        else if (brd[0] == sym && brd[4] == sym && brd[8] == sym) {
            System.out.println("\n" + sym + " won!");
            return true;
        } else if (brd[2] == sym && brd[4] == sym && brd[6] == sym) {
            System.out.println("\n" + sym + " won!");
            return true;
        } else {
            return false;
        }
    }

    public static boolean checkTie(char[] brd) {
        boolean tie = true;
        for (int i = 0; i < 9; i++) {
            if (brd[i] == Character.forDigit(i, 10)) {
                tie = false;
            }
        }
        if (tie) {
            System.out.println("\nIt's a tie!");
        }
        return tie;
    }
}

import java.util.Scanner;

public class BinaryToDecimal {

    public static void main(String[] args) {
        convert();
    }

    private static void convert() {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter a binary string:");
        String binary = input.nextLine(); // Reads line from console;
        try {
            // Tries to convert binary string;
            int decimal = Integer.parseInt(binary, 2);
            System.out.println("Binary: " + binary);
            System.out.println("Decimal: " + decimal);
        } catch (Exception e) {
            // If String is not binary, prints error message;
            System.out.println("That is not a binary string.\nPress enter to "
                    + "try again");
            input.nextLine(); // waits for pressing enter;
            convert(); // starts this ,ethod again;
        }
    }

}

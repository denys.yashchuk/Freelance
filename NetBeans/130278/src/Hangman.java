
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Hangman {

    public static void main(String[] args) {
        ArrayList<String> words = new ArrayList<String>();
        char anotherGame;

        try {
            // tries creates file scanner;
            Scanner fileReader = new Scanner(new File("./hangman.txt"));
            // if there is one more line, gets it and adds to ArrayList
            while (fileReader.hasNext()) {
                words.add(fileReader.nextLine());
            }
        } catch (FileNotFoundException ex) {
            // if we get an exception (there is no file), prints error message;
            System.out.println("There is no file :(");
            System.exit(1); // stops programm;
        }

        Scanner input = new Scanner(System.in);

        do {
            int index = (int) (Math.random() * words.size());

            String hiddenWord = words.get(index);
            StringBuilder guessedWord = new StringBuilder();

            for (int i = 0; i < hiddenWord.length(); i++) {
                guessedWord.append('*');
            }

            int numberOfCorrectLettersGuessed = 0, numberOfMisses = 0;

            while (numberOfCorrectLettersGuessed < hiddenWord.length()) {
                System.out.print("(Guess) Enter a letter in word " + guessedWord
                        + " > ");
                String s = input.nextLine();
                char letter = s.charAt(0);

                if (guessedWord.indexOf(letter + "") >= 0) {
                    System.out.println("\t" + letter + " is already in the word");
                } else if (hiddenWord.indexOf(letter) < 0) {
                    System.out.println("\t" + letter + " is not in the word");
                    numberOfMisses++;
                } else {
                    int k = hiddenWord.indexOf(letter);
                    while (k >= 0) {
                        guessedWord.setCharAt(k, letter);
                        numberOfCorrectLettersGuessed++;
                        k = hiddenWord.indexOf(letter, k + 1);
                    }
                }
            }

            System.out.println("The word is " + hiddenWord + ". You missed "
                    + numberOfMisses + ((numberOfMisses <= 1) ? " time" : " times"));

            System.out.print("Do you want to guess for another word? Enter y or n> ");
            anotherGame = input.nextLine().charAt(0);
        } while (anotherGame == 'y');
    }
}

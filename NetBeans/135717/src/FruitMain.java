import java.sql.*;
import java.util.*;

public class FruitMain {
	
			
public static void main(String[] args){
	try
    	{
    		Class.forName("com.mysql.jdbc.Driver");  //Establishing new driver
    	}
    	catch(Exception e)
    	{
    		System.out.println("Driver not found");
    	}
    	
    	try  //Getteing new connection with the server DB
    	{
    		Connection con = DriverManager.getConnection(
    			"jdbc:mysql://10.31.33.63/testdb", "student", "123456");
    			
    			Statement stm = con.createStatement(); //Create the statement object
    			try
    			{
    								stm.executeUpdate(  //Drop
    					"drop table Fruits");
    			}
    			catch(SQLException e)
    			{
    				System.out.println(e.getMessage());
    			}
    			try
    			{
    				stm.executeUpdate( //Create Table
    					"create table Fruits(name varchar(10) primary key , qty integer, price double)");
    			}
    			catch(SQLException e)
    			{
    				System.out.println("Table not created");
    			}
    			try  //Inserting and Deleting
    			{
    				stm.executeUpdate(
    					"insert into Fruits (name, qty, price) values('Banana', '100', '1.25')");
    				stm.executeUpdate(
    					"insert into Fruits (name, qty, price) values('Orange', '140', '5.5')");
    				stm.executeUpdate(
    					"insert into Fruits (name, qty, price) values('Apple', '450', '3.5')");
    				stm.executeUpdate(
    					"insert into Fruits (name, qty, price) values('Guava', '200', '4.5')");
    				stm.executeUpdate(
    					"insert into Fruits (name, qty, price) values('Papaya', '360', '6.5')");
    				stm.executeUpdate(
    					"insert into Fruits (name, qty, price) values('Grapes', '200', '4.0')");
    				stm.executeUpdate(
    					"insert into Fruits (name, qty, price) values('Mango', '360', '7.5')");
    			}
    			catch(SQLException e)
    			{
    				System.out.println(e.getMessage());
    			}
					
    			con.close();
    	}
    	catch(SQLException e)
    	{
    		System.out.println("Connection not found");
    	}
	while(true) {
	System.out.println("1.Display stock info for all items");
	System.out.println("2.Display stock info for particular item");
	System.out.println("3.Update stock info for particular item");
	System.out.println("4.Add new item");
	System.out.println("5.Delete item");
	System.out.println("6.Sell items to customer");
	System.out.println("7.Exit");
	
	System.out.println("Please Select your Choice: ");
	Scanner scanner = new Scanner(System.in);
	int choice = scanner.nextInt();
	FruitMethods method = new FruitMethods();
	try{
	
	switch (choice) {
		case 1: 
			method.displayAll();
			break;
		case 2:
			method.displayItem();
			break;
		case 3:
			method.updateItem();
			break;
		case 4:
			method.addItem();
			break;
		case 5:
			method.deleteItem();
			break;
		case 6:
			method.sellItem();
			break;
		case 7: 
			System.exit(0);
			break;
		default: 
			System.out.println("Invalid Choice.");
			break;					
	}
	
	}
	catch(Exception e){
		
	}
	}
}
}

import java.sql.*;
import java.util.*;

class FruitMethods {

    Scanner scanner = new Scanner(System.in);
    private PreparedStatement preparedStatement;

    //OPTION 1
    public String displayAll() throws Exception {
        String out = "";
        try {

            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://10.31.33.63/testdb", "student", "123456");
            Statement stm = con.createStatement();
            ResultSet rs = stm.executeQuery("select * from Fruits"); //selecting a specific row
            System.out.println("Name \t Stock Quantity \t Unit Price(SAR)");
            while (rs.next()) {
                out += rs.getString(1) + "&" + rs.getInt(2) + "&" + rs.getDouble(3)+"\n";
            }
        } catch (Exception e) {

        }
        return out;
    }
    //////////////OPTION 2///////////////

    public String displayItem(String kb) throws Exception {
        String out = "";
        try {

            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://10.31.33.63/testdb", "student", "123456");
            Statement stm = con.createStatement();
            String queryString = "select * from Fruits where name = ? ";
            preparedStatement = con.prepareStatement(queryString);

            preparedStatement.setString(1, kb);
            ResultSet rs = preparedStatement.executeQuery();
            System.out.println("Name \t Stock Quantity \t Unit Price(SAR)");
            while (rs.next()) {
                out += rs.getString(1) + "\t\t\t" + rs.getInt(2) + "\t\t\t\t" + rs.getDouble(3)+"\n";
            }
        } catch (Exception e) {
            System.out.println("No Such Fruit.");
        }
        return out;
    }

////////////////////////OPTION 3/////////////////////////
    public void updateItem(String kb, int newStock, double  newPrice) throws SQLException {
        try {
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://10.31.33.63/testdb", "student", "123456");
            Statement stm = con.createStatement();


            System.out.println("1.Modify stock quantity: ");
            System.out.println("2.Modify unit price: ");
            System.out.println("Choose one of the options: ");

                String queryString = "select * from Fruits where name = ? ";
                preparedStatement = con.prepareStatement(queryString);

                preparedStatement.setString(1, kb);
                ResultSet rs = preparedStatement.executeQuery();
                while (rs.next()) {
                    System.out.println(
                            "Available stock for " + rs.getString(1) + " is " + rs.getInt(2));
                }

                if (newStock >= 0) {
                    queryString = "update Fruits set qty = ? where name = ?";
                    preparedStatement = con.prepareStatement(queryString);
                    preparedStatement.setInt(1, newStock);
                    preparedStatement.setString(2, kb);
                    preparedStatement.executeUpdate();

                    System.out.println("Fruit quantity is updated.");
                } else {
                    System.out.println("Error: Negative stock quantity.");
                }

                queryString = "select * from Fruits where name = ? ";
                preparedStatement = con.prepareStatement(queryString);

                preparedStatement.setString(1, kb);
                rs = preparedStatement.executeQuery();
                while (rs.next()) {
                    System.out.println(
                            "Current unit price for " + rs.getString(1) + " is " + rs.getInt(3) + " SAR");
                }

                if (newPrice >= 0) {
                    queryString = "update Fruits set price = ? where name = ?";
                    preparedStatement = con.prepareStatement(queryString);
                    preparedStatement.setDouble(1, newPrice);
                    preparedStatement.setString(2, kb);
                    preparedStatement.executeUpdate();

                    System.out.println("Fruit unit price is updated.");
                } else {
                    System.out.println("Error: Negative unit price.");
                }

        } catch (Exception e) {
            System.out.println("No Such Fruit.");
        }
    }

////////////////////////OPTION 4/////////////////////////
    public void addItem(String kb, int kb1, double kb2) {
        try {

            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://10.31.33.63/testdb", "student", "123456");
            Statement stm = con.createStatement();
            String queryString = "insert into Fruits (name,qty,price) Values (?,?,?)";
            preparedStatement = con.prepareStatement(queryString);

            preparedStatement.setString(1, kb);
            preparedStatement.setInt(2, kb1);
            preparedStatement.setDouble(3, kb2);
            preparedStatement.executeUpdate();
            System.out.println("Fruit added.");

        } catch (Exception e) {
            System.out.println("No Such Fruit.");
        }
    }

    ////////////////OPTION 5///////////////////////
    public void deleteItem(String kb) {
        try {
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://10.31.33.63/testdb", "student", "123456");
            Statement stm = con.createStatement();

            String queryString = "delete from Fruits where name = ?";
            preparedStatement = con.prepareStatement(queryString);

            preparedStatement.setString(1, kb);
            preparedStatement.executeUpdate();

            System.out.println("Fruit deleted.");

        } catch (Exception e) {
            System.out.println("No Such Fruit.");
        }
    }

//////////////////////OPTION 6/////////////////////////////
    public double sellItem(String fruitName, int fruitNum) {
        try {
            double total = 0;
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://10.31.33.63/testdb", "student", "123456");
            Statement stm = con.createStatement();

                String queryString = "select * from Fruits where name = ? ";
                preparedStatement = con.prepareStatement(queryString);
                preparedStatement.setString(1, fruitName);
                ResultSet rs = preparedStatement.executeQuery();

                while (rs.next()) {

                    String fruit = rs.getString(1);
                    int qty = rs.getInt(2);
                    double price = rs.getDouble(3);
                    double Subtotal = fruitNum * rs.getDouble(3);
                    if (fruitNum <= qty) {

                        System.out.println("Name \t  Quantity \t Unit Price(SAR) \t Sub Total(SAR)");
                        System.out.println(
                                rs.getString(1) + "\t\t" + fruitNum + "\t\t\t\t" + rs.getDouble(3) + "\t\t\t\t" + Subtotal);
                        int sum = qty - fruitNum;
                        total = Subtotal + total;
                        queryString = "update Fruits set qty = ? where name = ?";
                        preparedStatement = con.prepareStatement(queryString);
                        preparedStatement.setInt(1, sum);
                        preparedStatement.setString(2, fruitName);
                        preparedStatement.executeUpdate();
                        if (sum == 0) {
                            queryString = "delete from Fruits where name = ?";
                            preparedStatement = con.prepareStatement(queryString);
                            preparedStatement.setString(1, fruit);
                            preparedStatement.executeUpdate();
                        }
                    } else {
                        System.out.println("Insufficient stock. stock available is " + rs.getInt(2));
                    }
                    
                }
                return total;
        } catch (Exception e) {
            System.out.println("Unknown Error.");
            return 0;
        }
        
    }
}
